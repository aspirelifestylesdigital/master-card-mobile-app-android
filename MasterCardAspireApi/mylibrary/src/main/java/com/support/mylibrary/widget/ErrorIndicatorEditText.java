package com.support.mylibrary.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import com.support.mylibrary.R;

/**
 * Created by vinh.trinh on 5/18/2017.
 */

public class ErrorIndicatorEditText extends AppCompatEditText implements View.OnFocusChangeListener {

    private Drawable errorIndicator;
    private Drawable normal;
    private boolean isError;
    private OnFocusChangeListener focusChangeListener;
    private TextInteractListener textInteractListener;
    int pH, pV;

    public void setTextInteractListener(TextInteractListener textInteractListener) {
        this.textInteractListener = textInteractListener;
    }

    public ErrorIndicatorEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        errorIndicator = ContextCompat.getDrawable(context, R.drawable.error_indicator);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ErrorIndicatorEditText);
        String font = attributes.getString(R.styleable.ErrorIndicatorEditText_eiedt_font);
        if(!TextUtils.isEmpty(font)){
            setTypeface(Typeface.createFromAsset(context.getAssets(), font));
        }
        attributes.recycle();
        super.setOnFocusChangeListener(this);
    }

    @Override
    public void setError(CharSequence error) {
        if (normal == null) {
            normal = getBackground();
            pV = this.getPaddingTop();
            pH = this.getPaddingLeft();
        }
        if(error == null) {
            stateNormal();
        } else {
            stateError();
        }
    }

    @Override
    public void setError(CharSequence error, Drawable icon) {
        if (normal == null) {
            normal = getBackground();
            pV = this.getPaddingTop();
            pH = this.getPaddingLeft();
        }
        if(error == null) {
            stateNormal();
        } else {
            stateError();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        boolean error = this.isError;
        if (focusChangeListener != null) focusChangeListener.onFocusChange(v, hasFocus);
        this.isError = error;
        if (isError) {
            stateError();
        }
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener l) {
        focusChangeListener = l;
    }

    private void stateError() {
        setBackgroundDrawable(errorIndicator);
        setPadding(pV, pH, pV, pH);
        isError = true;
    }

    private void stateNormal() {
        setBackgroundDrawable(normal);
        setPadding(pV, pH, pV, pH);
        isError = false;
    }
    @Override
    public boolean onTextContextMenuItem(int id) {
        boolean consumed = super.onTextContextMenuItem(id);
        if(textInteractListener == null) return consumed;
        switch (id) {
            case android.R.id.paste:
                textInteractListener.onTextPaste(this, this.getText().toString());
                return true;
        }
        return consumed;
    }

    public void showErrorColorLine(){
        setError("");
    }

    public void hideErrorColorLine(){
        setError(null);
    }

    public interface TextInteractListener {
        void onTextPaste(AppCompatEditText view, String textPasted);
    }
}
