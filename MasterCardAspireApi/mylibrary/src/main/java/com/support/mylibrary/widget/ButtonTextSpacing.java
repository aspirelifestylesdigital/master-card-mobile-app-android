package com.support.mylibrary.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ScaleXSpan;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.support.mylibrary.R;

/**
 * Created by vinh.trinh on 5/23/2017.
 */

public class ButtonTextSpacing extends AppCompatButton {
    private float spacing;
    private CharSequence originalText = "";
    private float pxScale = 1f;
    private boolean textAllCaps = true;
    public ButtonTextSpacing(Context context) {
        super(context);
    }

    public ButtonTextSpacing(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.ButtonTextSpacing);
        spacing = attributes.getFloat(R.styleable.ButtonTextSpacing_text_spacing, 0);
        spacing = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, spacing,getResources().getDisplayMetrics());
        String font = attributes.getString(R.styleable.ButtonTextSpacing_btn_font);
        setCustomFont(context, font);
        attributes.recycle();
        originalText = super.getText();
        applyLetterSpacing();
        this.invalidate();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = text;
        applyLetterSpacing();
    }

    @Override
    public CharSequence getText() {
        return originalText;
    }

    /*private void applyLetterSpacing() {
        if (this.originalText == null) return;
        if(spacing == 0) {
            super.setText(originalText, BufferType.SPANNABLE);
            return;
        }
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < originalText.length(); i++) {
            String c = ""+ originalText.charAt(i);
            builder.append(c.toLowerCase());
            if(i+1 < originalText.length()) {
                builder.append("\u00A0");
            }
        }
        SpannableString finalText = new SpannableString(builder.toString());
        if(builder.toString().length() > 1) {
            for(int i = 1; i < builder.toString().length(); i+=2) {
                finalText.setSpan(new ScaleXSpan((spacing+1)/10), i, i+1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        super.setText(finalText, BufferType.SPANNABLE);
    }*/
    private void applyLetterSpacing() {
        if (spacing == 0f) {
            super.setText(originalText, BufferType.SPANNABLE);
            return;
        }

        // Check if current SDK version is after LOLLIPOP (21), then use letter spacing instead
        /*if (!isInEditMode() && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setLetterSpacing(spacing * dpScale / 11); // 'EM' unit: 1 'EM' = 11 dp
            super.setText(originalText, BufferType.SPANNABLE);
            return;
        }*/

        if (TextUtils.isEmpty(originalText)
                || originalText.length() < 2
                || (spacing >= -1f && spacing <= 1f)) {
            super.setText(originalText, BufferType.SPANNABLE);
            return;
        }

        // Make ALL CAPS flag false first
        setAllCaps(false);
        // Create text builder with non breaking space letter between
        StringBuilder builder;
        if (textAllCaps) {
            builder = new StringBuilder(originalText.toString().toUpperCase().replaceAll("(.)", "\u00A0" + "$1"));
        } else {
            builder = new StringBuilder(originalText.toString().replaceAll("(.)", "\u00A0" + "$1"));
        }

        float nonBreakingSpace = getPaint().measureText("\u00A0");
        //Log.d(TAG, "nonBreakingSpace = " + nonBreakingSpace);

        float scale = spacing * pxScale / nonBreakingSpace;
        //Log.d(TAG, "scale = " + scale);

        if (scale > 0.9 && scale < 1f) {
            super.setText(builder, BufferType.NORMAL);
            return;
        }

        // Scale all non breaking space letters by provided spacing value
        SpannableString finalText = new SpannableString(builder);
        if (builder.length() > 1) {
            for (int i = 0; i < builder.length(); i += 2) {
                finalText.setSpan(new ScaleXSpan(scale),
                        i,
                        i + 1,
                        Spannable.SPAN_INCLUSIVE_INCLUSIVE
                );
            }
        }
        // Set text with buffer type SPANNABLE
        super.setText(finalText, BufferType.SPANNABLE);

        setPadding(getPaddingLeft() - (int) (spacing * pxScale - spacing), getPaddingTop(), getPaddingRight(), getPaddingBottom());
    }
    private void setCustomFont(Context ctx, String font) {
        if(TextUtils.isEmpty(font)) return;
        try {
            Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), font);
            setTypeface(typeface);
        } catch (Exception e) {
//            Log.e(TAG, "Unable to load typeface: "+e.getMessage());
        }
    }
}
