package com.support.mylibrary.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.support.mylibrary.R;

public class JustifiedTextView extends WebView {
    public static final String TAG = JustifiedTextView.class.getName();

    public interface IJustifyTextViewListener {
        void onContentScroll(boolean hitToBottom);
        void onHyperLinkClicked(String url);
    }
    private String core      = "<html><head>" +
            "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">" +
            "<style type=\"text/css\">\n" +
            "@font-face {\n" +
            "    font-family: mc;\n" +
            "    src: url(\"file:///android_asset/%s\")\n" +
            "}\n" +
            "body {\n" +
            "    font-family: mc;\n" +
            "    font-size: medium;\n" +

            "}\n" +
            "a {color: #ff4000;" +
            "-webkit-tap-highlight-color: rgba(0,0,0,0);}\n" +
            "a:link,a:visited,a:hover,a:active{background-color:transparent;}" +
            "</style></head><body style='text-align:%s;color:rgba(%s);font-size:%dpx;margin: 0; padding: 0'>%s</body></html>";
    private String text;
    private int textColor;
    private int backgroundColor;
    private int textSize;
    private String fontName;
    private String textAlign;
    IJustifyTextViewListener justifyTextViewListener;

    public JustifiedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public JustifiedTextView(Context context, AttributeSet attrs, int i) {
        super(context, attrs, i);
        init(attrs);
    }

    @SuppressLint("NewApi")
    public JustifiedTextView(Context context, AttributeSet attrs, int i, boolean b) {
        super(context, attrs, i, b);
        init(attrs);
    }

    public void setJustifyTextViewListener(IJustifyTextViewListener justifyTextViewListener) {
        this.justifyTextViewListener = justifyTextViewListener;
    }
    private void detectWebviewScroll(){
        int diff = (int)(getContentHeight()*getContext().getResources().getDisplayMetrics().density)
                -(getHeight()+getScrollY());// Calculate the difference in scrolling
        //Log.d(TAG, "Diff = " + diff);
        //Log.d(TAG, "Webview + hit bottom = " + (diff <= 5));
        if(justifyTextViewListener != null) {
            // The bottom has been reached if the difference is 0
            justifyTextViewListener.onContentScroll(diff <= 5);
        }
    }
    @Override
    protected void onScrollChanged(int x, int y, int oldx, int oldy) {
        super.onScrollChanged(x, y, oldx, oldy);
        detectWebviewScroll();
    }
    private void init(AttributeSet attrs) {
        TypedArray a=getContext().obtainStyledAttributes(
                attrs,
                R.styleable.JustifiedTextView);

        text = a.getString(R.styleable.JustifiedTextView_text);
        textColor = a.getColor(R.styleable.JustifiedTextView_textColor, Color.WHITE);
        backgroundColor = a.getColor(R.styleable.JustifiedTextView_backgroundColor, Color.TRANSPARENT);
        textSize = a.getInt(R.styleable.JustifiedTextView_textSize, 12);
        fontName = a.getString(R.styleable.JustifiedTextView_fontName);
        textAlign = a.getString(R.styleable.JustifiedTextView_textAlign);
        if(TextUtils.isEmpty(textAlign)){
            textAlign = "center";
        }
        a.recycle();

        this.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);
        this.setOverScrollMode(WebView.OVER_SCROLL_NEVER);
        this.setWebChromeClient(new WebChromeClient(){

        });
        this.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        detectWebviewScroll();
                    }
                }, 500);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //Log.d(TAG, "shouldOverrideUrlLoading(WebView view, String url)");
                if(justifyTextViewListener != null){
                    justifyTextViewListener.onHyperLinkClicked(url);
                }
                return true;
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                //Log.d(TAG, "onLoadResource : " + url);
            }
        });
        //loadUrl("javascript:document.body.style.marginRight=document.body.style.marginBottom= '0px'");
    }

    public void setText(String s){
        text = s;
        reloadData();
    }

    @SuppressLint("NewApi")
    private void reloadData(){

        if(!TextUtils.isEmpty(text)) {
            String data = String.format(core,fontName, textAlign, toRgba(textColor),textSize,text);
            this.loadDataWithBaseURL(null, data, "text/html","utf-8", null);
        }

        // set WebView's background color *after* data was loaded.
        super.setBackgroundColor(backgroundColor);

        // Hardware rendering breaks background color to work as expected.
        // Need to use software renderer in that case.
        if(android.os.Build.VERSION.SDK_INT >= 11)
            this.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }

    public void setTextColor(int hex){
        textColor = hex;
        reloadData();
    }

    public void setBackgroundColor(int hex){
        backgroundColor = hex;
        reloadData();
    }

    public void setTextSize(int textSize){
        this.textSize = textSize;
        reloadData();
    }

    private String toRgba(int hex) {
        String h = Integer.toHexString(hex);
        int a = Integer.parseInt(h.substring(0, 2),16);
        int r = Integer.parseInt(h.substring(2, 4),16);
        int g = Integer.parseInt(h.substring(4, 6),16);
        int b = Integer.parseInt(h.substring(6, 8),16);
        return String.format("%d,%d,%d,%d", r, g, b, a);
    }
}