package com.support.mylibrary.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatCheckBox;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.support.mylibrary.R;

/**
 * Created by vinh.trinh on 6/5/2017.
 */

public class FontCheckbox extends AppCompatCheckBox {

    public FontCheckbox(Context context) {
        super(context);
    }

    public FontCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.FontCheckbox);
        String font = attributes.getString(R.styleable.FontCheckbox_cb_font);
        int buttonResId = attributes.getResourceId(R.styleable.FontCheckbox_button_drawable, 0);
        if(buttonResId != 0){
            setButtonDrawable(buttonResId);
        }
        setCustomFont(context, font);
        attributes.recycle();
    }

    private void setCustomFont(Context ctx, String font) {
        if(TextUtils.isEmpty(font)) return;
        try {
            Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), font);
            setTypeface(typeface);
        } catch (Exception e) {
//            Log.e(TAG, "Unable to load typeface: "+e.getMessage());
        }
    }
}
