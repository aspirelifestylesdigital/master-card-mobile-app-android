package com.support.mylibrary.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.support.mylibrary.R;

/**
 * Created by vinh.trinh on 6/5/2017.
 */

public class FontEditText extends AppCompatEditText {

    public FontEditText(Context context) {
        super(context);
    }

    public FontEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.FontEditText);
        String font = attributes.getString(R.styleable.FontEditText_edt_font);
        setCustomFont(context, font);
        attributes.recycle();
    }

    private void setCustomFont(Context ctx, String font) {
        if(TextUtils.isEmpty(font)) return;
        try {
            Typeface typeface = Typeface.createFromAsset(ctx.getAssets(), font);
            setTypeface(typeface);
        } catch (Exception e) {
//            Log.e(TAG, "Unable to load typeface: "+e.getMessage());
        }
    }
}
