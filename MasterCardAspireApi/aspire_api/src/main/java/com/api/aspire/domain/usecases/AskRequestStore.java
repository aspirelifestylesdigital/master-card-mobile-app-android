package com.api.aspire.domain.usecases;

import com.api.aspire.data.preference.PreferencesStorageAspire;

public class AskRequestStore {

    private PreferencesStorageAspire preferencesStorageAspire;

    public AskRequestStore(PreferencesStorageAspire preferencesStorageAspire) {
        this.preferencesStorageAspire = preferencesStorageAspire;
    }

    public String loadContent() {
        return preferencesStorageAspire.requestContentStore();
    }

    public void saveContent(String content) {
        preferencesStorageAspire.editor().requestContentStore(content).build().saveAsync();
    }

}
