package com.api.aspire.domain.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.api.aspire.common.logic.EncryptData;
import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class ProfileAspire extends ProfileAspireResponse implements Parcelable {
    private boolean locationOn;
    //-- save internal email local
    private String userNameLocal;
    private String secretKey;
    private String phoneLocal;
    @SerializedName("preferenceData")
    private PreferenceData preferenceData;

    //- variable internal app
    private String countryCode;
    /**
     * verify binCode local app
     */
    private String binCodeLocal;
    /**
     * verify binCode on api
     */
    private String binCodeRemote;

    private String zipCode;

    public String getFirstName() {
        return getFirstname();
    }

    public void setFirstName(String firstName) {
        setFirstname(firstName);
    }

    public String getLastName() {
        return getLastname();
    }

    public void setLastName(String lastName) {
        setLastname(lastName);
    }

    public String getEmail() {
        return getUserNameLocal();
    }

    public void setEmail(String email) {
        setUserNameLocal(email);
    }

    public String getPhone() {
        return getPhoneLocal();
    }

    public void setPhone(String phone) {
        setPhoneLocal(phone);
    }

    public boolean isLocationOn() {
        return locationOn;
    }

    public void locationToggle(boolean on) {
        this.locationOn = on;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public PreferenceData getPreferenceData() {
        return preferenceData;
    }

    public void setPreferenceData(PreferenceData preferenceData) {
        this.preferenceData = preferenceData;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public ProfileAspire() {

    }

    public ProfileAspire(String jsonString) {
        if (TextUtils.isEmpty(jsonString)) {
            return;
        }

        ProfileAspire profile = new Gson().fromJson(jsonString, ProfileAspire.class);

        if (profile == null) {
            return;
        }
        this.partyid = profile.getPartyid();
        this.firstname = profile.getFirstname();
        this.lastname = profile.getLastname();
        this.displayname = profile.getDisplayname();
        this.prefix = profile.getPrefix();
        this.salutation = profile.getSalutation();
        this.homecountry = profile.getHomecountry();
        this.disabilitystatus = profile.getDisabilitystatus();
        this.persontype = profile.getPersontype();
        this.locations = profile.getLocations();
        this.phones = profile.getPhones();
        this.emails = profile.getEmails();
        this.preferences = profile.getPreferences();
        this.appUserPreferences = profile.getAppUserPreferences();
        this.relationships = profile.getRelationships();
        this.locationOn = profile.isLocationOn();
        this.userNameLocal = profile.getUserNameLocal();
        this.secretKey = profile.getSecretKey();
        this.phoneLocal = profile.getPhoneLocal();
        this.preferenceData = profile.getPreferenceData();
        this.binCodeLocal = profile.getBinCodeLocal();
        this.binCodeRemote = profile.getBinCodeRemote();
        this.zipCode = profile.getZipCode();
    }

    public JSONObject toJSON() throws JSONException {
        String jsonString = new Gson().toJson(this);

        if (TextUtils.isEmpty(jsonString)) {
            return null;
        }
        return new JSONObject(jsonString);
    }

    public String getSecretKeyDecrypt() {
        return EncryptData.getInstance().decrypt(secretKey);
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = EncryptData.getInstance().encrypt(secretKey);
    }

    private String getPhoneLocal() {
        return phoneLocal;
    }

    private void setPhoneLocal(String phoneLocal) {
        this.phoneLocal = phoneLocal;
    }

    private String getUserNameLocal() {
        return userNameLocal;
    }

    private void setUserNameLocal(String userNameLocal) {
        this.userNameLocal = userNameLocal;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getBinCodeLocal() {
        return binCodeLocal;
    }

    public void setBinCodeLocal(String binCodeLocal) {
        this.binCodeLocal = binCodeLocal;
    }

    public String getBinCodeRemote() {
        return binCodeRemote;
    }

    public void setBinCodeRemote(String binCodeRemote) {
        this.binCodeRemote = binCodeRemote;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.locationOn ? (byte) 1 : (byte) 0);
        dest.writeString(this.userNameLocal);
        dest.writeString(this.secretKey);
        dest.writeString(this.phoneLocal);
        dest.writeParcelable(this.preferenceData, flags);
        dest.writeString(this.countryCode);
        dest.writeString(this.binCodeLocal);
        dest.writeString(this.binCodeRemote);
        dest.writeString(this.zipCode);
        dest.writeTypedList(appUserPreferences);
    }

    protected ProfileAspire(Parcel in) {
        this.locationOn = in.readByte() != 0;
        this.userNameLocal = in.readString();
        this.secretKey = in.readString();
        this.phoneLocal = in.readString();
        this.preferenceData = in.readParcelable(PreferenceData.class.getClassLoader());
        this.countryCode = in.readString();
        this.binCodeLocal = in.readString();
        this.binCodeRemote = in.readString();
        this.zipCode = in.readString();
        appUserPreferences = in.createTypedArrayList(AppUserPreferences.CREATOR);
    }

    public static final Creator<ProfileAspire> CREATOR = new Creator<ProfileAspire>() {
        @Override
        public ProfileAspire createFromParcel(Parcel source) {
            return new ProfileAspire(source);
        }

        @Override
        public ProfileAspire[] newArray(int size) {
            return new ProfileAspire[size];
        }
    };
}
