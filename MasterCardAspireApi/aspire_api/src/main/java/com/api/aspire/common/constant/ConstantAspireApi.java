package com.api.aspire.common.constant;

public class ConstantAspireApi {

    public interface APP_USER_PREFERENCES {

        String USER_LOCATION_STATUS_KEY = "User Location Status";
        String BIN_CODE_KEY = "Passcode"; //-- veraCode security app call name is BinCode
        String SELECTED_CITY_KEY = "Selected City";
        String NEWSLETTER_STATUS_KEY = "Newsletter Status";// MC
        String NEWSLETTER_DATE_KEY = "Newsletter Date";// MC
        String POLICY_VERSION_KEY = "Policy Version";// MC
        String PLATFORM_KEY = "Device OS";
    }
}
