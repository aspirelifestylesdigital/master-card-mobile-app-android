package com.api.aspire.data.repository;

import com.api.aspire.data.datasource.RemoteAuthDataStore;
import com.api.aspire.domain.repository.AuthAspireRepository;

import io.reactivex.Single;

public class AuthDataAspireRepository implements AuthAspireRepository {

    @Override
    public Single<Boolean> checkPassCode(String tokenService, String xAppId, String xOrganization, int passCode) {
        return new RemoteAuthDataStore().checkPassCode(tokenService, xAppId, xOrganization, passCode);
    }
}
