package com.api.aspire.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;

import io.reactivex.Completable;
import io.reactivex.CompletableEmitter;
import io.reactivex.Single;

public class SignInCase{

    private GetToken getToken;
    private ProfileAspireRepository profileRepository;
    private UserProfileOKTADataRepository userProfileOKTADataRepository;

    //<editor-fold desc="Variable">
    private String accessTokenCache;
    private MapProfileBase mapLogic;
    //</editor-fold>

    public SignInCase(MapProfileBase mapLogic,
                      ProfileAspireRepository profileRepository,
                      UserProfileOKTADataRepository userProfileOKTADataRepository,
                      GetToken getToken) {

        this.profileRepository = profileRepository;
        this.getToken = getToken;
        this.mapLogic = mapLogic;
        this.userProfileOKTADataRepository = userProfileOKTADataRepository;
    }

    public Completable handleSavePassCode(ProfileAspire profile, String passCodeStorageCache, int bin) {
        if (profile == null
                || TextUtils.isEmpty(passCodeStorageCache)
                || TextUtils.isEmpty(accessTokenCache)) {

            return Completable.error(new Exception("Error General save PassCode"));
        }

        String passCodeRemote = ProfileLogicCore.getAppUserPreference(profile.getAppUserPreferences(),
                ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);

        Completable rsCompletable;

        if (TextUtils.isEmpty(passCodeRemote)
                || !passCodeRemote.equals(passCodeStorageCache)) {

            ProfileLogicCore.updateAppUserPreference(profile.getAppUserPreferences(),
                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY,
                    passCodeStorageCache);

            rsCompletable = profileRepository.saveProfile(accessTokenCache, profile);

        } else {

            rsCompletable = Completable.create(CompletableEmitter::onComplete);
        }

        return rsCompletable;
    }

    public Single<ProfileAspire> getProfile(Params params) {
        return getToken.getTokenUser(params.email, params.password)
                .flatMap(accessToken -> {
                    accessTokenCache = accessToken;
                    return profileRepository.signInProfile(accessToken, params.password, params.email);
                });
    }

    //<editor-fold desc="flow check passCode Sign In project MDA, BDA, MC">

    public Single<ProfileAspire> loadProfileNoSaveStorage(Params params) {
        return getToken.getTokenUser(params.email, params.password)
                .flatMap(accessToken -> {
                    accessTokenCache = accessToken;
                    return profileRepository.signInNoSaveStorage(accessToken, params.password, params.email);
                });
    }

    public Single<ProfileAspire> saveProfileStorage(ProfileAspire profileAspire) {
        return profileRepository.saveProfileStorage(profileAspire);
    }

    public Completable loadProfileOKTACheckStatus(String email) {
        String clientIdEmail = mapLogic.convertClientIdEmail(email);
        return userProfileOKTADataRepository.getUserProfileOKTA(clientIdEmail)
                .flatMapCompletable(userProfileOKTA -> userProfileOKTADataRepository.handleCheckStatus(userProfileOKTA));
    }

    //</editor-fold>

    public static class Params {
        public final String email;
        final String password;

        public Params(String email, String password) {
            this.email = email;
            this.password = password;
        }
    }
}
