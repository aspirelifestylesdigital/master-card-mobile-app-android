package com.api.aspire.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.AuthAspireRepository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class CheckPassCode extends UseCase<Boolean, CheckPassCode.Params> {

    private PreferencesStorageAspire preferencesStorage;
    private AuthAspireRepository repository;
    private GetAccessToken mGetAccessToken;
    private LoadProfile mLoadProfile;
    private SaveProfile saveProfile;
    private String accessToken;
    private GetToken getToken;

    private MapProfileBase mapLogic;

    public CheckPassCode(MapProfileBase mapLogic,
                         PreferencesStorageAspire preferencesStorage,
                         AuthAspireRepository repository,
                         GetAccessToken mGetAccessToken,
                         LoadProfile mLoadProfile,
                         SaveProfile saveProfile,
                         GetToken getToken) {

        this.repository = repository;
        this.mGetAccessToken = mGetAccessToken;
        this.mLoadProfile = mLoadProfile;
        this.saveProfile = saveProfile;
        this.getToken = getToken;
        this.preferencesStorage = preferencesStorage;
        this.mapLogic = mapLogic;
    }

    @Override
    Observable<Boolean> buildUseCaseObservable(Params params) {
        return null;
    }

    @Override
    public Single<Boolean> buildUseCaseSingle(Params params) {
        final int passCodeNumber = CommonUtils.convertStringToInt(params.binCode);
        if (passCodeNumber == -1) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }
        return getToken.getTokenService()
                .flatMap(serviceToken ->
                        repository.checkPassCode(serviceToken, mapLogic.getXAppId(), mapLogic.getXOrganization(), passCodeNumber)
                );
    }

    @Override
    Completable buildUseCaseCompletable(Params params) {
        return null;
    }

    public Completable saveUserProfile(String newPassCode) {

        final int passCodeNumber = CommonUtils.convertStringToInt(newPassCode);
        if (passCodeNumber == -1) {
            return Completable.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }

        return mGetAccessToken.execute()
                .flatMap(accessToken -> {
                    this.accessToken = accessToken;
                    return mLoadProfile.buildUseCaseSingle(accessToken);
                })
                .flatMapCompletable(profileAspire -> {
                    if (profileAspire != null
                            && profileAspire.getAppUserPreferences() != null) {
                        //update binCode into profile
                        ProfileLogicCore.updateAppUserPreference(profileAspire.getAppUserPreferences(),
                                ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY,
                                newPassCode);
                    }

                    return saveProfile.buildUseCaseCompletable(new SaveProfile.Params(profileAspire, accessToken));
                });
    }

    public Single<ResultPassCode> handleCheckPassCode(ProfileAspire profileAspire) {
        if (profileAspire == null) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }

        String passCodeInProfile = getAppUserPreference(profileAspire.getAppUserPreferences(),
                ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);

        if (TextUtils.isEmpty(passCodeInProfile)) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }

        Params params = new Params(passCodeInProfile);

        return buildUseCaseSingle(params).flatMap(statusPassCode -> {
            if (statusPassCode == null || !statusPassCode) {
                return Single.just(new ResultPassCode(PassCodeStatus.INVALID));
            } else {
                return Single.just(new ResultPassCode(PassCodeStatus.VALID));
            }
        });
    }

    public Single<ResultPassCode> handleCheckPassCode() {
        return mGetAccessToken.execute()
                .flatMap(accessToken -> mLoadProfile.buildUseCaseSingle(accessToken))
                .flatMap(this::handleCheckPassCode);
    }

    /**
     * Wrapper enum data type
     */
    public static class ResultPassCode {
        public PassCodeStatus passCodeStatus;

        public ResultPassCode(PassCodeStatus passCodeStatus) {
            this.passCodeStatus = passCodeStatus;
        }

        public PassCodeStatus getPassCodeStatus() {
            return passCodeStatus;
        }
    }

    public enum PassCodeStatus {VALID, INVALID, NONE}

    public static class Params {
        String binCode;

        public Params(String binCode) {
            this.binCode = binCode;
        }

        public void setBinCode(String binCode) {
            this.binCode = binCode;
        }
    }

    private String getAppUserPreference(List<ProfileAspireResponse.AppUserPreferences> appUserPreferences, String key) {
        if (TextUtils.isEmpty(key)) {
            return "";
        }
        if (appUserPreferences == null || appUserPreferences.isEmpty()) return "";


        for (ProfileAspireResponse.AppUserPreferences preference : appUserPreferences) {
            if (key.equals(preference.getPreferenceKey())) {
                return preference.getPreferenceValue();
            }
        }
        return "";
    }


    //<editor-fold desc="flow for UDA: check binCode before login">
    public Single<Boolean> checkOutPassCode(Params params) {
        return buildUseCaseSingle(params)
                .map((result) -> {
                    if (result != null && result) {
                        preferencesStorage.editor().binCode(params.binCode).build().save();
                    }
                    return result;
                });
    }

    public String getPassCodeStorage() {
        return preferencesStorage.getBinCode();
    }

    public Single<String> passCodeStorage() {
        return Single.create(e -> {
            String passCode = getPassCodeStorage();
            if (TextUtils.isEmpty(passCode)) {
                e.onError(new Exception(ErrCode.PASS_CODE_ERROR.name()));
            } else {
                e.onSuccess(passCode);
            }
        });
    }
    //</editor-fold>
}
