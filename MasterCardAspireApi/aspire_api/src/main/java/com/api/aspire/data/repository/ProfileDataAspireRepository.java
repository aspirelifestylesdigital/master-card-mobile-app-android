package com.api.aspire.data.repository;

import android.text.TextUtils;
import android.util.Log;

import com.api.aspire.data.datasource.LocalProfileDataStore;
import com.api.aspire.data.datasource.RemoteProfileDataStoreAspire;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.api.aspire.domain.usecases.MapProfileBase;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public class ProfileDataAspireRepository implements ProfileAspireRepository {

    private LocalProfileDataStore local;
    private RemoteProfileDataStoreAspire remote;
    private PreferencesStorageAspire pref;

    private MapProfileBase mapLogic;

    public ProfileDataAspireRepository(PreferencesStorageAspire pref,
                                       MapProfileBase mapLogic) {
        this.pref = pref;
        this.mapLogic = mapLogic;
        this.local = new LocalProfileDataStore(pref);
        this.remote = new RemoteProfileDataStoreAspire(pref, mapLogic);
    }

    @Override
    public Single<ProfileAspire> signInProfile(String accessToken, String secret, String userNameEmail) {
        return remote.signIn(accessToken, userNameEmail, secret)
                .flatMap(profileAspire -> logicProfile(profileAspire, secret, userNameEmail))
                .flatMap(profile -> saveProfilePref(profile).andThen(local.loadProfile()));
    }

    @Override
    public Single<ProfileAspire> signInNoSaveStorage(String accessToken, String secret, String userNameEmail) {
        return remote.signIn(accessToken, userNameEmail, secret)
                .flatMap(profileAspire -> logicProfile(profileAspire, secret, userNameEmail));
    }


    @Override
    public Single<ProfileAspire> saveProfileStorage(ProfileAspire profileAspire) {
        return saveProfilePref(profileAspire).andThen(local.loadProfile());
    }

    @Override
    public Single<ProfileAspire> loadProfile(String accessToken) {
        if (TextUtils.isEmpty(accessToken)) {
            return local.loadProfile();
        } else {
            return handleLoadProfileRemote(accessToken)
                    .flatMap(profile -> saveProfilePref(profile).andThen(local.loadProfile()));
        }
    }

    private Single<ProfileAspire> handleLoadProfileRemote(String accessToken) {
        return local.loadProfile()
                .flatMap(prLocal -> {
                    final String userNameEmail = prLocal.getEmail();
                    final String secret = prLocal.getSecretKeyDecrypt();

                    return remote.loadProfile(accessToken, userNameEmail, secret, true)
                            .flatMap(profileAspire -> logicProfile(profileAspire, secret, userNameEmail));
                });
    }

    private Single<ProfileAspire> logicProfile(ProfileAspire profileAspire, String password, String userNameEmail) {
        return mapLogic.mapResponse(profileAspire, password, userNameEmail);
    }

    /**
     * save profile and save preference data
     * ex: CityData
     * */
    private Completable saveProfilePref(ProfileAspire profileAspire){
        return local.saveProfile(profileAspire).andThen(mapLogic.handlePreferenceApp(pref, profileAspire));
    }
    /*
    1. local have profile
    2. same account (email local == email remote)
    */
    @Override
    public Completable refreshProfile(String accessToken) {
        return handleLoadProfileRemote(accessToken).zipWith(local.loadProfile(),
                (profileRemote, profileLocal) -> {
                    if (profileLocal == null || TextUtils.isEmpty(profileLocal.getEmail())
                            || !profileLocal.getEmail().equals(profileRemote.getEmail())) {
                        // do nothing save profile in local if user sign out or different email
                        throw new Exception("User empty (Sign Out)");
                    }
                    return profileRemote;
                })
                .flatMapCompletable(profileRemote ->{
                    Log.d("TAG", "refreshProfile: save");
                    return saveProfilePref(profileRemote);
                });
    }

    @Override
    public Completable saveProfile(String accessToken, ProfileAspire profile) {
        UpdateProfileAspireRequest updatedProfile = mapLogic.mapUpdate(pref, profile);
        return remote.saveProfile(accessToken, updatedProfile)
                .andThen(saveProfilePref(profile));
    }

    @Override
    public Completable createProfile(String serviceToken,
                                     CreateProfileAspireRequest createProfileRequest,
                                     ProfileAspire profile) {

        return remote.createProfile(serviceToken, createProfileRequest);
    }

}
