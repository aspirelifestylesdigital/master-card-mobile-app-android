package com.api.aspire.data.repository;

import com.api.aspire.data.datasource.RemoteChangeRecoveryQuestionDataStore;
import com.api.aspire.data.entity.recoveryquestion.RecoveryQuestionRequest;
import com.api.aspire.domain.repository.ChangeSecurityQuestionRepository;

import io.reactivex.Completable;

public class ChangeSecurityQuestionDataRepository implements ChangeSecurityQuestionRepository {

    RemoteChangeRecoveryQuestionDataStore remote;

    public ChangeSecurityQuestionDataRepository(RemoteChangeRecoveryQuestionDataStore remote) {
        this.remote = remote;
    }

    @Override
    public Completable changeSecurityQuestion(String userId, String question, String answer, String password) {
        return remote.changeRecoveryQuestion(userId, buildRecoveryQuestionRequest(question, answer, password));
    }

    private RecoveryQuestionRequest buildRecoveryQuestionRequest(String question, String answer, String password) {
        RecoveryQuestionRequest request = new RecoveryQuestionRequest();

        request.setPassword(new RecoveryQuestionRequest.Password(password));
        request.setRecoveryQuestion(new RecoveryQuestionRequest.RecoveryQuestion(question, answer));

        return request;
    }
}
