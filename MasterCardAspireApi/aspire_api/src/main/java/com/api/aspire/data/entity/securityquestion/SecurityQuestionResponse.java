package com.api.aspire.data.entity.securityquestion;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SecurityQuestionResponse {
    @SerializedName("questions")
    List<Question> questions;

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public static class Question {
        private String question;
        private String questionText;

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }

        public String getQuestionText() {
            return questionText;
        }

        public void setQuestionText(String questionText) {
            this.questionText = questionText;
        }
    }
}
