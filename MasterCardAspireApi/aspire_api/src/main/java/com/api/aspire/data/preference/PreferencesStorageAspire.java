package com.api.aspire.data.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.api.aspire.common.constant.SharedPrefAspireConstant;
import com.api.aspire.domain.model.AuthData;
import com.api.aspire.domain.model.ProfileAspire;
import com.google.gson.Gson;

import org.json.JSONException;


public class PreferencesStorageAspire {

    private Context context;
    private String jsonProfile;
    private String selectedCity;
    private Boolean hasForgotPwd;
    private String binCode;
    private String authToken;
    private String requestContentStore;

    public PreferencesStorageAspire(Context context) {
        this.context = context;
    }

    private PreferencesStorageAspire(Context context,
                                     String profile,
                                     String selectedCity,
                                     Boolean hasForgotPwd,
                                     String binCode,
                                     String authToken,
                                     String requestContentStore) {
        this.context = context;
        this.jsonProfile = profile;
        this.selectedCity = selectedCity;
        this.hasForgotPwd = hasForgotPwd;
        this.binCode = binCode;
        this.authToken = authToken;
        this.requestContentStore = requestContentStore;
    }

    public boolean profileCreated() {
        try {
            ProfileAspire profile = profile();
            return profile != null
                    && !TextUtils.isEmpty(profile.getEmail())
                    && !TextUtils.isEmpty(profile.getSecretKey());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public ProfileAspire profile() throws JSONException {
        SharedPreferences preferences = getInstance();
        return new ProfileAspire(
                preferences.getString(SharedPrefAspireConstant.PROFILE, "")
        );
    }

    public String selectedCity() {
        SharedPreferences preferences = getInstance();
        return preferences.getString(SharedPrefAspireConstant.SELECTED_CITY, "");
    }

    public boolean hasForgotPwd() {
        SharedPreferences preferences = getInstance();
        return preferences.getBoolean(SharedPrefAspireConstant.FORGOT_SECRET_KEY, false);
    }

    public String getBinCode() {
        SharedPreferences preferences = getInstance();
        return preferences.getString(SharedPrefAspireConstant.BIN_CODE, "");
    }

    public AuthData authToken() {
        SharedPreferences preferences = getInstance();
        String auth = preferences.getString(SharedPrefAspireConstant.AUTH_TOKEN, "");
        return new Gson().fromJson(auth, AuthData.class);
    }

    public String requestContentStore() {
        SharedPreferences preferences = getInstance();
        return preferences.getString(SharedPrefAspireConstant.REQUEST_CONTENT_STORE, "");
    }

    public void save() {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            throw new IllegalStateException("operation shouldn't have executed on main thread");
        }
        SharedPreferences preferences = getInstance();
        SharedPreferences.Editor editor = preferences.edit();
        if (jsonProfile != null) {
            editor.putString(SharedPrefAspireConstant.PROFILE, jsonProfile);
        }
        if (!TextUtils.isEmpty(selectedCity)) {
            editor.putString(SharedPrefAspireConstant.SELECTED_CITY, selectedCity);
        }
        if (hasForgotPwd != null) {
            editor.putBoolean(SharedPrefAspireConstant.FORGOT_SECRET_KEY, hasForgotPwd);
        }
        if (binCode != null) {
            editor.putString(SharedPrefAspireConstant.BIN_CODE, binCode);
        }
        if (authToken != null) {
            editor.putString(SharedPrefAspireConstant.AUTH_TOKEN, authToken);
        }
        if (requestContentStore != null) {
            editor.putString(SharedPrefAspireConstant.REQUEST_CONTENT_STORE, requestContentStore);
        }
        editor.commit();
    }

    public void saveAsync() {
        SharedPreferences preferences = getInstance();
        SharedPreferences.Editor editor = preferences.edit();
        if (jsonProfile != null) {
            editor.putString(SharedPrefAspireConstant.PROFILE, jsonProfile);
        }
        if (!TextUtils.isEmpty(selectedCity)) {
            editor.putString(SharedPrefAspireConstant.SELECTED_CITY, selectedCity);
        }
        if (hasForgotPwd != null) {
            editor.putBoolean(SharedPrefAspireConstant.FORGOT_SECRET_KEY, hasForgotPwd);
        }
        if (binCode != null) {
            editor.putString(SharedPrefAspireConstant.BIN_CODE, binCode);
        }
        if (authToken != null) {
            editor.putString(SharedPrefAspireConstant.AUTH_TOKEN, authToken);
        }
        if (requestContentStore != null) {
            editor.putString(SharedPrefAspireConstant.REQUEST_CONTENT_STORE, requestContentStore);
        }
        editor.apply();
    }

    public void clear() {
        SharedPreferences preferences = getInstance();
        preferences.edit().clear().apply();
    }

    private SharedPreferences getInstance() {
        return PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
    }

    public Editor editor() {
        return new Editor(context);
    }

    public static class Editor {
        private Context context;
        private String jsonProfile;
        private Boolean hasForgotPwd;
        private String binCode;
        private String selectedCity = "";
        private String authToken;
        private String requestContentStore;

        public Editor(Context context) {
            this.context = context;
        }

        public Editor profile(ProfileAspire val) throws JSONException {
            this.jsonProfile = val.toJSON().toString();
            return this;
        }

        public Editor selectedCity(String val) {
            this.selectedCity = val;
            return this;
        }

        public Editor hasForgotPwd(boolean val) {
            hasForgotPwd = val;
            return this;
        }

        public Editor binCode(String passCode) {
            this.binCode = passCode;
            return this;
        }

        public Editor authToken(String accessToken) {
            this.authToken = accessToken;
            return this;
        }

        public Editor requestContentStore(String requestContentStore) {
            this.requestContentStore = requestContentStore;
            return this;
        }

        public PreferencesStorageAspire build() {
            return new PreferencesStorageAspire(context, jsonProfile,
                    selectedCity, hasForgotPwd, binCode, authToken, requestContentStore);
        }
    }
}
