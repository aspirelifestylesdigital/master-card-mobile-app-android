package com.api.aspire.data.restapi;

import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireRequest;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;

public interface AspireConcierge {

    @Headers("Content-Type: application/json")
    @POST(".")
    Call<ConciergeCaseAspireResponse> createConciergeCase(@Header("Authorization") String auth,
                                                          @Header("X-App-Id") String xAppId,
                                                          @Header("X-Organization") String xOrganization,
                                                          @Body ConciergeCaseAspireRequest request);

    @Headers("Content-Type: application/json")
    @PUT(".")
    Call<ConciergeCaseAspireResponse> updateConciergeCase(@Header("Authorization") String auth, @Body ConciergeCaseAspireRequest request);

    @Headers("Content-Type: application/json")
    @DELETE(".")
    Call<ConciergeCaseAspireResponse> deleteConciergeCase(@Header("Authorization") String auth, @Body ConciergeCaseAspireRequest request);
}
