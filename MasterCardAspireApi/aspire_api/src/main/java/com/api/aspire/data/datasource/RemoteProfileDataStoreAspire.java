package com.api.aspire.data.datasource;

import android.text.TextUtils;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.data.entity.oauth.GetTokenAspireResponse;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.CreateProfileAspireResponse;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.data.entity.profile.UpdateProfileAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.retro2client.AppHttpClient;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.MapProfileBase;
import com.google.gson.Gson;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import retrofit2.Call;
import retrofit2.Response;

public class RemoteProfileDataStoreAspire {

    private PreferencesStorageAspire preferencesStorageAspire;
    private MapProfileBase mapLogic;

    public RemoteProfileDataStoreAspire(PreferencesStorageAspire pref,
                                        MapProfileBase mapLogic) {
        this.preferencesStorageAspire = pref;
        this.mapLogic = mapLogic;
    }

    /**
     * Sign in always get new access token -> don't refresh token
     * */
    public Single<ProfileAspire> signIn(String accessToken,
                                        String username,
                                        String secret) {
        return loadProfile(accessToken, username, secret, false);
    }

    /**
     * call api get retrieve profile
     * ErrorApi.isAccessTokenExpired == true
     * call api refresh get new token and call api get retrieve profile second time
     */
    public Single<ProfileAspire> loadProfile(String accessToken,
                                             String username,
                                             String secret,
                                             boolean shouldRefreshToken) {
        return Single.create(e -> {
            Call<ProfileAspireResponse> call = requestApiRetrieveProfile(accessToken);
            Response<ProfileAspireResponse> response = call.execute();
            if (!response.isSuccessful()) {
                String error = response.errorBody().string();
                if(shouldRefreshToken &&
                        ErrorApi.isAccessTokenExpired(error)){
                    refreshTokenAndCallGetProfileSecond(username, secret, e);
                }else{
                    e.onError(new Exception(error));
                }
                return;
            }

            ProfileAspireResponse responseBody = response.body();
            if (responseBody != null) {
                ProfileAspire profile = new ProfileAspire(new Gson().toJson(responseBody));
                e.onSuccess(profile);
            } else {
                e.onError(new Exception(response.message()));
            }
        });
    }

    /**
     * 1. call getToken refresh new token
     * 2. save token storage
     * 3. get profile with new access token
     * */
    private void refreshTokenAndCallGetProfileSecond(String username, String secretKey, SingleEmitter<ProfileAspire> e) throws Exception {
        GetToken getTokenUseCase = new GetToken(preferencesStorageAspire, mapLogic);
        //-- call api get new token
        Call<GetTokenAspireResponse> requestTokenCall = getTokenUseCase.requestApiGetTokenUser(username, secretKey);

        Response<GetTokenAspireResponse> tokenResponse = requestTokenCall.execute();
        if(!tokenResponse.isSuccessful()){
            e.onError(new Exception(tokenResponse.errorBody().string()));
            return;
        }

        String accessTokenRefresh = tokenResponse.body().getAccessToken();
        String tokenType = tokenResponse.body().getTokenType();

        if(TextUtils.isEmpty(accessTokenRefresh) || TextUtils.isEmpty(tokenType)){
            e.onError(new Exception(tokenResponse.body().toString()));
        }else{
            //-- save token storage
            String tokenRefresh = getTokenUseCase.createTokenAndSaveStorage(accessTokenRefresh, tokenType);

            //-- call api get profile second time
            Response<ProfileAspireResponse> responseSecond = requestApiRetrieveProfile(tokenRefresh).execute();

            if(!responseSecond.isSuccessful()){
                e.onError(new Exception(responseSecond.errorBody().string()));
                return;
            }
            ProfileAspireResponse responseBodySecond = responseSecond.body();
            if (responseBodySecond != null) {
                ProfileAspire profile = new ProfileAspire(new Gson().toJson(responseBodySecond));
                e.onSuccess(profile);
            } else {
                e.onError(new Exception(responseSecond.message()));
            }
        }
    }

    private Call<ProfileAspireResponse> requestApiRetrieveProfile(String accessToken) {
        return AppHttpClient.getInstance()
                .getAspireProfileApi()
                .retrieveProfile(accessToken, mapLogic.getXAppId(), mapLogic.getXOrganization());
    }

    public Completable saveProfile(String accessToken, UpdateProfileAspireRequest updateProfileRequest) {
        return Completable.create(e -> {

            Call<UpdateProfileAspireResponse> call = AppHttpClient.getInstance()
                    .getAspireProfileApi()
                    .updateProfile(accessToken, mapLogic.getXAppId(), mapLogic.getXOrganization(), updateProfileRequest);

            Response<UpdateProfileAspireResponse> response = call.execute();
            if (!response.isSuccessful()) {
                e.onError(new BackendException(response.errorBody().string()));
                return;
            }

            UpdateProfileAspireResponse updateProfileAspireResponse = response.body();
            if (updateProfileAspireResponse != null && updateProfileAspireResponse.isSuccess()) {
                e.onComplete();
            } else {
                e.onError(new Exception(response.message()));
            }
        });
    }

    public Completable createProfile(String serviceToken, CreateProfileAspireRequest createProfileRequest) {
        return Completable.create(e -> {

            Call<CreateProfileAspireResponse> call = AppHttpClient.getInstance()
                    .getAspireCreateProfileApi()
                    .createCustomer(serviceToken, mapLogic.getXAppId(), mapLogic.getXOrganization(), createProfileRequest);

            Response<CreateProfileAspireResponse> response = call.execute();
            if (!response.isSuccessful()) {

                String errorContent = response.errorBody().string();
                if (response.code() == 400) {
                    if(isExistsUser(errorContent)){
                        errorContent = ErrCode.USER_EXISTS_ERROR.name();
                    }else if(isParamSecretError(errorContent)){
                        errorContent = ErrCode.CREATE_ACCOUNT_ASPIRE_PARAM_ERROR.name();
                    }
                }
                e.onError(new Exception(errorContent));
                return;
            }

            CreateProfileAspireResponse responseBody = response.body();
            final String repSuccess = "The new customer account is registered successfully";
            if (responseBody != null && repSuccess.equalsIgnoreCase(responseBody.message)) {
                e.onComplete();
            } else {
                e.onError(new Exception(response.message()));
            }
        });
    }

    /*
    {
      "errorCode": "ACE_BADREQUEST_016",
      "message": "Matching party found"
    }
    */
    private boolean isExistsUser(String errorMessage) {
        final String accountExists = "An object with this field already exists";

        return !TextUtils.isEmpty(errorMessage) && (errorMessage.contains(accountExists)
                || errorMessage.contains("ACE_BADREQUEST_008")
                || errorMessage.contains("ACE_BADREQUEST_016"));
    }

    /*
    {
      "errorCode": "ACE_BADREQUEST_000",
      "message": "Bad request exception at PMA API: string 'a' is too short (length: 1, required minimum: 6) for idamAccount/password"
    }
    */
    private boolean isParamSecretError(String errorMessage) {
        final String valueSecret = "password";

        return !TextUtils.isEmpty(errorMessage) && (errorMessage.contains(valueSecret)
                || errorMessage.contains("ACE_BADREQUEST_000"));
    }
}
