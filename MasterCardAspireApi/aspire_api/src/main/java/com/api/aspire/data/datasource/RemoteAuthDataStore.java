package com.api.aspire.data.datasource;


import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.entity.passcode.PassCodeRequest;
import com.api.aspire.data.entity.passcode.PassCodeResponse;
import com.api.aspire.data.retro2client.AppHttpClient;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Response;

public class RemoteAuthDataStore {

    public Single<Boolean> checkPassCode(String serviceToken, String xAppId, String xOrganization, int passCode) {
        return Single.create(e -> {

            PassCodeRequest passCodeRequest = new PassCodeRequest(passCode);

            Call<PassCodeResponse> request = AppHttpClient
                    .getInstance()
                    .getAspirePassCodeApi()
                    .checkPassCode(serviceToken, xAppId, xOrganization, passCodeRequest);

            Response<PassCodeResponse> response = request.execute();
            if (response != null && response.isSuccessful()) {
                boolean result = "success".equalsIgnoreCase(response.body().getResult());
                if (result) {
                    e.onSuccess(true);
                } else {
                    e.onError(new Exception(ErrCode.PASS_CODE_ERROR.name()));
                }
            } else {
                String errorApi = ErrCode.PASS_CODE_ERROR.name();
                if (response != null && response.errorBody() != null) {
                    errorApi = response.errorBody().string();
                }
                e.onError(new Exception(errorApi));
            }
        });
    }
}
