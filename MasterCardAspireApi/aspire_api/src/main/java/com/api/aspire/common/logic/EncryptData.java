package com.api.aspire.common.logic;

import android.text.TextUtils;
import android.util.Base64;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/*
* https://www.novixys.com/blog/java-aes-example/
* https://www.veracode.com/blog/research/encryption-and-decryption-java-cryptography
* https://stackoverflow.com/a/13580503
* */
public final class EncryptData {

    private final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private final String KEY = "FPTLb4YpWq"; //-- random value

    private static class EncryptDataHelper {
        private static final EncryptData INSTANCE = new EncryptData();
    }

    private EncryptData() {

    }

    public static EncryptData getInstance() {
        return EncryptDataHelper.INSTANCE;
    }

    public String encrypt(String valueData){
        if(TextUtils.isEmpty(valueData)){
            return "";
        }
        try {
            IvParameterSpec iv = getIvParameterSpec();
            SecretKeySpec key = generateKey();
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
            byte[] encVal = cipher.doFinal(valueData.getBytes("UTF-8"));
            return Base64.encodeToString(encVal, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String decrypt(String valueEncrypt){
        if(TextUtils.isEmpty(valueEncrypt)){
            return "";
        }
        try {
            IvParameterSpec iv = getIvParameterSpec();
            SecretKeySpec key = generateKey();
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
            byte[] decryptedValue64 = Base64.decode(valueEncrypt, Base64.DEFAULT);
            byte[] decryptedByteValue = cipher.doFinal(decryptedValue64);
            return new String(decryptedByteValue, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private IvParameterSpec getIvParameterSpec() {
        byte[] empty = new byte[16];
        return new IvParameterSpec(empty);
    }

    private SecretKeySpec generateKey() throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = KEY.getBytes("UTF-8");
        digest.update(bytes, 0, bytes.length);
        byte[] key = digest.digest();
        return new SecretKeySpec(key, ALGORITHM);
    }



}
