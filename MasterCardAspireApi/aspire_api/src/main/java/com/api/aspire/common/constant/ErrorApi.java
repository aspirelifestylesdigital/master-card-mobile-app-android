package com.api.aspire.common.constant;

import android.text.TextUtils;

public final class ErrorApi {

    /**
     * Error 1: {"error":"invalid_grant","error_description":"The credentials provided were invalid."}
     * Error 2:
     * {
     * "errorCode": "ACE_BADREQUEST_000",
     * "message": "Bad request exception at PMA API"
     * }
     */
    public static boolean isGetTokenError(String errorMessage) {
        return (!TextUtils.isEmpty(errorMessage) &&
                (errorMessage.contains("The credentials provided were invalid.")
                        || errorMessage.contains("Bad request exception at PMA API")
                        || ErrCode.PROFILE_ERROR.name().equalsIgnoreCase(errorMessage)
                )
        );
    }

    /**
    {
    "errorCode": "E0000014",
    "errorSummary": "Update of credentials failed",
    "errorLink": "E0000014",
    "errorId": "oaeLXPXKJ6LS2yuPo6Tn1RpNQ",
    "errorCauses": [
        {
            "errorSummary": "Password has been used too recently"
        }
    ]
}
{
    "errorCode": "E0000014",
    "errorSummary": "Update of credentials failed",
    "errorLink": "E0000014",
    "errorId": "oaeUpeZDbJERLGzWURJ3UhKBw",
    "errorCauses": [
        {
            "errorSummary": "Password cannot be your current password"
        }
    ]
}
{
	"errorCode": "E0000014",
	"errorSummary": "Update of credentials failed",
	"errorLink": "E0000014",
	"errorId": "oae4B6WYfIFR8KIuFY4WXUH8Q",
	"errorCauses": [{
			"errorSummary": "Password requirements were not met. Password requirements: at least 10 characters, a lowercase letter, an uppercase letter, a number, a symbol, no parts of your username, does not include your first name, does not include your last name. Your password cannot be any of your last 12 passwords."
		}
	]
}
    * */
    public static boolean isChangePasswordWrong(String errorMessage){
        return (!TextUtils.isEmpty(errorMessage) &&
                ( errorMessage.contains("E0000014")
                        || errorMessage.contains("Password has been used too recently")
                        || errorMessage.contains("Password cannot be your current password")
                        || errorMessage.contains("Password requirements were not met")
                )
        );
    }

    public static boolean isChangePasswordNotMatch(String errorMessage) {
        return (!TextUtils.isEmpty(errorMessage) &&
                (errorMessage.contains("Old Password is not correct")
                )
        );
    }

    /*
{
  "errorCode":"ACE_UNAUTHORISED_001",
  "message":"Policy 348312: Access token is either invalid or has expired."
}
    */
    public static boolean isAccessTokenExpired(String errorMessage) {
        return (!TextUtils.isEmpty(errorMessage) &&
                (errorMessage.contains("ACE_UNAUTHORISED_001") ||
                        errorMessage.contains("Access token is either invalid or has expired")
                )
        );
    }

}
