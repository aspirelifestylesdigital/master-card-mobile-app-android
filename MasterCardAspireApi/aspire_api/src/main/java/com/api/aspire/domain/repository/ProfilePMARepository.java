package com.api.aspire.domain.repository;

import com.api.aspire.data.entity.userprofile.pma.ProfilePMAResponse;
import com.api.aspire.data.entity.userprofile.pma.SearchProfilePMAResponse;

import io.reactivex.Single;

public interface ProfilePMARepository {

    Single<SearchProfilePMAResponse> searchProfilePMA(String tokenPMA, String email);

    Single<ProfilePMAResponse> getProfilePMA(String tokenPMA, String partyId);

}
