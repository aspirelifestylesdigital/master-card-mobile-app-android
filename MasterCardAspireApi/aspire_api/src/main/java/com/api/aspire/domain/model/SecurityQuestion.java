package com.api.aspire.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SecurityQuestion implements Parcelable {
    private String question;
    private String questionText;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.question);
        dest.writeString(this.questionText);
    }

    public SecurityQuestion() {
    }

    protected SecurityQuestion(Parcel in) {
        this.question = in.readString();
        this.questionText = in.readString();
    }

    public static final Creator<SecurityQuestion> CREATOR = new Creator<SecurityQuestion>() {
        @Override
        public SecurityQuestion createFromParcel(Parcel source) {
            return new SecurityQuestion(source);
        }

        @Override
        public SecurityQuestion[] newArray(int size) {
            return new SecurityQuestion[size];
        }
    };
}
