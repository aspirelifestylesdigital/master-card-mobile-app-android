package com.api.aspire.data.repository;

import com.api.aspire.data.datasource.LocalSecurityQuestionsDataStore;
import com.api.aspire.data.entity.securityquestion.SecurityQuestionResponse;
import com.api.aspire.domain.model.SecurityQuestion;
import com.api.aspire.domain.repository.SecurityQuestionRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

public class SecurityQuestionDataRepository implements SecurityQuestionRepository {

    @Override
    public Single<List<SecurityQuestion>> getSecurityQuestions() {
        LocalSecurityQuestionsDataStore local = new LocalSecurityQuestionsDataStore();

        return local.getSecurityQuestions()
                .map(questionsResponse -> {
                    List<SecurityQuestion> securityQuestions = new ArrayList<>();
                    for (int i = 0; i < questionsResponse.getQuestions().size(); i++) {
                        SecurityQuestionResponse.Question question = questionsResponse.getQuestions().get(i);
                        SecurityQuestion securityQuestion = new SecurityQuestion();
                        securityQuestion.setQuestion(question.getQuestion());
                        securityQuestion.setQuestionText(question.getQuestionText());
                        securityQuestions.add(securityQuestion);
                    }
                    return securityQuestions;
                });
    }
}
