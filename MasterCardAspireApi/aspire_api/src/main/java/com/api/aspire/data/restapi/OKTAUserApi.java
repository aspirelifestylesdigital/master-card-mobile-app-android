package com.api.aspire.data.restapi;


import com.api.aspire.BuildConfig;
import com.api.aspire.data.entity.forgotpassword.ForgotPasswordRequest;
import com.api.aspire.data.entity.forgotpassword.ForgotPasswordResponse;
import com.api.aspire.data.entity.profile.ChangePasswordOKTARequest;
import com.api.aspire.data.entity.profile.ChangePasswordOKTAResponse;
import com.api.aspire.data.entity.recoveryquestion.RecoveryQuestionRequest;
import com.api.aspire.data.entity.recoveryquestion.RecoveryQuestionResponse;
import com.api.aspire.data.entity.userprofile.UserProfileResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface OKTAUserApi {

    @Headers({"Authorization: " + BuildConfig.WS_OKTA_TOKEN_AUTHORIZATION, "Content-Type: application/json"})
    @POST("{email}/credentials/change_password")
    Call<ChangePasswordOKTAResponse> changePassword(@Path("email") String email,
                                                    @Body ChangePasswordOKTARequest body);

//    @Headers({"Authorization: " + BuildConfig.WS_OKTA_TOKEN_AUTHORIZATION, "Content-Type: application/json"})
//    @POST("{email}/lifecycle/reset_password?sendEmail=true")
//    Call<ResponseBody> forgotPassword(@Path("email") String email);

    @Headers({"Authorization: " + BuildConfig.WS_OKTA_TOKEN_AUTHORIZATION, "Content-Type: application/json"})
    @GET("{email}")
    Call<UserProfileResponse> getProfile(@Path("email") String email);

    @Headers({"Authorization: " + BuildConfig.WS_OKTA_TOKEN_AUTHORIZATION, "Content-Type: application/json"})
    @POST("{userId}/credentials/change_recovery_question")
    Call<RecoveryQuestionResponse> changeRecoveryQuestion(@Path("userId") String userId,
                                                          @Body RecoveryQuestionRequest recoveryQuestionRequest);

    @Headers({"Authorization: " + BuildConfig.WS_OKTA_TOKEN_AUTHORIZATION, "Content-Type: application/json"})
    @POST("{email}/credentials/forgot_password?sendEmail=true")
    Call<ForgotPasswordResponse> forgotPassword(@Path("email") String email, @Body ForgotPasswordRequest request);
}