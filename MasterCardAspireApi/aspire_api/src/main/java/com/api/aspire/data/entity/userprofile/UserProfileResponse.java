package com.api.aspire.data.entity.userprofile;

import com.api.aspire.domain.model.UserProfileOKTA;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserProfileResponse {

    @Expose
    @SerializedName("credentials")
    private UserProfileOKTA.Credentials credentials;
    @Expose
    @SerializedName("profile")
    private UserProfileOKTA.Profile profile;
    @Expose
    @SerializedName("passwordChanged")
    private String passwordchanged;
    @Expose
    @SerializedName("lastUpdated")
    private String lastupdated;
    @Expose
    @SerializedName("lastLogin")
    private String lastlogin;
    @Expose
    @SerializedName("statusChanged")
    private String statuschanged;
    @Expose
    @SerializedName("activated")
    private String activated;
    @Expose
    @SerializedName("created")
    private String created;
    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("id")
    private String id;

    public UserProfileResponse(UserProfileOKTA.Credentials credentials, UserProfileOKTA.Profile profile, String passwordchanged, String lastupdated, String lastlogin, String statuschanged, String activated, String created, String status, String id) {
        this.credentials = credentials;
        this.profile = profile;
        this.passwordchanged = passwordchanged;
        this.lastupdated = lastupdated;
        this.lastlogin = lastlogin;
        this.statuschanged = statuschanged;
        this.activated = activated;
        this.created = created;
        this.status = status;
        this.id = id;
    }

    public UserProfileOKTA.Credentials getCredentials() {
        return credentials;
    }

    public void setCredentials(UserProfileOKTA.Credentials credentials) {
        this.credentials = credentials;
    }

    public UserProfileOKTA.Profile getProfile() {
        return profile;
    }

    public void setProfile(UserProfileOKTA.Profile profile) {
        this.profile = profile;
    }

    public String getPasswordchanged() {
        return passwordchanged;
    }

    public void setPasswordchanged(String passwordchanged) {
        this.passwordchanged = passwordchanged;
    }

    public String getLastupdated() {
        return lastupdated;
    }

    public void setLastupdated(String lastupdated) {
        this.lastupdated = lastupdated;
    }

    public String getLastlogin() {
        return lastlogin;
    }

    public void setLastlogin(String lastlogin) {
        this.lastlogin = lastlogin;
    }

    public String getStatuschanged() {
        return statuschanged;
    }

    public void setStatuschanged(String statuschanged) {
        this.statuschanged = statuschanged;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static class Links {
        @Expose
        @SerializedName("deactivate")
        private UserProfileOKTA.Deactivate deactivate;
        @Expose
        @SerializedName("changePassword")
        private UserProfileOKTA.Changepassword changepassword;
        @Expose
        @SerializedName("self")
        private UserProfileOKTA.Self self;
        @Expose
        @SerializedName("changeRecoveryQuestion")
        private UserProfileOKTA.Changerecoveryquestion changerecoveryquestion;
        @Expose
        @SerializedName("expirePassword")
        private UserProfileOKTA.Expirepassword expirepassword;
        @Expose
        @SerializedName("forgotPassword")
        private UserProfileOKTA.Forgotpassword forgotpassword;
        @Expose
        @SerializedName("resetPassword")
        private UserProfileOKTA.Resetpassword resetpassword;
        @Expose
        @SerializedName("suspend")
        private UserProfileOKTA.Suspend suspend;

        public UserProfileOKTA.Deactivate getDeactivate() {
            return deactivate;
        }

        public void setDeactivate(UserProfileOKTA.Deactivate deactivate) {
            this.deactivate = deactivate;
        }

        public UserProfileOKTA.Changepassword getChangepassword() {
            return changepassword;
        }

        public void setChangepassword(UserProfileOKTA.Changepassword changepassword) {
            this.changepassword = changepassword;
        }

        public UserProfileOKTA.Self getSelf() {
            return self;
        }

        public void setSelf(UserProfileOKTA.Self self) {
            this.self = self;
        }

        public UserProfileOKTA.Changerecoveryquestion getChangerecoveryquestion() {
            return changerecoveryquestion;
        }

        public void setChangerecoveryquestion(UserProfileOKTA.Changerecoveryquestion changerecoveryquestion) {
            this.changerecoveryquestion = changerecoveryquestion;
        }

        public UserProfileOKTA.Expirepassword getExpirepassword() {
            return expirepassword;
        }

        public void setExpirepassword(UserProfileOKTA.Expirepassword expirepassword) {
            this.expirepassword = expirepassword;
        }

        public UserProfileOKTA.Forgotpassword getForgotpassword() {
            return forgotpassword;
        }

        public void setForgotpassword(UserProfileOKTA.Forgotpassword forgotpassword) {
            this.forgotpassword = forgotpassword;
        }

        public UserProfileOKTA.Resetpassword getResetpassword() {
            return resetpassword;
        }

        public void setResetpassword(UserProfileOKTA.Resetpassword resetpassword) {
            this.resetpassword = resetpassword;
        }

        public UserProfileOKTA.Suspend getSuspend() {
            return suspend;
        }

        public void setSuspend(UserProfileOKTA.Suspend suspend) {
            this.suspend = suspend;
        }
    }

    public static class Deactivate {
        @Expose
        @SerializedName("method")
        private String method;
        @Expose
        @SerializedName("href")
        private String href;

        public Deactivate(String method, String href) {
            this.method = method;
            this.href = href;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Changepassword {
        @Expose
        @SerializedName("method")
        private String method;
        @Expose
        @SerializedName("href")
        private String href;

        public Changepassword(String method, String href) {
            this.method = method;
            this.href = href;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Self {
        @Expose
        @SerializedName("href")
        private String href;

        public Self(String href) {
            this.href = href;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Changerecoveryquestion {
        @Expose
        @SerializedName("method")
        private String method;
        @Expose
        @SerializedName("href")
        private String href;

        public Changerecoveryquestion(String method, String href) {
            this.method = method;
            this.href = href;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Expirepassword {
        @Expose
        @SerializedName("method")
        private String method;
        @Expose
        @SerializedName("href")
        private String href;

        public Expirepassword(String method, String href) {
            this.method = method;
            this.href = href;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Forgotpassword {
        @Expose
        @SerializedName("method")
        private String method;
        @Expose
        @SerializedName("href")
        private String href;

        public Forgotpassword(String method, String href) {
            this.method = method;
            this.href = href;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Resetpassword {
        @Expose
        @SerializedName("method")
        private String method;
        @Expose
        @SerializedName("href")
        private String href;

        public Resetpassword(String method, String href) {
            this.method = method;
            this.href = href;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Suspend {
        @Expose
        @SerializedName("method")
        private String method;
        @Expose
        @SerializedName("href")
        private String href;

        public Suspend(String method, String href) {
            this.method = method;
            this.href = href;
        }

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getHref() {
            return href;
        }

        public void setHref(String href) {
            this.href = href;
        }
    }

    public static class Credentials {
        @Expose
        @SerializedName("provider")
        private UserProfileOKTA.Provider provider;
        @Expose
        @SerializedName("recovery_question")
        private UserProfileOKTA.RecoveryQuestion recoveryQuestion;
        @Expose
        @SerializedName("password")
        private UserProfileOKTA.Password password;

        public Credentials(UserProfileOKTA.Provider provider, UserProfileOKTA.RecoveryQuestion recoveryQuestion, UserProfileOKTA.Password password) {
            this.provider = provider;
            this.recoveryQuestion = recoveryQuestion;
            this.password = password;
        }

        public UserProfileOKTA.Provider getProvider() {
            return provider;
        }

        public void setProvider(UserProfileOKTA.Provider provider) {
            this.provider = provider;
        }

        public UserProfileOKTA.RecoveryQuestion getRecoveryQuestion() {
            return recoveryQuestion;
        }

        public void setRecoveryQuestion(UserProfileOKTA.RecoveryQuestion recoveryQuestion) {
            this.recoveryQuestion = recoveryQuestion;
        }

        public UserProfileOKTA.Password getPassword() {
            return password;
        }

        public void setPassword(UserProfileOKTA.Password password) {
            this.password = password;
        }
    }

    public static class Provider {
        @Expose
        @SerializedName("name")
        private String name;
        @Expose
        @SerializedName("type")
        private String type;

        public Provider(String name, String type) {
            this.name = name;
            this.type = type;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class RecoveryQuestion {
        @Expose
        @SerializedName("question")
        private String question;

        public String getQuestion() {
            return question;
        }

        public void setQuestion(String question) {
            this.question = question;
        }
    }

    public static class Password {
    }

    public static class Profile {
        @Expose
        @SerializedName("email")
        private String email;
        @Expose
        @SerializedName("login")
        private String login;
        @Expose
        @SerializedName("partyId")
        private String partyid;
        @Expose
        @SerializedName("userType")
        private String usertype;
        @Expose
        @SerializedName("organization")
        private String organization;
        @Expose
        @SerializedName("lastName")
        private String lastname;
        @Expose
        @SerializedName("firstName")
        private String firstname;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPartyid() {
            return partyid;
        }

        public void setPartyid(String partyid) {
            this.partyid = partyid;
        }

        public String getUsertype() {
            return usertype;
        }

        public void setUsertype(String usertype) {
            this.usertype = usertype;
        }

        public String getOrganization() {
            return organization;
        }

        public void setOrganization(String organization) {
            this.organization = organization;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }
    }


    public static class UserProfileResponseBuilder {
        private UserProfileOKTA.Credentials credentials;
        private UserProfileOKTA.Profile profile;
        private String passwordchanged;
        private String lastupdated;
        private String lastlogin;
        private String statuschanged;
        private String activated;
        private String created;
        private String status;
        private String id;

        public UserProfileResponseBuilder setCredentials(UserProfileOKTA.Credentials credentials) {
            this.credentials = credentials;
            return this;
        }

        public UserProfileResponseBuilder setProfile(UserProfileOKTA.Profile profile) {
            this.profile = profile;
            return this;
        }

        public UserProfileResponseBuilder setPasswordchanged(String passwordchanged) {
            this.passwordchanged = passwordchanged;
            return this;
        }

        public UserProfileResponseBuilder setLastupdated(String lastupdated) {
            this.lastupdated = lastupdated;
            return this;
        }

        public UserProfileResponseBuilder setLastlogin(String lastlogin) {
            this.lastlogin = lastlogin;
            return this;
        }

        public UserProfileResponseBuilder setStatuschanged(String statuschanged) {
            this.statuschanged = statuschanged;
            return this;
        }

        public UserProfileResponseBuilder setActivated(String activated) {
            this.activated = activated;
            return this;
        }

        public UserProfileResponseBuilder setCreated(String created) {
            this.created = created;
            return this;
        }

        public UserProfileResponseBuilder setStatus(String status) {
            this.status = status;
            return this;
        }

        public UserProfileResponseBuilder setId(String id) {
            this.id = id;
            return this;
        }

        public UserProfileResponse createUserProfileResponse() {
            return new UserProfileResponse(credentials, profile, passwordchanged, lastupdated, lastlogin, statuschanged, activated, created, status, id);
        }
    }
}
