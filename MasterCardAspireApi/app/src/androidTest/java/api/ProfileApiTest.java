package api;

import android.content.Context;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.util.Log;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.common.exception.BackendException;
import com.api.aspire.common.logic.EncryptData;
import com.api.aspire.data.datasource.RemoteChangeRecoveryQuestionDataStore;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AspireConciergeRepository;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.data.repository.ChangeSecurityQuestionDataRepository;
import com.api.aspire.data.repository.ForgotPasswordDataRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.data.repository.ProfileDataPMARepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.repository.AuthAspireRepository;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.api.aspire.domain.usecases.AskConciergeAspire;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.api.aspire.domain.usecases.CheckPassCode;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetProfilePMA;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.ResetPassword;
import com.api.aspire.domain.usecases.RetrievePassword;
import com.api.aspire.domain.usecases.SaveProfile;
import com.api.aspire.domain.usecases.SignInCase;
import com.api.aspire.domain.usecases.SignUpCase;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.BuildConfig;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.logic.Validator;
import com.mastercard.androidapp.domain.mapper.profile.MapProfile;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.reactivex.Completable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

import static junit.framework.Assert.assertTrue;


/**
 * JUnit4 unit tests for logic.
 */
@RunWith(AndroidJUnit4.class)
public class ProfileApiTest {

    private String TAG = "ProfileApiTest";
    private LoadProfile loadProfile;
    private MapProfile mapProfile;
    private MapProfileApp mapProfileApp;
    private GetAccessToken getAccessToken;
    private SaveProfile saveProfile;
    private ResetPassword changePassword;
    private RetrievePassword retrievePassword;
    private CheckPassCode checkPassCode;
    private AskConciergeAspire askConciergeApi;
    private GetToken getToken;

    private Context context;

    private boolean rsTest;
    private CreateProfileCase createProfileCase;
    private ChangeSecurityQuestion useCaseSecurityQuestion;
    private SignInCase signInCase;
    private SignUpCase signUpCase;
//    private GetUserApp getUserApp;

    private final int BIN_CODE_VALID = BuildConfig.AS_BIN;
    private String email = "tictacpcudaaspire1@gmail.com";
    private String pwd = "Admin@1234";

    /*list account: tictacpcuda15@gmail.com */

    @Before
    public void setUp() {
        this.context = App.getInstance().getApplicationContext();
        this.mapProfileApp = new MapProfileApp();
        this.mapProfile = new MapProfile();
        PreferencesStorageAspire preferencesStorageAspire = new PreferencesStorageAspire(context);
        ProfileAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorageAspire, mapProfileApp);
        UserProfileOKTADataRepository userProfileOKTADataRepository = new UserProfileOKTADataRepository(new RemoteUserProfileOKTADataStore());
        this.useCaseSecurityQuestion = new ChangeSecurityQuestion(userProfileOKTADataRepository,
                new ChangeSecurityQuestionDataRepository(new RemoteChangeRecoveryQuestionDataStore()),
                mapProfileApp);
        this.getToken = new GetToken(preferencesStorageAspire, mapProfileApp);
        signInCase = new SignInCase(mapProfileApp, profileRepository, userProfileOKTADataRepository, getToken);
        this.createProfileCase = new CreateProfileCase(profileRepository,
                useCaseSecurityQuestion,
                signInCase,
                getToken);
        this.loadProfile = new LoadProfile(profileRepository);
        this.getAccessToken = new GetAccessToken(loadProfile, getToken);
        this.saveProfile = new SaveProfile(profileRepository);
        this.changePassword = new ResetPassword(mapProfileApp, preferencesStorageAspire, loadProfile);
        this.retrievePassword = new RetrievePassword(mapProfileApp, new ForgotPasswordDataRepository(), userProfileOKTADataRepository);
        AuthAspireRepository repository = new AuthDataAspireRepository();
        this.checkPassCode = new CheckPassCode(mapProfileApp, preferencesStorageAspire, repository,
                getAccessToken, loadProfile, saveProfile, getToken);

        //Request Concierge case
        AspireConciergeRepository aspireConciergeRepository = new AspireConciergeRepository();
        this.askConciergeApi = new AskConciergeAspire(mapProfileApp, aspireConciergeRepository,
                getToken, loadProfile);

        signUpCase = new SignUpCase(mapProfileApp,
                new RemoteUserProfileOKTADataStore(),
                new GetProfilePMA(mapProfileApp),
                new ProfileDataPMARepository(mapProfileApp)
        );

    }

    @Test
    public void splashScreenTokenExpiredRefreshNewToken() {
        rsTest = false;

        email = "tictacmctest1001@gmail.com";

        loadProfile.buildUseCaseSingle("Bearer eyJraWQiOiIzT0JsS1l5TGEzWk1rMmVHb2l2YjVGa0VLcmVONC03Vl9JS01lZ3VESVo0IiwiYWxnIjoiUlMyNTYifQ.eyJ2ZXIiOjEsImp0aSI6IkFULnA1QUFCanBBS2RmUVZndHV1MWZXeTFSN3FYcTJnN1R4Z3RtWjZwTE1uX3ciLCJpc3MiOiJodHRwczovL2FzcGlyZS5va3RhcHJldmlldy5jb20iLCJhdWQiOiJodHRwczovL2FzcGlyZS5va3RhcHJldmlldy5jb20iLCJzdWIiOiJNQ0RfdGljdGFjbWN0ZXN0MTAwMkBnbWFpbC5jb20iLCJpYXQiOjE1MzczMjY3MjUsImV4cCI6MTUzNzMzMDMyNSwiY2lkIjoiMG9hZWFudmt6ZXZ3TWpYQVkwaDciLCJ1aWQiOiIwMHVnYXFxbHA5UmszNTNWZzBoNyIsInNjcCI6WyJvcGVuaWQiXX0.SYQFXeHPJpN4DeIYopJFsLhSxX8pnneduVUp92fihd9cpB5gATHgLJPWMXmcdLR8rJaDD0D3HzJf95zzRe-Gm53egzccXsH1WXs2eCYEfABzp9B27D0uoswjD_pYY7iFhMC2tgBuJXHp3o43f0IWDRLp5smwNoRm65x2kNoXlyMoKLau3RPu0oDVgaLPoRq20o_z6BpetM5wVZOvEECMj2S4uDCCjcE9VLb17TJ82-JQ4QXscOY2zHIp86fMejthjQXwRRMHcGGefeii7a6vDA_dLFDqOBoW-T5AgJALhNv1x_PbY_FRnHyT-262_m_MgsvuTGChmjYDH0hBammRvA")
                .flatMapCompletable(profileAspire -> Completable.complete())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "//---------- onComplete: splashScreenTokenExpiredRefreshNewToken");
                        rsTest = true;
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "//---------- onError: " + e.getMessage());
                        rsTest = false;
                        dispose();
                    }
                });
        //result
        assertTrue(rsTest);
    }

    //<editor-fold desc="Sign In">

    @Test
    public void signInSaveProfileStorageReturnSuccess() {
        rsTest = false;

        email = "tictacmctest1002@gmail.com";

        SignInCase.Params params = new SignInCase.Params(email, pwd);

        signInCase.getProfile(params).toCompletable()
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "//---------- onComplete: Sign In done");
                        rsTest = true;
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d(TAG, "//---------- onError: " + e.getMessage());
                        rsTest = false;
                        dispose();
                    }
                });
        //result
        assertTrue(rsTest);
    }

    /**
     * Sign In for project BDA, MDA
     */
    @Test
    public void signIn_LockOutProfile_ReturnSuccess() {
        rsTest = false;

        email = "tictacmctest1001@gmail.com";

        SignInCase.Params params = new SignInCase.Params(email, pwd);

        signInCase.loadProfileOKTACheckStatus(params.email)
                .andThen(signInCase.loadProfileNoSaveStorage(params).toCompletable())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "//---------- onComplete: Sign In done");
                        rsTest = true;
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(ErrCode.PROFILE_OKTA_LOCKED_OUT.name().equals(e.getMessage())){
                            Log.d(TAG, "//---------- onError: SignIn PROFILE_OKTA_LOCKED_OUT "
                                    + App.getInstance().getString(R.string.valid_login_okta_profile_error));

                        }else if (ErrCode.PASS_CODE_ERROR.name().equals(e.getMessage())
                                || ErrCode.PASS_CODE_INVALID_ERROR.name().equals(e.getMessage())) {
                            Log.d(TAG, "//---------- onError: SignIn binCode");
                        } else if (ErrorApi.isGetTokenError(e.getMessage())) {
                            Log.d(TAG, "//---------- onError: " + App.getInstance().getString(R.string.errorSignIn));
                        } else {
                            Log.d(TAG, "//---------- onError: " + e.getMessage());
                        }
                        rsTest = false;
                        dispose();
                    }
                });
        //result
        assertTrue(rsTest);
    }
    //</editor-fold>

    //<editor-fold desc="Sign Up / Create user">
    @Test
    public void signUp_ReturnSuccess() {
        rsTest = false;

        email = "tictactestnotnuse1@gmail.com";
//        email = "tictacPMA_mc10@gmail.com";

        Profile profile = new Profile();
        profile.setFirstName("Tx");
        profile.setLastName("T2");
        profile.setEmail(email);
        profile.setPhone("1-123-312-3123");
        profile.setZipCode("90000-0000");

        ProfilePMAMapView profilePMAMapView = null;
        ChangeSecurityQuestion.Param securityQuestionParam = new ChangeSecurityQuestion.Param(
                email,
                pwd,
                "Who is your favorite book/movie character?",
                "batman"
        );
        CreateProfileCase.Params params = mapProfile.createProfile(profile,
                null,
                securityQuestionParam
        );

        createProfileCase.buildUseCaseSingle(params)
                .subscribeWith(new DisposableSingleObserver<ProfileAspire>() {
                    @Override
                    public void onSuccess(ProfileAspire profileAspire) {
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(err)) {

                        } else if (ErrCode.USER_PROFILE_OKTA_NOT_EXIST.name().equalsIgnoreCase(err)
                                || ErrCode.CHANGE_SECURITY_QUESTION_ERROR.name().equalsIgnoreCase(err)) {

                        }
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });

        //result
        assertTrue(rsTest);
    }

    /*account don't exist OKTA. And Exist in PMA:

   tictacPMA_1@gmail.com
   tictacPMA_mc1@gmail.com
   tictacPMA_mc2@gmail.com
   tictacPMA_mc3@gmail.com
   tictacPMA_mc4@gmail.com
   tictacPMA_mc5@gmail.com
   */
    @Test
    public void checkAccountOnSystem_ReturnSuccess() {
        rsTest = false;

//        email = "pma_s3_1@gmail.com"; //- normal. SIGN_UP_PROFILE_OKTA_EXISTS_ERROR
//        email = "tictacPMAtest@aspire.org"; //-SIGN_UP_PROFILE_OKTA_AND_PMA_NO_FOUND_ERROR
        email = "tictacPMA_1@gmail.com";
        signUpCase.buildCaseCheckProfileOKTAAndPMA(email)
                .subscribeWith(
                        new DisposableSingleObserver<ProfilePMAMapView>() {
                            @Override
                            public void onSuccess(ProfilePMAMapView profilePMAMapView) {
                                Log.d(TAG, "onSuccess: " + profilePMAMapView.getPhone());
                                rsTest = true;
                            }

                            @Override
                            public void onError(Throwable e) {
                                String err = e.getMessage();
                                Log.d(TAG, "//---------- onError: " + err);

                                rsTest = false;
                            }
                        }
                );

        //result
        assertTrue(rsTest);
    }

    @Test
    public void checkGetTokenPMA_ReturnSuccess() {
        rsTest = false;
        signUpCase.getPMAToken()
                .subscribeWith(
                        new DisposableSingleObserver<String>() {
                            @Override
                            public void onSuccess(String s) {
                                Log.d(TAG, "onSuccess: " + s);
                                rsTest = true;
                            }

                            @Override
                            public void onError(Throwable e) {
                                String err = e.getMessage();
                                Log.d(TAG, "//---------- onError: " + err);

                                rsTest = false;
                            }
                        }
                );

        //result
        assertTrue(rsTest);
    }

    @Test
    public void changeSecurityQuestion_ReturnSuccess() {
        rsTest = false;

        email = "tictacmc11@gmail.com";
        ChangeSecurityQuestion.Param securityQuestionParam = new ChangeSecurityQuestion.Param(
                email,
                pwd,
                "Who is your favorite book/movie character?",
                "test"
        );

        useCaseSecurityQuestion.buildUseCaseCompletable(securityQuestionParam)
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        if (ErrCode.USER_PROFILE_OKTA_NOT_EXIST.name().equalsIgnoreCase(err)
                                || ErrCode.CHANGE_SECURITY_QUESTION_ERROR.name().equalsIgnoreCase(err)) {

                        }
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });

        //result
        assertTrue(rsTest);
    }
    //</editor-fold>

    /**
     * run func signIn
     * to get access from local
     */
    @Test
    public void updateProfile_ReturnSuccess() {
//        rsTest = false;
//        loadProfile.loadRemote(getAccessToken)
//                .flatMap(profileCache -> {
//                    //update profile here
//                    profileCache.setPhone("123478");
//
//                    // don't need map profile local
////                    ProfileAspire profileRequest = mapProfile.updateProfile(profileCache, profile);
//
//                    return Single.just(profileCache);
//                })
//                .flatMapCompletable(proUpdated -> saveProfile.updateProfile(proUpdated))
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });
//
//        //result
//        assertTrue(rsTest);
    }

    /**
     * run func signIn
     * to get access from local
     */
    @Test
    public void changePassword_ReturnError() {
        rsTest = false;

        String oldPassword = "Admin@12345";
        String newPassword = "Admin@1234";

        ResetPassword.Params params = new ResetPassword.Params(oldPassword, newPassword);
        changePassword.buildUseCaseCompletable(params)
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });

        //result
        assertTrue(rsTest);
    }

    /**
     * run func signIn
     * to get access from local
     */
    @Test
    public void forgotPassword_ReturnError() {
        rsTest = false;

        String email = "tictacpcuda100@gmail.com";

//        retrievePassword.param(email)
//                .execute(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });

        //result
        assertTrue(rsTest);
    }

    /**
     * run func signIn
     * to get access from local
     */
    @Test
    public void updateUserPreference_ReturnSuccess() {
        rsTest = false;
//        loadProfile.loadRemote(getAccessToken)
//                .flatMap(profileCache -> {
//                    //update userPreference here
//                    PreferenceData preferenceData = profileCache.getPreferenceData();
//                    preferenceData.setHotel("2 Stars", preferenceData.getHotelPrefID());
//                    profileCache.setPreferenceData(preferenceData);
//
//                    return Single.just(profileCache);
//                })
//                .flatMapCompletable(proUpdated -> saveProfile.updateProfile(proUpdated)
//                        .andThen(loadProfile.loadRemote(getAccessToken)
//                                .flatMapCompletable(proLoadRemote -> Completable.complete())
//                        )
//                )
//                .subscribeWith(new DisposableCompletableObserver() {
//                    @Override
//                    public void onComplete() {
//                        rsTest = true;
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        String err = e.getMessage();
//                        Log.d(TAG, "//---------- onError: " + err);
//
//                        rsTest = false;
//                    }
//                });
//
//        //result
//        assertTrue(rsTest);
    }

    @Test
    public void checkPassCode_ReturnSuccess() {
        rsTest = false;
        String inputPassCode = "6445785";

        CheckPassCode.Params params = new CheckPassCode.Params(inputPassCode);

        checkPassCode.buildUseCaseSingle(params)
                .flatMapCompletable(result -> {
                    if (result != null && result) {
                        return Completable.complete();
                    } else {
                        return Completable.error(new Exception("PassCode Invalid"));
                    }
                })
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });

        //result
        assertTrue(rsTest);
    }


    //<editor-fold desc="Concierge Case">
    @Test
    public void sendRequestConciergeCase_ReturnSuccess() {
        rsTest = false;
        //value
        final String content = "Testing";
        final boolean email = true;
        final boolean phone = false;

        /*askConciergeApi.loadStorage()
                .flatMap(profileAspire -> {
                    String binCode = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                            ConstantAspireApi.APP_USER_PREFERENCES.PASS_CODE_KEY);
                    int binNumber = CommonUtils.convertStringToInt(binCode);
                    return Single.just(binNumber);
                })
                .flatMap(binCode -> createRequestCase(content, email, phone, binCode))
                .flatMapCompletable(response -> {
                    if (response.isEmpty()) {
                        throw new Exception("Failed to retrieve your profile.");
                    } else if (response.isSuccess()) {
                        rsTest = true;
                    } else {
                        throw new Exception(response.getMessage());
                    }
                    return Completable.complete();
                })
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });*/

        //result
        assertTrue(rsTest);
    }
/*
    private Single<ConciergeCaseAspireResponse> createRequestCase(String content, boolean email, boolean phone, int binCode) {
        return askConciergeApi.execute(new AskConciergeAspire.Params(
                ConciergeCaseType.CREATE,
                content,
                null,//city
                null,
                email,
                phone,
                binCode,
                BuildConfig.AS_PROGRAM,
                AppConstant.CONCIERGE_REQUEST_TYPE.OTHERS.getValue()
        ));
    }*/
    //</editor-fold>


    @Test
    public void revokeTokenCase_ReturnSuccess() {
        rsTest = false;

        String email = "tictacmc12@gmail.com";
        String secret = "Admin@1234";
        getAccessToken.getTokenSignIn(email, secret)
                .flatMapCompletable(token -> {
                    Log.d(TAG, "---revokeToken: \n" + token);
                    return getToken.revokeToken(token);
                })
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onSuccess: revoke token success");
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });

        //result
        assertTrue(rsTest);
    }

    @Test
    public void revokeTokenInApp_ReturnSuccess() {
        rsTest = false;

        String email = "tictacmc12@gmail.com";
        String secret = "Admin@1234";
        getAccessToken.getTokenSignIn(email, secret)
                .flatMapCompletable(token -> {
                    Log.d(TAG, "---revokeToken: \n" + token);
                    return getToken.revokeTokenInApp();
                })
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        Log.d(TAG, "onSuccess: revoke token success");
                        rsTest = true;
                    }

                    @Override
                    public void onError(Throwable e) {
                        String err = e.getMessage();
                        Log.d(TAG, "//---------- onError: " + err);

                        rsTest = false;
                    }
                });

        //result
        assertTrue(rsTest);
    }


    @Test
    public void checkFunc_ReturnSuccess() {
        rsTest = false;

//        String email = "tictacmc12@gmail.com";
//        String secret = "Admin@1234";
//
//        final String valueSecret = "password";
//
//        String errorMessage = "{\n" +
//                "      \"errorCode\": \"ACE_BADREQUEST_000\",\n" +
//                "      \"message\": \"Bad request exception at PMA API: string 'a' is too short (length: 1, required minimum: 6) for idamAccount/password\"\n" +
//                "    }";
//        boolean status = !TextUtils.isEmpty(errorMessage) && (errorMessage.contains(valueSecret)
//                || errorMessage.contains("ACE_BADREQUEST_000"));
//
//        //result
//        assertTrue(rsTest);

        String email = "tieu2@gmail.com";
        String secret = "Atieu@7654321";

        Validator validator = new Validator();
        boolean result = validator.secretRuleContain(secret, email, "haha","hihi");
    }

    @Test
    public void testSecretPass_ReturnSuccess() {
        rsTest = true;

        String value = EncryptData.getInstance().encrypt("Admin@1234");
        Log.d(TAG, "testSecretPass_ReturnSuccess Encrypt: " + value);

        String deValue = EncryptData.getInstance().decrypt(value);
        Log.d(TAG, "testSecretPass_ReturnSuccess DeEncrypt: " + deValue);

        //result
        assertTrue(rsTest);
    }



}