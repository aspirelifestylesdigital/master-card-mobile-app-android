package com.mastercard.androidapp.common.logic;

import com.mastercard.androidapp.BuildConfig;

import java.io.File;

/**
 * Created by Thu Nguyen on 7/24/2017.
 */

public class DeviceRootUtils {
    public static boolean isRooted() {
        if(!BuildConfig.DEBUG) {
            return findBinary();
        }
        return false;
    }

    private static boolean findBinary() {
        boolean found = false;
        if (!found) {
            String[] places = { "/sbin/su", "/system/bin/su", "/system/xbin/su", "/system/xbin/sudo",
                    "/data/local/xbin/su", "/data/local/bin/su",
                    "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su",
                    "/magisk/.core/bin/su", "/su/bin/su"};
            for (String where : places) {
                if (new File(where).exists()) {
                    found = true;
                    break;
                }
            }
        }
        return found;
    }
}
