package com.mastercard.androidapp.presentation.profile;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.mastercard.androidapp.domain.model.Profile;

/**
 * Created by vinh.trinh on 7/11/2017.
 */

public class ProfileComparator {

    public Profile original;

    private EditText edtFirstName, edtLastName, edtEmail, edtPhone, edtZipCode, editAnswer;
    private boolean firstName, lastName, email, phone, zipCode, agree, answer, question;
    private CheckBox checkBox;
    private Button btnSubmit, btnCancel;
    boolean changed;

    public ProfileComparator(EditText edtFirstName, EditText edtLastName, EditText edtEmail, EditText edtPhone,
                             EditText edtZipCode, CheckBox checkBox, Button btnSubmit, Button btnCancel ,EditText edtAnswer) {
        this.edtFirstName = edtFirstName;
        this.edtLastName = edtLastName;
        this.edtEmail = edtEmail;
        this.edtPhone = edtPhone;
        this.edtZipCode = edtZipCode;
        this.checkBox = checkBox;
        this.btnSubmit = btnSubmit;
        this.btnCancel = btnCancel;
        this.editAnswer = edtAnswer;
        setUp();
    }

    public ProfileComparator(EditText edtFirstName, EditText edtLastName, EditText edtPhone,
                             EditText edtZipCode, CheckBox checkBox, Button btnSubmit) {
        this(edtFirstName,
                edtLastName,
                null,
                edtPhone,
                edtZipCode,
                checkBox,
                btnSubmit,
                null,
                null
        );
    }

    private void setUp() {
        edtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (string.contains("*")) return;
                firstName = !string.equals(original.getFirstName());
                onChanged();
            }
        });
        edtLastName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (string.contains("*")) return;
                lastName = !string.equals(original.getLastName());
                onChanged();
            }
        });

        /*if (edtEmail != null) {
            edtEmail.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String string = s.toString();
                    if (string.contains("*")) return;
                    email = !string.equals(original.getEmail());
                    onChanged();
                }
            });
        }*/

        edtPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (string.contains("*")) return;
                phone = !string.equals(original.getPhone());
                onChanged();
            }
        });
        edtZipCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (string.contains("*")) return;
                zipCode = !string.equals(original.getZipCode());
                onChanged();
            }
        });
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                agree = original.isAgreed() != b;
                onChanged();
            }
        });
        if (editAnswer !=null){
            editAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(editAnswer.isFocused()) {
                        String string = s.toString();
                        answer = string.trim().length() > 0 ;
                        onChanged();
                    }
                }
            });
        }
    }

    public boolean isAnswerChanged() {
        return answer;
    }

    public boolean isQuestion() {
        return question;
    }

    public void setQuestion(boolean question) {
        this.question = question;
    }

    public boolean isProfileChanged() {
        return firstName || lastName || phone || zipCode || agree;
    }

    private void onChanged() {
        boolean valueCheck = isProfileChanged();
        if (editAnswer == null) {
            this.changed = valueCheck;
        } else {
            //-- My Profile
            this.changed = valueCheck || answer;
        }
        if(btnSubmit != null){
            btnSubmit.setEnabled(changed);
            btnSubmit.setClickable(changed);
        }

        if(btnCancel != null){
            btnCancel.setEnabled(changed);
            btnCancel.setClickable(changed);
        }
    }

    public void handleSelectQuestion() {
        //-- My Profile
        this.changed = isProfileChanged();

        if(btnSubmit != null){
            btnSubmit.setEnabled(changed);
            btnSubmit.setClickable(changed);
        }

        if(btnCancel != null){
            btnCancel.setEnabled(changed);
            btnCancel.setClickable(changed);
        }
    }
}
