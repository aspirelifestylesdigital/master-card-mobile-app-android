package com.mastercard.androidapp.datalayer.entity.b2cutility;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/3/2017.
 */

public class CheckBINResult {
    @SerializedName("Status")
    private String status;
    @SerializedName("Success")
    private Boolean success;
    @SerializedName("Valid")
    private Boolean valid;

    public String status() {
        return status;
    }

    public Boolean success() {
        return success;
    }

    public Boolean valid() {
        return valid;
    }
}
