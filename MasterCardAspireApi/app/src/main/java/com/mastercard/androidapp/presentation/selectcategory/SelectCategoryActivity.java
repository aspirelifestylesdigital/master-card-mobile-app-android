package com.mastercard.androidapp.presentation.selectcategory;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.common.constant.CityData;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.common.constant.RequestCode;
import com.mastercard.androidapp.common.constant.ResultCode;
import com.mastercard.androidapp.presentation.base.CommonActivity;
import com.mastercard.androidapp.presentation.cityguidecategory.CityGuideCategoriesActivity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class SelectCategoryActivity extends CommonActivity implements
        CategoryAdapter.OnCategoryItemClickListener
        , Category.View {

    private final String MASTERCARD_TRAVEL_URL = "https://travel.mastercard.com/travel/arc.cfm";
    private final String GOLF_URL = "https://priceless.com/golf";
    private final String PRICELESS_CITY_URL = "https://priceless.com";
    @BindView(R.id.selection_recycle_view)
    RecyclerView categoriesListView;
    private CategoryAdapter adapter;
    private DialogHelper dialogHelper;
    private CategoryPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_category);
        setTitle(R.string.category_title);
        setToolbarColor(R.color.colorPrimaryDark);
        presenter = new CategoryPresenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        GridLayoutManager manager = new GridLayoutManager(this, 2);
        categoriesListView.setLayoutManager(manager);
        categoriesListView.addItemDecoration(
                new SelectCategorySpacesItemDecoration(
                        getResources().getDimensionPixelSize(R.dimen.grid_item_margin)));
        adapter = new CategoryAdapter(this);
        adapter.setListener(this);
        categoriesListView.setAdapter(adapter);

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0 || position == 11) {
                    return 2;
                }
                return 1;
            }
        });

        categoriesListView.setPadding(getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_medium),
                getResources().getDimensionPixelSize(R.dimen.padding_smallest));

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.CATEGORY_LIST.getValue());
        }
        /**
         * when call categories from API, uncomment this.
         * For now I hash code the city id.
         */
//        presenter.getSubCategories();
    }

    @OnClick(R.id.btn_all_category)
    void allCategoryBtnClick(){
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.SELECTED_CATEGORY, "");
        setResult(ResultCode.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onItemClick(String category) {
        if (category.equalsIgnoreCase(adapter.getCategoryList()[1])) {//click on Master Card Travel
            selectSpecializeCategory(MASTERCARD_TRAVEL_URL);
        } else if (category.equalsIgnoreCase(adapter.getCategoryList()[5])) {//click on Golf
            selectSpecializeCategory(GOLF_URL);
        } else if (category.equalsIgnoreCase(adapter.getCategoryList()[7])) {//click on Priceless City
            selectSpecializeCategory(PRICELESS_CITY_URL);
        } else if (adapter.getCategoryList().length == CategoryAdapter.ITEM_MAXIMUM &&
                category.equalsIgnoreCase(adapter.getCategoryList()[10])) {//click on City Guide
            //return the category id of the current city selected if that city support city guide
            int cityId = CityData.guideCode();
            //start sub category select
            Intent intent = new Intent(this, CityGuideCategoriesActivity.class);
            intent.putExtra(IntentConstant.CATEGORY_ID, cityId);
            startActivityForResult(intent, RequestCode.SELECT_SUB_CATEGORY);
        } else {
            Intent intent = new Intent();
            if(category.toLowerCase().contains("experiences")) {
                category = "EXPERIENCES";
            }
            intent.putExtra(IntentConstant.SELECTED_CATEGORY, category);
            setResult(ResultCode.RESULT_OK, intent);
            finish();
        }
    }

    /**
     * specialize categories include: Mastercard Travel, Golf, Priceless Cities
     */
    private void selectSpecializeCategory(String url) {
        dialogHelper.showLeavingAlert(url);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCode.SELECT_SUB_CATEGORY) {
            if (resultCode == ResultCode.RESULT_OK && null != data) {
                data.putExtra(IntentConstant.SUPPER_CATEGORY
                        , getResources().getString(R.string.city_guide));
                setResult(ResultCode.RESULT_OK_WITH_ID, data);
                finish();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
}
