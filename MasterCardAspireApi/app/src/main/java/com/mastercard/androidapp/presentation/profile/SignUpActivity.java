package com.mastercard.androidapp.presentation.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.domain.model.ProfileMetadata;
import com.mastercard.androidapp.presentation.base.BaseActivity;
import com.mastercard.androidapp.presentation.home.HomeActivity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;

import org.json.JSONException;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class SignUpActivity extends BaseActivity implements ProfileFragment.ProfileEventsListener,
        CreateProfile.View {

    private CreateProfilePresenter presenter;
    private DialogHelper dialogHelper;
    private View view;
    ProfileFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_fragment_holder);
        view = findViewById(R.id.fragment_place_holder);
        presenter = new CreateProfilePresenter(this);
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            fragment = ProfileFragment.newInstance(ProfileFragment.PROFILE.CREATE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder,
                            fragment,
                            ProfileFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_UP.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onProfileSubmit(Profile profile) {
        ProfileMetadata profileMetadata = null;
        try {
            profileMetadata = new ProfileMetadata(
                    getIntent().getStringExtra(Intent.EXTRA_TEXT));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(this);
        preferencesStorage.clear();
        if (profileMetadata != null) {
            profile.setMetadata(profileMetadata);
            profile.updateMetadata();
            profile.setPinCode(profileMetadata.validBinNumber);
        }
        presenter.createProfile(profile);
    }

    @Override
    public void showProfileCreatedDialog() {
        ProfileMetadata profileMetadata = null;
        try {
            profileMetadata = new ProfileMetadata(
                    getIntent().getStringExtra(Intent.EXTRA_TEXT));
            if (profileMetadata != null) {
                PreferencesStorageAspire preferencesStorageAspire = new PreferencesStorageAspire(this);
                preferencesStorageAspire.editor().binCode(profileMetadata.validBinNumber).build().saveAsync();
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }


        dialogHelper.createBuilder(0, R.string.profile_created_message
                , R.string.back_to_profile, R.string.home)
                .onPositive((dialog, which) -> {
                    backToProfile();
                    // Track GA with "Sign up" event
                    App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                            AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                            AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
                })
                .onNegative((dialog, which) -> {
                    proceedToHome();
                    // Track GA with "Sign up" event
                    App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                            AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                            AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
                })
                .build().show();
    }

    private void proceedToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();

    }

    private void backToProfile() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        Intent mainIntent = new Intent(this, MyProfileActivity.class);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.isNetworkUnEnable(errCode, extraMsg)) {
            dialogHelper.showNoInternetAlert();
        } else if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(extraMsg)) {
            dialogHelper.alert(App.getInstance().getString(R.string.errorTitle), App.getInstance().getString(R.string.errorExistEmail));
        } else {
            dialogHelper.showGeneralError();
        }
    }

    @Override
    public void showProgressDialog() {
        //dialogHelper.showProgress(getString(R.string.dialog_save_processing));
        ProgressDialogUtil.showLoading(this);
    }

    @Override
    public void dismissProgressDialog() {
        //dialogHelper.dismissProgress();
        ProgressDialogUtil.hideLoading();
    }

    @Override
    public void onProfileCancel() {
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            if (fragment != null) {
                fragment.getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fragment.invokeSoftKey();
                    }
                }, 100);

            }
        });
    }

    @Override
    public void onFragmentCreated() {
    }

    @Override
    public void onUpdateSecurityQuestion(String question, String answer) {

    }

    @Override
    public void onProfileSubmitAndUpdateSecurity(Profile profile, String question, String answer) {

    }

    boolean shouldClose = false;
    /** handle touch outside to close soft keyboard */
    /*@Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Rect contentRect = fragment.contentWrapperViewRect();
            if(!contentRect.contains((int) event.getX(), (int) event.getY())) {
                shouldClose = true;
            }
        } else if(event.getAction() == MotionEvent.ACTION_UP && shouldClose) {
            InputMethodManager imm = (InputMethodManager) this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            shouldClose = false;
        }
        return super.dispatchTouchEvent(event);
    }*/
}
