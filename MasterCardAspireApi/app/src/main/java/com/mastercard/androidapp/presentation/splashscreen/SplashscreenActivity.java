package com.mastercard.androidapp.presentation.splashscreen;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.common.logic.DeviceRootUtils;
import com.mastercard.androidapp.presentation.base.BaseActivity;
import com.mastercard.androidapp.presentation.checkout.BINCheckoutFragment;
import com.mastercard.androidapp.presentation.checkout.CheckoutActivity;
import com.mastercard.androidapp.presentation.checkout.PrivacyPolicyFragment;
import com.mastercard.androidapp.presentation.checkout.signInV2.SignInActivity;
import com.mastercard.androidapp.presentation.home.HomeActivity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;
import com.support.mylibrary.widget.JustifiedTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashscreenActivity extends BaseActivity implements SubsequentAccess.View, BINCheckoutFragment.BINCheckoutListener {
    @BindView(R.id.fullscreen_content)
    View contentView;
    @BindView(R.id.text_acknowledge)
    JustifiedTextView textAcknowledge;
    SubsequentAccessPresenter presenter;
    private DialogHelper dialogHelper;
    protected boolean isShowProgress = true;

    private SubsequentAccessPresenter subsequentAccessPresenter() {
        return new SubsequentAccessPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);
        //hide();
        presenter = subsequentAccessPresenter();
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        if (DeviceRootUtils.isRooted()) {
            dialogHelper.showRootedDeviceAlert(this);
        } else {
            presenter.process();
        }
        // Load temporary link for webview so that the BIN screen loads it smoothly
        String acknowledgeTextFormat = "<a href='%s' style='text-decoration:none;color:white'>I acknowledge that I have read and agree to the </a><a href='%s'>Terms of Use</a><a href='%s' style='text-decoration:none;color:white'> and </a><a href='%s'>Privacy Policy</a>";
        String acknowledgeText = String.format(acknowledgeTextFormat, BINCheckoutFragment.ANONYMOUS_HYPERLINK, BINCheckoutFragment.TERM_OF_USE_TEMP_HYPERLINK, BINCheckoutFragment.ANONYMOUS_HYPERLINK, BINCheckoutFragment.PRIVACY_POLICY_TEMP_HYPERLINK);
        textAcknowledge.setText(acknowledgeText);

        // Track GA
        App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SPLASH.getValue());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isShowProgress = false;
        ProgressDialogUtil.hideLoading();
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        if (contentView != null) {
            contentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    @Override
    public void toPrivacyPolicy(boolean isMajorChange) {
        Intent intent = new Intent(this, CheckoutActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, 1);
        intent.putExtra(PrivacyPolicyFragment.VERSION_MAJOR_CHANGE, isMajorChange);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if (!DeviceRootUtils.isRooted()) {
            super.onBackPressed();
        }
    }

    @Override
    public void toBINcheckout(boolean messageInclude) {
        Intent intent = new Intent(this, CheckoutActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, 0);
        intent.putExtra(IntentConstant.SPLASH_SCREEN_HIDE_NAV_BACK, true);
        if (messageInclude) {
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.checkout_bin_expired));
        }
        startActivity(intent);
        finish();
    }

    @Override
    public void toLoginScreen() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void proceedToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
        /*Intent intent = new Intent(this, CheckoutActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, 1);
        intent.putExtra(PrivacyPolicyFragment.VERSION_MAJOR_CHANGE, true);
        startActivity(intent);
        finish();*/
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        isShowProgress = false;
        // Hide progress dialog if any
        ProgressDialogUtil.hideLoading();
        if (errCode == ErrCode.CONNECTIVITY_PROBLEM) {
            //message = R.string.no_network_connection;
            dialogHelper.showNoInternetAlert(
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(Settings.ACTION_SETTINGS));
                            finish();
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
        } else {
            dialogHelper.showTimeoutError(
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            startActivity(new Intent(Settings.ACTION_SETTINGS));
                            finish();
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });
        }
//        dialogHelper.createBuilder(0, message, R.string.text_retry, R.string.text_cancel)
//                .onPositive((dialog, which) -> presenter.process())
//                .onNegative((dialog, which) -> finish())
//                .cancelable(false)
//                .build().show();
    }

    @Override
    public void showProgressDialog() {
        if (isShowProgress) {
            ProgressDialogUtil.showLoading(this);
        }
    }

    @Override
    public void hideProgressDialog() {
        ProgressDialogUtil.hideLoading();
    }

    @Override
    public void onBINCodeSubmit(String input) {

    }

    @Override
    public void onTermOfUseClick() {

    }

    @Override
    public void onPrivacyPolicyClick() {

    }

    @Override
    public void onErrorGetToken() {
        if (dialogHelper != null) {
            dialogHelper.showGetTokenError();
        }
    }
}