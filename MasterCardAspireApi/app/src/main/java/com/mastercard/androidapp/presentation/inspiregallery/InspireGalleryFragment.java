package com.mastercard.androidapp.presentation.inspiregallery;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.datalayer.repository.B2CDataRepository;
import com.mastercard.androidapp.domain.model.GalleryViewPagerItem;
import com.mastercard.androidapp.domain.usecases.GetGalleries;
import com.mastercard.androidapp.presentation.base.BaseFragment;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;
import com.support.mylibrary.widget.CircleIndicator;

import java.util.List;

import butterknife.BindView;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class InspireGalleryFragment extends BaseFragment implements GetGallery.View{

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.parent_view)
    FrameLayout parentView;
    @BindView(R.id.indicator)
    CircleIndicator indicator;
    @BindView(R.id.pbLoading)
    ProgressBar pbLoading;

    GetGalleryPresenter presenter;
    List<GalleryViewPagerItem> galleryItemList;
    private InspireGalleryEventListener inspireGalleryEventListener;
    private int currentPosition = 0;

    private ViewPager.OnPageChangeListener onPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            currentPosition = position;
            setTheHoldPageColor(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public static InspireGalleryFragment newInstance() {
        Bundle args = new Bundle();
        InspireGalleryFragment fragment = new InspireGalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InspireGalleryEventListener) {
            inspireGalleryEventListener = (InspireGalleryEventListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileEventsListener.");
        }
        presenter = new GetGalleryPresenter(new GetGalleries(new B2CDataRepository()));
        presenter.attach(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detach();
    }

    @Override
    public void onResume() {
        super.onResume();
        consumeTheBackgroundColor();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_inspiration_gallery, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProgressDialogUtil.showLoading(getActivity());
        // Begin call API
        presenter.getGalleryList();

        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.GALLERY.getValue());
        }
    }
    public void consumeTheBackgroundColor(){
        if(galleryItemList != null) {
            //update toolbar + status bar color after back from other screen
            setTheHoldPageColor(viewPager.getCurrentItem());
        }
    }
    private void setTheHoldPageColor(int position) {
        int color = galleryItemList.get(position).color;
        color = color != 0 ? color : ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark);
        inspireGalleryEventListener.onPageColorChanged(color);
        parentView.setBackgroundColor(color);
    }

    private void initViewPager() {
        InspireGalleryPagerAdapter pagerAdapter = new InspireGalleryPagerAdapter(getContext(), galleryItemList) {
            @Override
            public int getItemPosition(Object object) {
                return POSITION_NONE;
            }
        };
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(onPageChangeListener);
        //init indicator
        indicator.setViewPager(viewPager);
        pagerAdapter.registerDataSetObserver(indicator.getDataSetObserver());
        //set first page color
        setTheHoldPageColor(viewPager.getCurrentItem());
    }
    public int getCurrentColor(){
        if(galleryItemList != null && galleryItemList.size() > currentPosition) {
            int color = galleryItemList.get(currentPosition).color;
            color = color != 0 ? color : ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark);
            return color;
        }
        return ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark);
    }
    @Override
    public void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList) {
        //pbLoading.setVisibility(View.GONE);
        ProgressDialogUtil.hideLoading();
        if(galleryItemList != null && galleryItemList.size() > 0 ){
            viewPager.setVisibility(View.VISIBLE);
            indicator.setVisibility(View.VISIBLE);
            this.galleryItemList = galleryItemList;

            initViewPager();
        }
    }

    @Override
    public void onUpdateFailed() {
        pbLoading.setVisibility(View.GONE);
        if(getActivity() != null) {
            if (App.getInstance().hasNetworkConnection()) {
                new DialogHelper(getActivity()).showTimeoutError();
            } else {
                new DialogHelper(getActivity()).showNoInternetAlert();
            }
        }
    }

    public interface InspireGalleryEventListener {
        void onPageColorChanged(int color);
    }

}
