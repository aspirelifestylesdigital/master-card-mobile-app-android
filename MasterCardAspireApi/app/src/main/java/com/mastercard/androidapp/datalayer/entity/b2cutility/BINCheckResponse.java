package com.mastercard.androidapp.datalayer.entity.b2cutility;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 7/3/2017.
 */

public class BINCheckResponse {
    @SerializedName("CheckMasterCardBINResult")
    private CheckBINResult checkBINResult;

    public CheckBINResult checkBINResult() {
        return checkBINResult;
    }
}
