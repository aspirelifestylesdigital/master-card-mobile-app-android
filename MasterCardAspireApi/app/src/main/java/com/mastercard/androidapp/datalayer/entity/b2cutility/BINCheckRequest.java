package com.mastercard.androidapp.datalayer.entity.b2cutility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.mastercard.androidapp.BuildConfig;

/**
 * Created by vinh.trinh on 7/3/2017.
 */

public class BINCheckRequest {
    @SerializedName("subDomain")
    @Expose
    public final String subDomain;
    @SerializedName("password")
    @Expose
    public final String secretKey;
    @SerializedName("bin")
    @Expose
    public final String bin;

    public BINCheckRequest(String binCode) {
        subDomain = BuildConfig.WS_B2C_SUBDOMAIN;
        secretKey = BuildConfig.WS_B2C_SECRET;
        this.bin = binCode;
    }
}
