package com.mastercard.androidapp.presentation.inspiregallery;

import com.mastercard.androidapp.domain.model.GalleryViewPagerItem;
import com.mastercard.androidapp.presentation.base.BasePresenter;

import java.util.List;

/**
 * Created by Thu Nguyen on 6/13/2017.
 */

public interface GetGallery {

    interface View {
        void onGetGalleryListFinished(List<GalleryViewPagerItem> galleryItemList);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getGalleryList();
    }

}
