package com.mastercard.androidapp.common.logic;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vinh.trinh on 4/28/2017.
 */

public final class Validator {
    public final int NAME_MIN_LENGTH = 2;
    public final int NAME_MAX_LENGTH = 25;
    public final int PHONE_MAX_LENGTH = 14;
    public final int ZIP_CODE_LENGTH_5 = 5;
    public final int ZIP_CODE_LENGTH_10 = 10;// include "-"

    public final int PASSWORD_MIN_LENGTH = 1;

    private final String EMAIL_REGEX = "^((\"[\\w-\\s]+\")|([\\w-]+(?:\\.[\\w-]+)*)"
            + "|(\"[\\w-\\s]+\")([\\w-]+(?:\\.[\\w-]+)*))(@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]"
            + "{2,6}(?:\\.[a-z]{2})?)$)|(@\\[?((25[0-5]\\.|2[0-4][0-9]\\.|1[0-9]{2}\\.|[0-9]{1,2}\\.))"
            + "((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\]?$)";

    public boolean email(String email) {
        return email.matches(EMAIL_REGEX);
    }

    public boolean phone(String phone) {//length?
        return phone.length() == PHONE_MAX_LENGTH;
    }

    public boolean name(String name) {
        return name.length() >= NAME_MIN_LENGTH;
    }

    public boolean zipCode(String zipCode) {//5 or 10
        return zipCode.length() == ZIP_CODE_LENGTH_5 || zipCode.length() == ZIP_CODE_LENGTH_10;
    }

    public boolean secretValidatorWeekPassword(String minPass) {
        return minPass.length() >= PASSWORD_MIN_LENGTH;
    }

    public boolean password(String pwd, String confirmPwd) {
        return pwd.length() > 0 && confirmPwd.equals(pwd);
    }

    /**
     * use strong password
     */
    public boolean secretValidator(String secretWord) {
        Pattern pattern;
        Matcher matcher;
        if (secretWord != null && secretWord.trim().length() > 0) {
//            final String SECRET_PATTERN =
//                    "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[?()|~{}!@#\\$%\\^&\\*\\[\\]\"\\';:_\\-<>\\., =\\+\\/\\\\]).{8,25}";
            final String SECRET_PATTERN = "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\\$%\\^&\\*]).{10,25}";
            pattern = Pattern.compile(SECRET_PATTERN);
            matcher = pattern.matcher(secretWord);
            return matcher.matches();
        } else {
            return false;
        }
//        return  true;
    }

    public boolean answerSecurity(String answer) {
        return answer.trim().length() >= 4;
    }


    /**
     * rule secret have contain text first name, last name, username
     * */
    public boolean secretRuleContain(String secret, String email, String firstName, String lastName) {
        if(TextUtils.isEmpty(secret)){
            return false;
        }

        secret = secret.toLowerCase();
        email = email.toLowerCase();
        firstName = firstName.toLowerCase();
        lastName = lastName.toLowerCase();

        try {
            if(email.contains("@") && email.split("@").length > 1){
                email = email.split("@")[0];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return secret.contains(email)
                || secret.contains(firstName)
                || secret.contains(lastName);
    }
}
