package com.mastercard.androidapp.presentation.request;

import android.content.Context;

import com.api.aspire.common.constant.ConciergeCaseType;
import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.askconcierge.ConciergeCaseAspireResponse;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AspireConciergeRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.usecases.AskConciergeAspire;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.mastercard.androidapp.BuildConfig;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class AskConciergePresenter implements CreateNewConciergeCase.Presenter {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private CreateNewConciergeCase.View view;
    private AskConciergeAspire askConciergeApi;
    private String city;
    private String category;

    AskConciergePresenter(Context c) {
        PreferencesStorageAspire preferencesStorage = new PreferencesStorageAspire(c);
        MapProfileApp mapLogic = new MapProfileApp();
        this.askConciergeApi = new AskConciergeAspire(mapLogic,
                new AspireConciergeRepository(),
                new GetToken(preferencesStorage, mapLogic),
                new LoadProfile(new ProfileDataAspireRepository(preferencesStorage,mapLogic))
        );
    }

    @Override
    public void attach(CreateNewConciergeCase.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        this.view = null;
    }

    @Override
    public void param(String city, String category) {
        this.city = city;
        this.category = category;
    }

    @Override
    public void sendRequest(String content, boolean email, boolean phone) {
        view.showProgressDialog();
        compositeDisposable.add(
                askConciergeApi.loadStorage()
                        .flatMap(profileAspire -> {
                            String passCode = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);
                            int binNumber = CommonUtils.convertStringToInt(passCode);
                            return Single.just(binNumber);
                        })
                        .flatMap(binCode -> createRequestCase(content, email, phone, binCode))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new AskConciergeObserver())
        );
    }

    private Single<ConciergeCaseAspireResponse> createRequestCase(String content,
                                                                  boolean email, boolean phone,
                                                                  int binCode) {

        return askConciergeApi.execute(new AskConciergeAspire.Params(
                ConciergeCaseType.CREATE,
                content,
                city,
                category,
                email,
                phone,
                binCode,
                BuildConfig.AS_PROGRAM,
                AppConstant.CONCIERGE_REQUEST_TYPE.OTHERS.getValue()));
    }

    private final class AskConciergeObserver extends DisposableSingleObserver<ConciergeCaseAspireResponse> {

        @Override
        public void onSuccess(ConciergeCaseAspireResponse acResponse) {
            view.dismissProgressDialog();
            if (acResponse.isEmpty()) {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, "Failed to retrieve your profile.");
            } else if (acResponse.isSuccess()) {
                view.onRequestSuccessfullySent();
            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, acResponse.getMessage());
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }
}
