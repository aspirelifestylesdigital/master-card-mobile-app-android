package com.mastercard.androidapp.domain.mapper.profile;

import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.data.entity.profile.UpdateProfileAspireRequest;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.google.gson.Gson;
import com.mastercard.androidapp.BuildConfig;
import com.mastercard.androidapp.common.constant.CityData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class MapDataProfile {

    private static final String HOTEL_PREFERENCE = "Hotel Preference";
    private static final String DINING_PREFERENCE = "Dining Preference";
    private static final String TRANSPORT_PREFERENCE = "Car Type Preference";

    private static final String DEVICE_ANDROID = "Android";
    private static final String UNKNOWN = "Unknown";
    private static final String VAL_LOCATION_TYPE = "Original";
    private static final String VAL_EMAIL_TYPE_PRIMARY = "Primary";
    private static final String VAL_PHONE_TYPE_MOBILE = "Mobile";

    public static ProfileAspireResponse mapRequest(final ProfileAspire profileAspireLocal,
                                                   final String agreeNewLetterStatus,
                                                   final String valVersion,
                                                   final String valueNewLetterDate) {
        ProfileAspireResponse profileResponse;
        profileResponse = new ProfileAspireResponse();
        //- email
        String emailRq = TextUtils.isEmpty(profileAspireLocal.getEmail()) ? "" : profileAspireLocal.getEmail();
        ProfileAspireResponse.Emails emails = new ProfileAspireResponse.Emails();
        emails.setEmailaddress(emailRq);
        emails.setEmailtype(VAL_EMAIL_TYPE_PRIMARY);
        profileResponse.setEmails(Collections.singletonList(emails));
        //- First name & Last name
        profileResponse.setFirstname(profileAspireLocal.getFirstname());
        profileResponse.setLastname(profileAspireLocal.getLastname());
        //-don't have field salutation and location

        //- phone
        String phoneNumber = profileAspireLocal.getPhone();
        profileResponse.setPhones(initPhones(phoneNumber));

        //- location
        ProfileAspireResponse.Locations locations = initLocation(profileAspireLocal.getZipCode());
        profileResponse.setLocations(Collections.singletonList(locations));

        //-- Preference
        if (profileAspireLocal.getPreferenceData() == null) {
            profileAspireLocal.setPreferenceData(PreferenceData.plain());
        }//-- else get preferenceData in ProfileAspire

        //- userAppPreference
        //- binCode
        String binCodeSave = TextUtils.isEmpty(profileAspireLocal.getBinCodeRemote()) ? PreferenceData.NA_VALUE : profileAspireLocal.getBinCodeRemote();

        List<ProfileAspireResponse.AppUserPreferences> appUserPreferences = new ArrayList<>();
        appUserPreferences.add(new ProfileAspireResponse.AppUserPreferences(ConstantAspireApi.APP_USER_PREFERENCES.PLATFORM_KEY, DEVICE_ANDROID));

        appUserPreferences.add(new ProfileAspireResponse.AppUserPreferences(ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY, binCodeSave));

        //<editor-fold desc="special MC">
        appUserPreferences.add(new ProfileAspireResponse.AppUserPreferences(ConstantAspireApi.APP_USER_PREFERENCES.NEWSLETTER_STATUS_KEY, agreeNewLetterStatus));

        appUserPreferences.add(new ProfileAspireResponse.AppUserPreferences(ConstantAspireApi.APP_USER_PREFERENCES.NEWSLETTER_DATE_KEY, valueNewLetterDate));

        appUserPreferences.add(new ProfileAspireResponse.AppUserPreferences(ConstantAspireApi.APP_USER_PREFERENCES.POLICY_VERSION_KEY, valVersion));
        //</editor-fold>

        profileResponse.setAppUserPreferences(appUserPreferences);

        //- var require api
        profileResponse.setHomecountry(MapDataApi.getCountryCode());

        return profileResponse;
    }

    public static ProfileAspire mapResponse(final ProfileAspireResponse profileResponse,
                                            final String secretKeyCurrent,
                                            final String userEmailCurrent) {

        ProfileAspire profileAspireLocal = new ProfileAspire(new Gson().toJson(profileResponse));
        //set secret in storage local
        profileAspireLocal.setSecretKey(secretKeyCurrent);
        //email
        profileAspireLocal.setEmail(handleEmailProfile(profileResponse, userEmailCurrent));
        profileAspireLocal.setEmails(profileResponse.getEmails());
        //firstName
        profileAspireLocal.setFirstname(profileResponse.getFirstname());
        profileAspireLocal.setLastname(profileResponse.getLastname());
        //-salutation
        profileAspireLocal.setSalutation(MapDataApi.mapSalutationResponse(profileResponse.getSalutation()));
        //- phone
        profileAspireLocal.setPhone(loadPhone(profileResponse));
        profileAspireLocal.setPhones(profileResponse.getPhones());
        //- userAppPreference
        profileAspireLocal.setAppUserPreferences(ProfileLogicCore.isAppUserPrEmpty(profileResponse.getAppUserPreferences()));

        //- location
        profileAspireLocal = handleLocationApi(profileResponse, profileAspireLocal);

        //-- Preference
        PreferenceData preferenceData = PreferenceData.plain();
        for (ProfileAspireResponse.Preferences preference : profileResponse.getPreferences()) {

            if (HOTEL_PREFERENCE.equalsIgnoreCase(preference.getPreferencetype())) {

                preferenceData.setHotel(preference.getPreferencevalue(), preference.getPreferenceid());

            } else if (DINING_PREFERENCE.equalsIgnoreCase(preference.getPreferencetype())) {

                preferenceData.setCuisine(preference.getPreferencevalue(), preference.getPreferenceid());

            } else if (TRANSPORT_PREFERENCE.equalsIgnoreCase(preference.getPreferencetype())) {

                preferenceData.setVehicle(preference.getPreferencevalue(), preference.getPreferenceid());

            }
        }
        profileAspireLocal.setPreferenceData(preferenceData);

        profileAspireLocal.setPreferences(profileResponse.getPreferences());

        //- var require api
        profileAspireLocal.setHomecountry(MapDataApi.getCountryCode());

        return profileAspireLocal;
    }

    /**
     * binCode in Profile rank higher with in local (after change bin code always update in Profile)
     * */
    public static UpdateProfileAspireRequest mapUpdate(final ProfileAspire profileAspireLocal,
                                                       final String binCodeStorage) {
        //- update bin the end this func (bin verificationMetadata)
        UpdateProfileAspireRequest profileRequest = new UpdateProfileAspireRequest(-1);
        profileRequest.setPartyid(TextUtils.isEmpty(profileAspireLocal.getPartyid()) ? null : profileAspireLocal.getPartyid());

        profileRequest.setEmails(profileAspireLocal.getEmails());
        profileRequest.setFirstname(profileAspireLocal.getFirstname());
        profileRequest.setLastname(profileAspireLocal.getLastname());

        //-salutation

        //- phone
        profileRequest.setPhones(updatePhone(profileAspireLocal));

        //- userAppPreference
        profileRequest.setAppUserPreferences(ProfileLogicCore.isAppUserPrEmpty(profileAspireLocal.getAppUserPreferences()));

        //update device os
        ProfileLogicCore.setAppUserPreference(profileRequest.getAppUserPreferences(), ConstantAspireApi.APP_USER_PREFERENCES.PLATFORM_KEY, DEVICE_ANDROID);

        //- location
        //String locationValue = ProfileLogicCore.isUserLocation(profileAspireLocal.isLocationOn());
        //update location
        //ProfileLogicCore.setAppUserPreference(profileRequest.getAppUserPreferences(), ConstantAspireApi.APP_USER_PREFERENCES.USER_LOCATION_STATUS_KEY, locationValue);
        List<ProfileAspireResponse.Locations> locations = handleUpdateLocation(profileAspireLocal.getLocations(), profileAspireLocal.getZipCode());
        profileRequest.setLocations(locations);

        //-- Preference
        if (profileAspireLocal.getPreferenceData() == null) {
            profileAspireLocal.setPreferenceData(PreferenceData.plain());
        }//-- else get preferenceData in BaseProfile

        PreferenceData dataPref = profileAspireLocal.getPreferenceData();
        //binCode
        String valBinCode = ProfileLogicCore.getAppUserPreference(profileRequest.getAppUserPreferences(),ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);
        if (TextUtils.isEmpty(valBinCode) &&
                !TextUtils.isEmpty(binCodeStorage) &&
                !PreferenceData.NA_VALUE.equalsIgnoreCase(binCodeStorage)) {
            valBinCode = binCodeStorage;
            ProfileLogicCore.setAppUserPreference(profileRequest.getAppUserPreferences(), ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY, valBinCode);
        }

        ProfileAspireResponse.Preferences preHotel = getPreferenceItem(profileAspireLocal.getPreferences(), HOTEL_PREFERENCE, dataPref.getHotel());
        ProfileAspireResponse.Preferences preCuisine = getPreferenceItem(profileAspireLocal.getPreferences(), DINING_PREFERENCE, dataPref.getCuisine());
        ProfileAspireResponse.Preferences preTransport = getPreferenceItem(profileAspireLocal.getPreferences(), TRANSPORT_PREFERENCE, dataPref.getVehicle());
        profileRequest.setPreferences(getPreferenceList(profileAspireLocal.getPreferences(), preHotel, preCuisine, preTransport));

        //- var require api
        profileRequest.setHomecountry(MapDataApi.getCountryCode());

        //-- bin verificationMetadata
        profileRequest.setBinVerificationMetadata(CommonUtils.convertStringToInt(valBinCode));

        return profileRequest;
    }

    private static List<ProfileAspireResponse.Locations> handleUpdateLocation(List<ProfileAspireResponse.Locations> locationArr, String zipCode) {
        if (locationArr != null && locationArr.size() > 0) {
            ProfileAspireResponse.Locations locations1 = locationArr.get(0);
            ProfileAspireResponse.Address address = locations1.getAddress();
            address.setCity(UNKNOWN); //- don't use
            address.setAddressline5(null);
            address.setZipCode(zipCode);
            //- var require api
            address.setAddressline1(UNKNOWN);
            address.setCountry(MapDataApi.getCountryCode());
            locations1.setAddress(address);
            return locationArr;
        } else {
            //init new location - don't happen here
            ProfileAspireResponse.Locations locations = initLocation("");
            return Collections.singletonList(locations);
        }
    }

    private static ProfileAspireResponse.Locations initLocation(String zipCode) {
        ProfileAspireResponse.Address address = new ProfileAspireResponse.Address(
                MapDataApi.getCountryCode(), //location[].address.country
                UNKNOWN,
                null); //- don't use
        //- var require api
        address.setAddressline1(UNKNOWN);
        address.setZipCode(zipCode);
        return new ProfileAspireResponse.Locations(address, VAL_LOCATION_TYPE);
    }

    private static List<ProfileAspireResponse.Preferences> getPreferenceList(List<ProfileAspireResponse.Preferences> preferences,
                                                                             ProfileAspireResponse.Preferences preHotel,
                                                                             ProfileAspireResponse.Preferences preCuisine,
                                                                             ProfileAspireResponse.Preferences preTransport) {
        List<ProfileAspireResponse.Preferences> result;
        List<ProfileAspireResponse.Preferences> listUpdate = new ArrayList<>();
        if (preHotel != null) {
            listUpdate.add(preHotel);
        }
        if (preCuisine != null) {
            listUpdate.add(preCuisine);
        }
        if (preTransport != null) {
            listUpdate.add(preTransport);
        }

        if (preferences != null && preferences.size() > 0) {
            List<ProfileAspireResponse.Preferences> initList = new ArrayList<>();
            for (ProfileAspireResponse.Preferences preChange : listUpdate) {
                boolean statusUpdate = false;
                for (ProfileAspireResponse.Preferences preference : preferences) {
                    if (preChange.getPreferencetype().equalsIgnoreCase(preference.getPreferencetype())) {
                        //--update new value
                        preference.setPreferencevalue(preChange.getPreferencevalue());
                        statusUpdate = true;
                        break;
                    }
                }
                if (!statusUpdate) {
                    initList.add(preChange);
                }//-- else next preference
            }
            preferences.addAll(initList);
            result = preferences;
        } else {
            result = listUpdate;
        }
        return result;
    }

    /**
     * return
     * case data update value
     * case null don't init
     * case data with new value
     */
    private static ProfileAspireResponse.Preferences getPreferenceItem(List<ProfileAspireResponse.Preferences> preferences,
                                                                       String prefType,
                                                                       String prefValue) {

        if (preferences != null && preferences.size() > 0) {
            for (ProfileAspireResponse.Preferences preference : preferences) {
                if (prefType.equalsIgnoreCase(preference.getPreferencetype())) {
                    preference.setPreferencevalue(prefValue);
                    return preference;
                }
            }
        }
        //-- else
        ProfileAspireResponse.Preferences prefData;
        if (TextUtils.isEmpty(prefValue) || PreferenceData.NA_VALUE.equalsIgnoreCase(prefValue)) {
            prefData = null;
        } else {
            prefData = new ProfileAspireResponse.Preferences(prefValue, prefType);
        }
        return prefData;
    }

    /**
     * @return email current empty return email remote, otherwise return email current login
     */
    private static String handleEmailProfile(ProfileAspireResponse prfResponse, String userEmailCurrent) {
        String emailRemote;
        if (TextUtils.isEmpty(userEmailCurrent)) {
            if ((prfResponse.getEmails() != null && prfResponse.getEmails().size() > 0)) {
                emailRemote = prfResponse.getEmails().get(0).getEmailaddress();
                if (TextUtils.isEmpty(emailRemote)) {
                    emailRemote = "";
                }//-- else hold old data
            } else {
                emailRemote = "";
            }
        } else {
            emailRemote = userEmailCurrent;
        }
        return emailRemote;
    }

    private static List<ProfileAspireResponse.Phones> initPhones(String phoneNumber) {
        ProfileAspireResponse.Phones phones = new ProfileAspireResponse.Phones();
        phones.setPhoneid(null);
        phones.setPhonetype(VAL_PHONE_TYPE_MOBILE);
        phones.setPhonenumber(phoneNumber);
        return Collections.singletonList(phones);
    }

    private static String loadPhone(ProfileAspireResponse proValue) {
        if (proValue == null
                || proValue.getPhones() == null
                || proValue.getPhones().size() == 0) {
            return "";
        }
        List<ProfileAspireResponse.Phones> phoneList = proValue.getPhones();

        String phone = phoneList.get(0).getPhonenumber();
        phone = TextUtils.isEmpty(phone) ? "" : phone;

        return phone;
    }

    private static List<ProfileAspireResponse.Phones> updatePhone(ProfileAspire proValue) {
        if (proValue.getPhones() == null
                || proValue.getPhones().size() == 0) {
            return initPhones(proValue.getPhone());
        }

        List<ProfileAspireResponse.Phones> phoneList = proValue.getPhones();
        phoneList.get(0).setPhonenumber(proValue.getPhone());

        return phoneList;
    }

    /**
     * 2 case old account with address.location & userAppPreference.location
     * MC don't have location on/off
     */
    private static ProfileAspire handleLocationApi(ProfileAspireResponse proApi, ProfileAspire proSaveLocal) {
        proSaveLocal.setLocations(proApi.getLocations());
        //userAppPreference
        boolean rsLocation;
        String zipCode = "";
        if(proApi.getAppUserPreferences() == null || proApi.getAppUserPreferences().isEmpty()){
            rsLocation = false;
            CityData.reset();
        }else{
            // new account userAppPreference
            //- location on/off
            String locationApi = ProfileLogicCore.getAppUserPreference(proApi.getAppUserPreferences(), ConstantAspireApi.APP_USER_PREFERENCES.USER_LOCATION_STATUS_KEY);
            rsLocation = ProfileLogicCore.statusUserLocation(locationApi);
            //- handle city select city
            String selectCity = ProfileLogicCore.getAppUserPreference(proApi.getAppUserPreferences(), ConstantAspireApi.APP_USER_PREFERENCES.SELECTED_CITY_KEY);
            CityData.setSelectedCity(selectCity);
        }
        //<editor-fold desc="Special Case MC">
        //zipCode
        if(proApi.getLocations() != null
                && proApi.getLocations().size() > 0
                && proApi.getLocations().get(0).getAddress() != null){

            zipCode = proApi.getLocations().get(0).getAddress().getZipCode();
            zipCode = TextUtils.isEmpty(zipCode) ? "" : zipCode;
        }
        //</editor-fold>

        proSaveLocal.locationToggle(rsLocation); //don't use
        proSaveLocal.setZipCode(zipCode);
        return proSaveLocal;
    }

}