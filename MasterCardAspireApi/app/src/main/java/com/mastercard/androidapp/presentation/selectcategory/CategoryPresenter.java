package com.mastercard.androidapp.presentation.selectcategory;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by tung.phan on 5/31/2017.
 */

public class CategoryPresenter implements Category.Presenter {
    public static boolean CATEGORY_ALL = true;
    private CompositeDisposable disposables;
    private Category.View view;

    @Override
    public void attach(Category.View view) {
        this.view = view;
        disposables = new CompositeDisposable();
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getCategories() {
        /*disposables.add(getCategories.param(null).on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new CategoryPresenter.GetCategoriesObserver()));*/
    }
}
