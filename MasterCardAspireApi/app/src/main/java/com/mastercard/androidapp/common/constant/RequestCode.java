package com.mastercard.androidapp.common.constant;

/**
 * Created by tung.phan on 5/23/2017.
 */

public interface RequestCode {

    int SELECT_CITY = 100;
    int SELECT_CATEGORY = 101;
    int PHONE_CALL_PERMISSION = 102;
    int SPEECH_INPUT = 103;
    int SELECT_SUB_CATEGORY = 104;
    int SEARCH_INPUT = 105;
    int CHANGE_PASSWORD = 108;
    int REQUEST_CLEAR_FORM_SIGN_IN = 109;
    int SIGN_IN_FORGOT = 110;
}
