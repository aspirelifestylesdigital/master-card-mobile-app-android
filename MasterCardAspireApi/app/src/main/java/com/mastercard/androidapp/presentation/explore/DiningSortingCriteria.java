package com.mastercard.androidapp.presentation.explore;

import com.mastercard.androidapp.domain.model.explore.LatLng;

/**
 * Created by Den on 5/30/18.
 */

public class DiningSortingCriteria {
    LatLng location;
    String cuisine;
    //    public float distance;
    boolean isRegion;

    public DiningSortingCriteria(LatLng location, String cuisine) {
        this.location = location;
        this.cuisine = cuisine;
    }

    void setLocation(LatLng location) {
        this.location = location;
    }

    void setCuisine(String cuisine) {
        this.cuisine = cuisine;
    }

    void setIsRegion(boolean isRegion) {
        this.isRegion = isRegion;
    }
}
