package com.mastercard.androidapp.datalayer.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/6/2017.
 */

public class Tiles {
    @SerializedName("ID")
    private Integer ID;
    @SerializedName("TileImage")
    private String image;
    @SerializedName("TileLink")
    private String link;
    @SerializedName("TileText")
    private String text;
    @SerializedName("Title")
    private String title;
    @SerializedName("ShortDescription")
    private String shortDescription;
    @SerializedName("SubCategory")
    private String subCategory;
    @SerializedName("Category")
    private String category;
    @SerializedName("GeographicRegion")
    private String geographicRegion;

    public Integer getID() {
        return ID;
    }

    public String getImage() {
        return image;
    }

    public String getLink() {
        return link;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String subCategory() {
        return subCategory;
    }

    public String category() {
        return category;
    }

    public String geographicRegion() {
        return geographicRegion;
    }
}
