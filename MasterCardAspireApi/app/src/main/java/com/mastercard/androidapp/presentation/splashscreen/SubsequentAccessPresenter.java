package com.mastercard.androidapp.presentation.splashscreen;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.AuthData;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.google.gson.Gson;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.datalayer.repository.B2CDataRepository;
import com.mastercard.androidapp.domain.repository.B2CRepository;
import com.mastercard.androidapp.domain.usecases.CheckBIN;
import com.mastercard.androidapp.domain.usecases.GetMasterCardCopy;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;

import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.SingleEmitter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SubsequentAccessPresenter implements SubsequentAccess.Presenter {

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private SubsequentAccess.View view;
    private GetMasterCardCopy privacyPolicy;
    private CheckBIN checkBIN;
    private PreferencesStorageAspire preferencesStorageAspire;
    private LoadProfile mLoadProfile;
    private GetAccessToken getAccessToken;

    SubsequentAccessPresenter(Context context) {
        B2CRepository repository = new B2CDataRepository();
        MapProfileApp mapLogic = new MapProfileApp();
        this.privacyPolicy = new GetMasterCardCopy(repository);
        this.preferencesStorageAspire = new PreferencesStorageAspire(context);
        GetToken getToken = new GetToken(preferencesStorageAspire, mapLogic);
        this.checkBIN = new CheckBIN(new AuthDataAspireRepository(),
                mapLogic,
                getToken
        );
        this.mLoadProfile = new LoadProfile(new ProfileDataAspireRepository(preferencesStorageAspire,mapLogic));
        this.getAccessToken = new GetAccessToken(mLoadProfile, getToken);
    }

    @Override
    public void attach(SubsequentAccess.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void process() {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        Single.just("").delay(3, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> view.showProgressDialog());
        compositeDisposable.add(
                profileCreated()
                        .subscribeOn(Schedulers.io())
                        .observeOn(Schedulers.io())
                        .flatMap(profileCreated -> {
                            if (!profileCreated) {
                                // force to wait for 3s if user haven't created profile yet
                                Single<String> delay = Single.just("").delay(3, TimeUnit.SECONDS);
                                Single<ResultBuilder> result = Single.just(new ResultBuilder("", BIN_STATUS.NONE, PRIVACY_VERSION_STATUS.NONE));
                                return Single.zip(result, delay, (resultBuilder, s) -> resultBuilder);
                            }
                            ProfileAspire profileAspire = preferencesStorageAspire.profile();

                            String policyVersion = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                                    ConstantAspireApi.APP_USER_PREFERENCES.POLICY_VERSION_KEY);

                            return binCheck(policyVersion);
                        })
                        .flatMap(result -> {
                            if (!result.profileCreated || result.binCodeStatus != BIN_STATUS.VALID) {
                                return Single.just(result);
                            }
                            return ifPrivacyChanged(result.policyVersion, result.binCodeStatus);
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ResultBuilder>() {
                            @Override
                            public void onSuccess(@NonNull ResultBuilder resultBuilder) {
                                //-have profile local
                                view.hideProgressDialog();
                                if (resultBuilder.profileCreated) {
                                    if (resultBuilder.binCodeStatus != BIN_STATUS.VALID) {
                                        view.toBINcheckout(true);
                                        return;
                                    }
                                    if (resultBuilder.privacyPolicyChanged == PRIVACY_VERSION_STATUS.MAJOR_CHANGE) {
                                        view.toPrivacyPolicy(true);
                                        return;
                                    }
                                    view.proceedToHome();
                                } else {
                                    //- don't have profile local
                                    view.toLoginScreen();
                                }
                                dispose();
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                view.hideProgressDialog();
                                if (ErrorApi.isGetTokenError(e.getMessage())) {
                                    view.onErrorGetToken();
                                } else if (ErrCode.PASS_CODE_ERROR.name().equalsIgnoreCase(e.getMessage())) {
                                    view.toBINcheckout(true);
                                } else {
                                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR, null);
                                }
                                dispose();
                            }
                        }));
    }

    /*
    * -> load update profile local before check passCode
    * */
    private Single<Boolean> profileCreated() {
        return Single.create((SingleEmitter<Boolean> e) -> {

            boolean profileCreated = preferencesStorageAspire.profileCreated();
            e.onSuccess(profileCreated);

        }).flatMap(profileCreated -> {
            if (profileCreated) {
                return mLoadProfile.loadRemote(getAccessToken)
                        .flatMap(profileAspire -> Single.create(emit ->{

                            String binCode = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);

                            if(TextUtils.isEmpty(binCode)){
                                throw new Exception(ErrCode.PASS_CODE_ERROR.name());
                            }else{
                                preferencesStorageAspire.editor().binCode(binCode).build().save();
                                emit.onSuccess(true);
                            }
                        }));
            } else {
                return Single.just(profileCreated);
            }
        });
    }

    //- get new bin code after load Profile Remote
    private Single<ResultBuilder> binCheck(String privacyVersion) {
        String binCode = preferencesStorageAspire.getBinCode();
        if (TextUtils.isEmpty(binCode)) {
            return Single.just(new ResultBuilder(privacyVersion, BIN_STATUS.INVALID, PRIVACY_VERSION_STATUS.NONE));
        }
        return checkBIN.buildUseCaseSingle(binCode)
                .map(valid -> new ResultBuilder(privacyVersion,
                        valid ? BIN_STATUS.VALID : BIN_STATUS.INVALID,
                        PRIVACY_VERSION_STATUS.NONE));
    }

    private Single<ResultBuilder> ifPrivacyChanged(String privacyVersion, BIN_STATUS binCodeStatus) {
        return privacyPolicy.buildUseCaseSingle(AppConstant.MASTERCARD_COPY_UTILITY.Privacy.getValue())
                .flatMap(result -> Single.just(
                        new ResultBuilder(privacyVersion, binCodeStatus, checkPrivacyVersionChanged(privacyVersion, result.getCurrentVersion())))
                );
    }

    private PRIVACY_VERSION_STATUS checkPrivacyVersionChanged(String local, String server) {
        String[] localMajorVersion = local.split("\\.");
        String[] serverMajorVersion = server.split("\\.");
        if (!localMajorVersion[0].equals(serverMajorVersion[0])) {
            return PRIVACY_VERSION_STATUS.MAJOR_CHANGE;
        }
//        if(local.length() > localMajorVersion[0].length() && server.length() > serverMajorVersion[0].length()){
//            String localSubVersion = local.substring(localMajorVersion[0].length());
//            String serverSubVersion = server.substring(serverMajorVersion[0].length());
//            if(!localSubVersion.equals(serverSubVersion)){
//                return PRIVACY_VERSION_STATUS.MINOR_CHANGE;
//            }
//        }


        return PRIVACY_VERSION_STATUS.NONE;
    }

    public static class ResultBuilder {
        public String policyVersion;
        public BIN_STATUS binCodeStatus;
        public PRIVACY_VERSION_STATUS privacyPolicyChanged;
        boolean profileCreated;

        public ResultBuilder(String policyVersion, BIN_STATUS binCodeStatus, PRIVACY_VERSION_STATUS privacyPolicyChanged) {
            this.policyVersion = policyVersion;
            this.profileCreated = !TextUtils.isEmpty(policyVersion);
            this.binCodeStatus = binCodeStatus;
            this.privacyPolicyChanged = privacyPolicyChanged;
        }
    }

    public enum BIN_STATUS {VALID, INVALID, NONE}

    public enum PRIVACY_VERSION_STATUS {MAJOR_CHANGE, MINOR_CHANGE, NONE}
}
