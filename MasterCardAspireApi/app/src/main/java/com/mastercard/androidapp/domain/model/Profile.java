package com.mastercard.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.api.aspire.data.entity.preference.PreferenceData;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.domain.model.ProfileAspire;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by vinh.trinh on 4/27/2017.
 */
public class Profile implements Parcelable {

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String zipCode;
    private boolean agreed;
    private ProfileMetadata metadata;
    private String secretKey;
    private String pinCode;

    private String question;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAgreeStr() {
        String agree = "OFF";
        if (isAgreed()) {
            agree = "ON";
        }
        return agree;
    }

    /*value: on/off */
    public static boolean isAgree(String agree) {
        if (agree.equalsIgnoreCase("ON") ||
                agree.equalsIgnoreCase("YES")) {
            return true;
        }
        return false;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public boolean allIsEmpty() {
        return firstName.length() == 0 && lastName.length() == 0 && email.length() == 0 &&
                phone.length() == 0 && zipCode.length() == 0;
    }

    public boolean anyEmpty() {
        return firstName.length() == 0 || lastName.length() == 0 || email.length() == 0 ||
                phone.length() == 0 || zipCode.length() == 0;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public boolean isAgreed() {
        return agreed;
    }

    public void setAgreed(boolean agreed) {
        this.agreed = agreed;
    }

    public ProfileMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(ProfileMetadata metadata) {
        if (metadata.optStatus != null) this.agreed = metadata.optStatus.equals("1");
        this.metadata = metadata;
    }

    public void updateMetadata() {
        if (metadata == null) return;
        String newOptState = this.agreed ? "1" : "0";
        if (!newOptState.equals(metadata.optStatus)) {
            metadata.setOptStatus(newOptState);
            metadata.setOptStatusDate(getNewsLetterDate());
        }
    }

    public String getNewsLetterDate() {
        return new SimpleDateFormat("yyyy-MM-dd", Locale.US).format(new Date());
    }

    public Profile() {
        String temp = "";
        firstName = temp;
        lastName = temp;
        email = temp;
        phone = temp;
        zipCode = temp;
    }

    public Profile(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        this.firstName = json.getString("firstName");
        this.lastName = json.getString("lastName");
        this.email = json.getString("email");
        this.phone = json.getString("phone");
        this.zipCode = json.getString("zipCode");
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        json.put("firstName", firstName);
        json.put("lastName", lastName);
        json.put("email", email);
        json.put("phone", phone);
        json.put("zipCode", zipCode);
        return json;
    }

    private Profile(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        phone = in.readString();
        zipCode = in.readString();
        agreed = in.readInt() == 1;
        metadata = in.readParcelable(ProfileMetadata.class.getClassLoader());
        question = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(zipCode);
        dest.writeInt(agreed ? 1 : 0);
        dest.writeParcelable(metadata, 0);
        dest.writeString(question);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Profile> CREATOR = new Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };
}
