package com.mastercard.androidapp.datalayer.restapi;

import com.mastercard.androidapp.datalayer.entity.b2cutility.B2CGetMasterCardCopyRequest;
import com.mastercard.androidapp.datalayer.entity.b2cutility.B2CGetMasterCardCopyResponse;
import com.mastercard.androidapp.datalayer.entity.b2cutility.BINCheckRequest;
import com.mastercard.androidapp.datalayer.entity.b2cutility.BINCheckResponse;
import com.mastercard.androidapp.datalayer.entity.search.SearchRequest;
import com.mastercard.androidapp.datalayer.entity.search.SearchResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * for all other categories business except Dining
 */
public interface B2CUtilityApi {

    @POST("utility/GetClientCopy")
    Call<B2CGetMasterCardCopyResponse> getMasterCardCopy(@Body B2CGetMasterCardCopyRequest body);

    @POST("utility/SearchIAAndCCA")
    Call<SearchResponse> searchByKeyword(@Body SearchRequest body);

    @POST("utility/CheckMasterCardBIN")
    Call<BINCheckResponse> checkBINCode(@Body BINCheckRequest body);
}
