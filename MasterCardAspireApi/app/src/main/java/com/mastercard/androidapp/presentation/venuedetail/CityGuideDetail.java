package com.mastercard.androidapp.presentation.venuedetail;

import com.mastercard.androidapp.domain.model.explore.CityGuideDetailItem;
import com.mastercard.androidapp.presentation.base.BasePresenter;

/**
 * Created by ThuNguyen on 6/13/2017.
 */

public interface CityGuideDetail {

    interface View {
        void onGetCityGuideDetailFinished(CityGuideDetailItem cityGuideDetailItem);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getCityGuideDetail(Integer categoryId, Integer itemId);
    }

}
