package com.mastercard.androidapp.common.logic;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.net.Uri;

import com.mastercard.androidapp.App;

/**
 * Created by ThuNguyen on 6/30/2017.
 */

public class MapSearchHelper {
    private static final String MAP_SEARCH_FORMAT = "geo:%s,%s?q=%s";
    public static final String GOOGLE_MAP_SEARCH_LINK = "https://www.google.com/maps/search/?api=1&query=%s";
    private static final String GOOGLE_MAP_APP_PACKAGE_NAME = "com.google.android.apps.maps";
    private static MapSearchHelper instance;
    private MapSearchHelper(){}
    public static MapSearchHelper getInstance(){
        if(instance == null){
            instance = new MapSearchHelper();
        }
        return instance;
    }
    public void searchNameAndShowOnMap(Activity currentActivity, String addressName, String itemName){
        // Find latlng from address
        String uriStr = null;
        Address address = LocationUtils.getExactAddressFromAnyName(App.getInstance(), addressName);
        if(address != null && address.hasLatitude()){
            uriStr = String.format(MAP_SEARCH_FORMAT, String.valueOf(address.getLatitude()), String.valueOf(address.getLongitude()), itemName);
        }else{
            uriStr = String.format(MAP_SEARCH_FORMAT, "0", "0", addressName);
        }
        Uri uri = Uri.parse(uriStr);
        Intent intent = null;
        if(isAppInstalled(currentActivity, GOOGLE_MAP_APP_PACKAGE_NAME)){
            intent = new Intent(Intent.ACTION_VIEW, uri);
            intent.setPackage(GOOGLE_MAP_APP_PACKAGE_NAME);
        }else{
            uri = Uri.parse(String.format(GOOGLE_MAP_SEARCH_LINK, addressName));
            intent = new Intent(Intent.ACTION_VIEW, uri);
        }
        currentActivity.startActivity(intent);

    }
    private static boolean isAppInstalled(Context context, String packageName) {
        try {
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        }
        catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
