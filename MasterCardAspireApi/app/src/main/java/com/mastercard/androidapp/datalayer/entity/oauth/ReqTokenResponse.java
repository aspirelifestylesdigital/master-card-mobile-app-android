package com.mastercard.androidapp.datalayer.entity.oauth;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class ReqTokenResponse {
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @SerializedName("RequestToken")
    private String requestToken;
    @SerializedName("ExpirationTime")
    private String expirationTime;
    @SerializedName("Status")
    private String status;
    @SerializedName("Message")
    private String statusMessage;
    @SerializedName("message")
    private String message;

    public String consumerKey() {
        return consumerKey;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public String message() {
        return message;
    }

    public String expirationTime() {
        return expirationTime;
    }

    public String status() {
        return status;
    }

    public String statusMessage() {
        return statusMessage;
    }
}
