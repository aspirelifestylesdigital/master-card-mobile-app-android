package com.mastercard.androidapp.domain.mapper.profile;

import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.data.entity.profile.CreateProfileAspireRequest;
import com.api.aspire.data.entity.profile.ProfileAspireResponse;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.model.UserProfileOKTA;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.mastercard.androidapp.domain.model.Profile;

import java.util.List;

public class MapProfile {

    public MapProfile() {
    }

    private String handlePartyId(String partyId) {
        String valPartyId;
        if (TextUtils.isEmpty(partyId)) {
            valPartyId = null;
        } else {
            valPartyId = partyId;
        }
        return valPartyId;
    }

    /**
     * Check Party Id
     * Create Object ProfileAspire
     * Create Param Request
     */
    public CreateProfileCase.Params createProfile(Profile profile,
                                                  String partyId,
                                                  ChangeSecurityQuestion.Param securityQuestionParam) {
        //- check partyId
        String valPartyId = handlePartyId(partyId);

        //- object request
        ProfileAspire profileAspire = new ProfileAspire();
        //- secret in profile
        profileAspire.setSecretKey(securityQuestionParam.secret);

        //- map profile
        profileAspire.setFirstName(profile.getFirstName());
        profileAspire.setLastName(profile.getLastName());
        profileAspire.setEmail(profile.getEmail());
        profileAspire.setPhone(setPhone(profile.getPhone()));
        //- don't have field salutation and location
        //- special
        profileAspire.setZipCode(setZipCode(profile.getZipCode()));
        final String valBinCode = profile.getPinCode();
        profileAspire.setBinCodeRemote(valBinCode);

        String agreeNewLetterStatus = profile.getAgreeStr();
        String policyVersion = "";
        String valueNewLetterDate = "";
        if (profile.getMetadata() != null) {
            policyVersion = profile.getMetadata().policyVersion;
            policyVersion = TextUtils.isEmpty(policyVersion) ? "" : policyVersion;
            valueNewLetterDate = profile.getMetadata().getOptStatusDate();
            valueNewLetterDate = TextUtils.isEmpty(valueNewLetterDate) ? MapDataApi.NA_VALUE : valueNewLetterDate;
        }

        ProfileAspireResponse profileAspireResponse = MapDataProfile.mapRequest(profileAspire,
                agreeNewLetterStatus,
                policyVersion,
                valueNewLetterDate);

        String clientEmail = MapDataApi.getClientIdEmailApp(profile.getEmail());

        //-- set bin verifyMetadata
        int binNumber = CommonUtils.convertStringToInt(valBinCode);

        CreateProfileAspireRequest createProfileRequest = new CreateProfileAspireRequest(valPartyId,
                clientEmail,
                securityQuestionParam.secret,
                profileAspireResponse,
                binNumber);

        return new CreateProfileCase.Params(profileAspire, createProfileRequest, securityQuestionParam);
    }

    public Profile getProfile(ProfileAspire profileAspire) {
        Profile profile = new Profile();

        //- map profile
        profile.setFirstName(profileAspire.getFirstName());
        profile.setLastName(profileAspire.getLastName());
        profile.setEmail(profileAspire.getEmail());
        profile.setPhone(getPhoneFormat(profileAspire.getPhone()));
        profile.setZipCode(getZipCodeFormat(profileAspire.getZipCode()));
        profile.setAgreed(Profile.isAgree(ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(), ConstantAspireApi.APP_USER_PREFERENCES.NEWSLETTER_STATUS_KEY)));

        return profile;
    }

    public void getProfileRecoveryQuestion(Profile profile, UserProfileOKTA userProfileOKTA) {
        if (profile == null || userProfileOKTA == null ||
                userProfileOKTA.getCredentials() == null ||
                userProfileOKTA.getCredentials().getRecoveryQuestion() == null)
            return;

        //- map profile
        profile.setQuestion(userProfileOKTA.getCredentials().getRecoveryQuestion().getQuestion());
    }

    public ProfileAspire updateProfile(ProfileAspire profileAspire, Profile profileUpdated) {

        //- map profile
        profileAspire.setFirstName(profileUpdated.getFirstName());
        profileAspire.setLastName(profileUpdated.getLastName());
        profileAspire.setEmail(profileUpdated.getEmail());
        profileAspire.setPhone(setPhone(profileUpdated.getPhone()));
        profileAspire.setZipCode(setZipCode(profileUpdated.getZipCode()));

        //<editor-fold desc="Special case MC">
        List<ProfileAspireResponse.AppUserPreferences> appUserPreferencesList =
                ProfileLogicCore.isAppUserPrEmpty(profileAspire.getAppUserPreferences());

        String valLetterStatusOld = ProfileLogicCore.getAppUserPreference(appUserPreferencesList, ConstantAspireApi.APP_USER_PREFERENCES.NEWSLETTER_STATUS_KEY);
        String valLetterStatusNew = profileUpdated.getAgreeStr();

        //-- follow func updateMetadata()
        if (!valLetterStatusOld.equalsIgnoreCase(valLetterStatusNew)) {
            ProfileLogicCore.setAppUserPreference(appUserPreferencesList, ConstantAspireApi.APP_USER_PREFERENCES.NEWSLETTER_DATE_KEY,
                    profileUpdated.getNewsLetterDate());
        }
        ProfileLogicCore.setAppUserPreference(appUserPreferencesList, ConstantAspireApi.APP_USER_PREFERENCES.NEWSLETTER_STATUS_KEY, valLetterStatusNew);
        //</editor-fold>

        return profileAspire;
    }

    public String setPhone(String phone) {
        if (!TextUtils.isEmpty(phone) && phone.contains("-")) {
            phone = phone.replace("-", "");
        }
        return phone;
    }

    public String setZipCode(String zipCode) {
        if (!TextUtils.isEmpty(zipCode) && zipCode.contains("-")) {
            zipCode = zipCode.replace("-", "");
        }
        return zipCode;
    }

    public static String getPhoneFormat(String phone) {
        if (TextUtils.isEmpty(phone)) return "";

        //make sure phone right format and length = 10
        if (phone.contains("-"))
            phone = phone.replaceAll("-", "");
        if (phone.length() > 10) {
            phone = phone.substring(phone.length() - 10, phone.length());
        }

        StringBuilder builder = new StringBuilder(phone);
        builder.insert(0, "1-");
        builder.insert(5, "-");
        builder.insert(9, "-");
        return builder.toString();
    }

    public static String getZipCodeFormat(String zipCode) {
        if (TextUtils.isEmpty(zipCode)) return "";
        if (zipCode.contains("-"))
            zipCode = zipCode.replaceAll("-", "");

        if (zipCode.length() == 5) return zipCode;

        StringBuilder builder = new StringBuilder(zipCode);
        builder.insert(5, "-");
        return builder.toString();
    }

}
