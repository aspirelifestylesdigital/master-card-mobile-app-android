package com.mastercard.androidapp.presentation.profile;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.SecurityQuestion;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.logic.MaskerHelper;
import com.mastercard.androidapp.common.logic.Validator;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.presentation.base.BaseFragment;
import com.mastercard.androidapp.presentation.challengequestion.ListChallengeQuestion;
import com.mastercard.androidapp.presentation.challengequestion.ListChallengeQuestionPresenter;
import com.mastercard.androidapp.presentation.widget.CustomSpinner;
import com.mastercard.androidapp.presentation.widget.DropdownAdapter;
import com.mastercard.androidapp.presentation.widget.TextPattern;
import com.mastercard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class ProfileFragment extends BaseFragment implements ListChallengeQuestion.View {

    private static final String ARG_WHICH = "which";
    private final String STATE_INPUTS = "input";
    @BindView(R.id.profile_parent_view)
    View profileParentView;
    @BindView(R.id.profile_rootview)
    RelativeLayout rlProfileView;
    @BindView(R.id.scroll_view)
    ScrollView mainScrollView;
    @BindView(R.id.fakeViewToMoveButtonDown)
    View fakeViewToMoveButtonDown;
    @BindView(R.id.contain_view)
    View containView;
    @BindView(R.id.tv_title)
    AppCompatTextView tvTitle;
    @BindView(R.id.checkBox_profile)
    AppCompatCheckBox checkBox;
    @BindView(R.id.tv_checkbox)
    TextView tvCheckbox;
    @BindView(R.id.edt_first_name)
    ErrorIndicatorEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    ErrorIndicatorEditText edtLastName;
    @BindView(R.id.flEmailMask)
    View flEmailMask;
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_phone)
    ErrorIndicatorEditText edtPhone;
    @BindView(R.id.edt_zip_code)
    ErrorIndicatorEditText edtZipCode;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.btn_cancel)
    AppCompatButton btnCancel;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.content_submit)
    LinearLayout contentSubmit;

    @BindView(R.id.tvChallengeQuestion)
    TextView tvChallengeQuestion;
    @BindView(R.id.vLineChallengeQuestion)
    View vLineChallengeQuestion;
    @BindView(R.id.edt_answer_question)
    ErrorIndicatorEditText edtAnswerQuestion;

    private CustomSpinner mSpinnerChallengeQuestion;
    private ListChallengeQuestionPresenter listChallengeQuestionPresenter;

    //    boolean initial = false;
//    boolean firstNameMask;
    private ProfileEventsListener listener;
    private Profile profile = new Profile();
    private ProfileComparator profileComparator;
    private Validator validator = new Validator();
    private FocusChangeListener firstNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener lastNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener emailMasker = new FocusChangeListener(MaskerHelper.INPUT.EMAIL);
    private FocusChangeListener phoneMasker = new FocusChangeListener(MaskerHelper.INPUT.PHONE);
    private FocusChangeListener zipCodeMasker = new FocusChangeListener(MaskerHelper.INPUT.FREE);
    private TextPattern phonePattern;
    private AppCompatEditText editText;
    private String errMessage;
    private int fieldFocus;
    private boolean isKeyboardShow;
    private boolean isEdtMode;
    private boolean profileFromUpdate = true;

    private boolean isAnswerMasker = true;
    private AnswerFocusChangeListener answerFocusChangeListener;


    public static ProfileFragment newInstance(PROFILE profile) {
        Bundle args = new Bundle();
        args.putString(ARG_WHICH, profile.name());
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileEventsListener) {
            listener = (ProfileEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileEventsListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        listener.onFragmentCreated();
        PROFILE which = PROFILE.valueOf(getArguments().getString(ARG_WHICH));
        if (which == PROFILE.EDIT) {
            editProfileUISetup();
        } else {
            edtEmail.setOnFocusChangeListener(emailMasker);
            tvCheckbox.setText(R.string.profile_checkbox_text_create);
        }
        edtFirstName.setOnFocusChangeListener(firstNameMasker);
        edtLastName.setOnFocusChangeListener(lastNameMasker);
        edtPhone.setOnFocusChangeListener(phoneMasker);
        edtZipCode.setOnFocusChangeListener(zipCodeMasker);
        setAnswerMaskOnView();
        answerFocusChangeListener = new AnswerFocusChangeListener();
        edtAnswerQuestion.setOnFocusChangeListener(answerFocusChangeListener);
        initTextInteractListener();
        phonePattern = new TextPattern(edtPhone, "#-###-###-####", 2);
        edtZipCode.addTextChangedListener(new TextPattern(edtZipCode, "#####-####"));
        edtPhone.addTextChangedListener(phonePattern);

        // Make secure for Email and Phone
        ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
                return false;
            }

            public void onDestroyActionMode(ActionMode actionMode) {
            }
        };
        edtEmail.setCustomSelectionActionModeCallback(actionModeCallback);
        edtPhone.setCustomSelectionActionModeCallback(actionModeCallback);

        listChallengeQuestionPresenter = new ListChallengeQuestionPresenter();
        listChallengeQuestionPresenter.attach(this);
        listChallengeQuestionPresenter.getChallengeQuestions();

        // Click guard
        ClickGuard.guard(btnSubmit, btnCancel);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        listChallengeQuestionPresenter.detach();
    }

    @OnClick({R.id.profile_rootview, R.id.content_wrapper, R.id.content_submit, R.id.flEmailMask})
    public void onClick(View view) {
        hideSoftKey();
        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(emailMasker.original);
        }
    }


    //<editor-fold desc="Custom Spinner">
    private CustomSpinner setUpCustomSpinner(TextView anchor,
                                             List<SecurityQuestion> data) {
        CustomSpinner customSpinner = new CustomSpinner(getActivity(), anchor);

        List<String> questionChallenges = new ArrayList<>();
        for (SecurityQuestion item : data) {
            questionChallenges.add(item.getQuestionText());
        }

        DropdownAdapter dropdownAdapter = new DropdownAdapter(getActivity(),
                R.layout.item_challenge_question_dropdown,
                questionChallenges, 52);
        customSpinner.setDropdownAdapter(dropdownAdapter);
        customSpinner.setmSpinnerListener(spinnerListener);
        customSpinner.setPopupHeightListQuestion();
        return customSpinner;
    }

    CustomSpinner.SpinnerListener spinnerListener = new CustomSpinner.SpinnerListener() {
        @Override
        public void onArchorViewClick() {
            ViewUtils.hideSoftKey(getView());
            if (getActivity() != null
                    && getActivity().getCurrentFocus() != null) {
                getActivity().getCurrentFocus().clearFocus();
            }
        }

        @Override
        public void onItemSelected(int index) {
            if (answerFocusChangeListener == null
                    || mSpinnerChallengeQuestion == null
                    || profileComparator == null
                    || edtAnswerQuestion == null) {
                return;
            }

            String questionCurrent = answerFocusChangeListener.getCacheQuestionProfile();
            String newQuestion = listChallengeQuestionPresenter.getQuestionContent(index);
            if(newQuestion.equalsIgnoreCase(questionCurrent)){
                profileComparator.setQuestion(false);
                setAnswerMaskOnView();

            }else {
                //-- change question
                profileComparator.setQuestion(true);
                edtAnswerQuestion.setText("");
            }

            profileComparator.handleSelectQuestion();
            answerFocusChangeListener.original = "";

        }
    };
    //</editor-fold>

    private void initTextInteractListener() {
        CopyPasteHandler copyPasteHandler = new CopyPasteHandler(validator);
        //everytime user paste text to edittext, onTextPasted get called.
        edtFirstName.setTextInteractListener(copyPasteHandler);
        edtLastName.setTextInteractListener(copyPasteHandler);
        edtPhone.setTextInteractListener(copyPasteHandler);
        edtZipCode.setTextInteractListener(copyPasteHandler);

        rlProfileView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                rlProfileView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rlProfileView.getRootView().getHeight();

                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    if (!isKeyboardShow) {
                        isKeyboardShow = true;
                        // keyboard is opened
                        mainScrollView.smoothScrollTo(0, (int) edtFirstName.getTop() + contentWrapper.getTop());
                        scaleView();
                        processAfterKeyboardToggle(true);
                    }
                } else {
                    // keyboard is closed
                    if (isKeyboardShow) {
                        isKeyboardShow = false;
                        fakeViewToMoveButtonDown.setVisibility(View.VISIBLE);
                        processAfterKeyboardToggle(false);
                    }
                }
            }
        });
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        profileFromUpdate = true;
        hideSoftKey();
        storeUp();
        fieldFocus = checkHasFocus();
        resetErrorText();
        if ( (profileComparator.isQuestion()
                || profileComparator.isAnswerChanged() || getAnswerInputSpaceOnView())
                && profileComparator.isProfileChanged()) {

            if (formValidationAll()) {
                String question = getQuestionContentOnView();
                listener.onProfileSubmitAndUpdateSecurity(profile, question, getAnswerOnView());
            }
        } else if (profileComparator.isQuestion()
                || profileComparator.isAnswerChanged() || getAnswerInputSpaceOnView()) {
            if (formValidationSecurity()) {
                String question = getQuestionContentOnView();
                listener.onUpdateSecurityQuestion(question, getAnswerOnView());
            }
        } else {
            if (formValidation()) {
                listener.onProfileSubmit(profile);
            }
        }

    }

    private String getAnswerOnView() {
        return isAnswerMasker ? answerFocusChangeListener.original : edtAnswerQuestion.getText().toString().trim();
    }

    private boolean getAnswerInputSpaceOnView() {
        String answer = isAnswerMasker ? answerFocusChangeListener.original : edtAnswerQuestion.getText().toString();
        boolean result = false;
        if (!TextUtils.isEmpty(answer)
                && answer.replace(" ","").length() == 0) {
            result = true;
        }
        return result;
    }

    @OnClick(R.id.btn_cancel)
    void onCancel(View view) {
        profileFromUpdate = false;

        setAnswerMaskOnView();
        if (answerFocusChangeListener != null) {
            answerFocusChangeListener.resetAnswerField();
        }

        hideSoftKey();
        storeUp();

        fieldFocus = checkHasFocus();
        resetErrorText();
        listener.onProfileCancel();
    }

    @OnClick(R.id.tv_checkbox)
    void onCheckBoxTick(View view) {
        checkBox.toggle();
    }

    @OnClick(R.id.contain_view)
    void onClickOnProfile(View view) {
//        hideSoftKey();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        storeUp();
        outState.putParcelable(STATE_INPUTS, profile);
        super.onSaveInstanceState(outState);
    }

    private boolean formValidationAll() {
        checkoutAll();
        if (editText != null) {
            viewFocusError();
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private boolean formValidationSecurity() {
        checkoutSecurity();
        if (editText != null) {
            viewFocusError();
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private boolean formValidation() {
        checkout();
        if (editText != null) {
            viewFocusError();
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }


    private void checkout() {
        editText = null;
        StringBuilder messageBuilder = buildError();
        errMessage = messageBuilder.toString();
    }

    private void checkoutAll() {
        editText = null;
        StringBuilder messageBuilder = buildError();

        String answer = getAnswerOnView();
        if (!validator.answerSecurity(answer)) {
            if (editText == null) editText = edtAnswerQuestion;
            edtAnswerQuestion.setError("");
            if (messageBuilder.length() > 0
                    && !messageBuilder.substring(messageBuilder.toString().length() - 1).equals("\n")) {
                messageBuilder.append("\n");
            }
            messageBuilder.append(getString(R.string.invalid_answer));
        } else {
            edtAnswerQuestion.setError(null);
        }

        errMessage = messageBuilder.toString();
    }

    private void checkoutSecurity() {
        editText = null;
        StringBuilder messageBuilder = new StringBuilder();

        String valAnswer = getAnswerOnView();

        if (!validator.answerSecurity(valAnswer)) {
            editText = edtAnswerQuestion;
            edtAnswerQuestion.setError("");
            messageBuilder.append(getString(R.string.invalid_answer));
        } else {
            edtAnswerQuestion.setError(null);
        }

        errMessage = messageBuilder.toString();
    }


    private StringBuilder buildError() {
        StringBuilder messageBuilder = new StringBuilder();
        if (!validator.name(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0)
                messageBuilder.append(getString(R.string.input_err_invalid_name, "first"));
        } else {
            edtFirstName.setError(null);
        }
        if (!validator.name(profile.getLastName())) {
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(getString(R.string.input_err_invalid_name, "last"));
            }
        } else {
            edtLastName.setError(null);
        }
        if (!validator.email(profile.getEmail())) {
            if (editText == null) editText = edtEmail;
            edtEmail.setError("");
            if (profile.getEmail().length() > 0) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(getString(R.string.input_err_invalid_email));
            }
        } else {
            edtEmail.setError(null);
        }
        if (!validator.phone(profile.getPhone())) {
            if (editText == null) editText = edtPhone;
            edtPhone.setError("");
            if (profile.getPhone().length() > 0) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(getString(R.string.input_err_invalid_phone));
            }
        } else {
            edtPhone.setError(null);
        }
        if (!validator.zipCode(profile.getZipCode())) {
            if (editText == null) editText = edtZipCode;
            edtZipCode.setError("");
            if (profile.getZipCode().length() > 0) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(getString(R.string.input_err_invalid_zip));
            }
        } else {
            edtZipCode.setError(null);
        }
        if (editText != null) {
            String temp = messageBuilder.toString();
            messageBuilder = new StringBuilder("");
            if (profile.anyEmpty()) {
                messageBuilder.append(getString(R.string.input_err_required));
            }
            if (!profile.allIsEmpty()) {
                if (messageBuilder.length() > 0 && temp.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(temp);
            }
        }
        return messageBuilder;
    }

    private void storeUp() {
        firstNameMasker.original = firstNameMasker.original.trim();
        lastNameMasker.original = lastNameMasker.original.trim();
        emailMasker.original = emailMasker.original.trim();
        phoneMasker.original = phoneMasker.original.trim();
        String firstName = firstNameMasker.original;
        String lastName = lastNameMasker.original;
        String email = emailMasker.original;
        String phone = phoneMasker.original;
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setPhone(phone);
        profile.setEmail(email);
        profile.setZipCode(edtZipCode.getText().toString());
        profile.setAgreed(checkBox.isChecked());
    }

    private int checkHasFocus() {
        if (edtFirstName.hasFocus()) {
            profile.setFirstName(edtFirstName.getText().toString().trim());
            return 1;
        }
        if (edtLastName.hasFocus()) {
            profile.setLastName(edtLastName.getText().toString().trim());
            return 2;
        }
        if (edtEmail.hasFocus()) {
            profile.setEmail(edtEmail.getText().toString().trim());
            return 3;
        }
        if (edtPhone.hasFocus()) {
            profile.setPhone(edtPhone.getText().toString().trim());
            return 4;
        }
        // zip code
        return 5;
    }

    public void reloadProfileAfterUpdated() {
        if (profile != null) {
            String question = getQuestionContentOnView();
            profile.setQuestion(question);
        }
        loadProfile(profile);
        //set 7* when update profile success
        setAnswerMaskOnView();
    }

    private String getQuestionContentOnView() {
        if (listChallengeQuestionPresenter == null
                || mSpinnerChallengeQuestion == null) {
            return "";
        }

        return listChallengeQuestionPresenter.getQuestionContent(
                mSpinnerChallengeQuestion.getSelectionPosition());
    }

    private void setAnswerMaskOnView() {
        edtAnswerQuestion.setText(MaskerHelper.mask(MaskerHelper.INPUT.ANSWER,""));
    }

    void loadProfile(Profile profile) {
        if (profile == null)
            return;

        //<editor-fold desc="Security">
        handleQuestionOnView(profile.getQuestion());
        //</editor-fold>

        //this.profile.setMetadata(profile.getMetadata());
        firstNameMasker.original = profile.getFirstName();
        lastNameMasker.original = profile.getLastName();
        phoneMasker.original = profile.getPhone();
        emailMasker.original = profile.getEmail();
        zipCodeMasker.original = profile.getZipCode();
        edtFirstName.setText(profile.getFirstName());
        edtLastName.setText(profile.getLastName());
        edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, profile.getEmail()));
        edtPhone.setText(profile.getPhone());
        edtZipCode.setText(profile.getZipCode());
        checkBox.setChecked(profile.isAgreed());

        if (profileFromUpdate) {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, false);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, false);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, false);
        } else {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, fieldFocus == 1);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, fieldFocus == 2);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, fieldFocus == 4);
        }

        profileComparator = new ProfileComparator(
                edtFirstName,
                edtLastName,
                edtEmail,
                edtPhone,
                edtZipCode,
                checkBox,
                btnSubmit,
                btnCancel,
                edtAnswerQuestion
        );
        profileComparator.original = profile;
        resetErrorText();
        editProfileUISetup();
    }

    private void handleQuestionOnView(String question) {
        //- load remote get question
        if(TextUtils.isEmpty(question)
                && !TextUtils.isEmpty(answerFocusChangeListener.getCacheQuestionProfile())){
            question = answerFocusChangeListener.getCacheQuestionProfile();
        }
        answerFocusChangeListener.original = "";
        if (!TextUtils.isEmpty(question)
                && mSpinnerChallengeQuestion != null) {
            int indexOfQuestion = mSpinnerChallengeQuestion.getTextPosition(question);
            mSpinnerChallengeQuestion.setSelectionPosition(indexOfQuestion);
            answerFocusChangeListener.setCacheQuestionProfile(question);
        }
    }

    private void editProfileUISetup() {
        isEdtMode = true;
        tvTitle.setVisibility(View.INVISIBLE);
        RelativeLayout.LayoutParams layoutParams = ((RelativeLayout.LayoutParams) mainScrollView.getLayoutParams());
        layoutParams.addRule(RelativeLayout.BELOW, 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        mainScrollView.invalidate();
        flEmailMask.setVisibility(View.VISIBLE);
        edtEmail.setInputType(InputType.TYPE_NULL);
        edtEmail.setTextIsSelectable(false);
        edtEmail.setBackgroundResource(R.drawable.disable_underline_indicator);
        int padding = getResources().getDimensionPixelOffset(R.dimen.edittext_padding_small);
        edtEmail.setPadding(padding, padding, padding, padding);
        edtEmail.setLongClickable(false);
        edtEmail.setCursorVisible(false);
        edtEmail.setEnabled(false);
        edtEmail.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode actionMode) {
            }
        });

        btnSubmit.setText(getString(R.string.update));
        btnCancel.setVisibility(View.VISIBLE);
        btnCancel.setEnabled(false);
        btnCancel.setClickable(false);
        btnSubmit.setEnabled(false);
        btnSubmit.setClickable(false);

    }

    private void resetErrorText() {
        edtFirstName.setError(null);
        edtLastName.setError(null);
        edtEmail.setError(null);
        edtZipCode.setError(null);
        edtPhone.setError(null);
        edtAnswerQuestion.hideErrorColorLine();
    }

    private void processAfterKeyboardToggle(boolean isKeyboardShow) {
        View currentView = getActivity().getCurrentFocus();
        if (currentView != null && currentView instanceof EditText) {
            ((EditText) currentView).setCursorVisible(isKeyboardShow);

            if (!isKeyboardShow) {
                currentView.clearFocus();
                profileParentView.requestFocus();
                //currentView.getOnFocusChangeListener().onFocusChange(currentView, false);
            } else {
                //containView.clearFocus();
                //currentView.requestFocus();
            }
        }
    }

    void viewFocusError() {
        if (editText != null) {
            if(editText.getId() == edtAnswerQuestion.getId()){
                answerFocusChangeListener.setResetField(false);
                editText.requestFocus();
                answerFocusChangeListener.setResetField(true);
            }else{
                editText.requestFocus();
            }
            editText.setError("");
            editText.setCursorVisible(true);
            editText.setSelection(editText.getText().toString().length());
        }

    }
    void invokeSoftKey() {
        viewFocusError();
        ViewUtils.showSoftKey(editText);
    }
    void hideSoftKey() {
        ViewUtils.hideSoftKey(getView());
    }

    @Override
    public void onPause() {
        super.onPause();
        hideSoftKey();
    }

    public void scaleView() {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                fakeViewToMoveButtonDown.getY(),
                fakeViewToMoveButtonDown.getY() + fakeViewToMoveButtonDown.getHeight(), // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(false); // Needed to keep the result of the animation
        anim.setDuration(0);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //fakeViewToMoveButtonDown.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fakeViewToMoveButtonDown.startAnimation(anim);
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void dismissProgressDialog() {

    }

    @Override
    public void showErrorMessage(ErrCode errCode, String extraMsg) {

    }

    @Override
    public void showSuccessMessage() {

    }

    @Override
    public void getChallengeQuestions(List<SecurityQuestion> questions) {
        mSpinnerChallengeQuestion = setUpCustomSpinner(tvChallengeQuestion, questions);
        mSpinnerChallengeQuestion.setNone(false);
    }

    public enum PROFILE {CREATE, EDIT}

    public interface ProfileEventsListener {
        void onProfileSubmit(Profile profile);

        void onProfileInputError(String message);

        void onProfileCancel();

        void onFragmentCreated();

        void onUpdateSecurityQuestion(String question, String answer);

        void onProfileSubmitAndUpdateSecurity(Profile profile, String question, String answer);
    }


    private class AnswerFocusChangeListener implements View.OnFocusChangeListener {

        private boolean isResetField = true;
        private String original = "";
        private String cacheQuestionProfile = "";

        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (hasFocus) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mainScrollView.scrollTo(0, mainScrollView.getBottom());
                    }
                }, 500);
                if (edtAnswerQuestion != null) {
                    isAnswerMasker = false;
                    edtAnswerQuestion.setCursorVisible(true);
                    //-- touch answer field -> clear out
                    if (isResetField) {
                        if (TextUtils.isEmpty(original)
                                || MaskerHelper.answerMaskValue(original)) {
                            edtAnswerQuestion.setText("");
                        } else {
                            edtAnswerQuestion.setText(original);
                        }

                    } else if (!TextUtils.isEmpty(original)) {
                        edtAnswerQuestion.setText(original);
                    } else{
                        edtAnswerQuestion.setText("");
                    }
                }
            } else {
                String tempAnswer = edtAnswerQuestion.getText().toString().trim();
                original = MaskerHelper.answerMaskValue(tempAnswer) ? "" : tempAnswer;

                //<editor-fold desc="Rule show field answer">
                //-- case: field answer have text -> mask text
                //-- case: field answer empty touch out
                //- same question : show 7*
                //- different question: always show empty
                isAnswerMasker = true;
                if (mSpinnerChallengeQuestion == null) {
                    return;
                }
                String answerOnView;
                if(TextUtils.isEmpty(original)){
                    String questionCurrent = mSpinnerChallengeQuestion.getTextSelectedSafe();

                    if(cacheQuestionProfile.equalsIgnoreCase(questionCurrent)){
                        //- same question : show 7*
                        answerOnView = MaskerHelper.mask(MaskerHelper.INPUT.ANSWER, "");
                    }else {
                        answerOnView = "";
                    }

                }else{
                    answerOnView = MaskerHelper.mask(MaskerHelper.INPUT.ANSWER, original);
                }
                edtAnswerQuestion.setText(answerOnView);
                //</editor-fold>
            }
        }

        void setResetField(boolean resetField) {
            isResetField = resetField;
        }

        public void setCacheQuestionProfile(String cacheQuestionProfile) {
            this.cacheQuestionProfile = cacheQuestionProfile;
        }

        public String getCacheQuestionProfile() {
            return cacheQuestionProfile;
        }

        public void resetAnswerField() {
            original = "";
        }
    }

    private class FocusChangeListener implements View.OnFocusChangeListener {

        MaskerHelper.INPUT input;
        private String original = "";

        FocusChangeListener(MaskerHelper.INPUT type) {
            input = type;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            AppCompatEditText view = (AppCompatEditText) v;
            if (input == MaskerHelper.INPUT.PHONE) {
                if (hasFocus) {
                    if (original.length() == 0) {
                        view.removeTextChangedListener(phonePattern);
                        original = "1-";
                        view.setText(original);
                        view.setSelection(2);
                        view.addTextChangedListener(phonePattern);
                        return;
                    }
                } else {
                    String text = view.getText().toString();
                    if (text.length() == 2) {
                        original = "";
                        view.removeTextChangedListener(phonePattern);
                        view.setText(original);
                        view.addTextChangedListener(phonePattern);
                        return;
                    }
                }
            }
            if (hasFocus) {
                view.setText(original);

                if (isEdtMode) {
                    edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
                }
            } else {
                String text = view.getText().toString().trim();
                original = text;
                view.setText(MaskerHelper.mask(input, original));
            }
            view.setCursorVisible(hasFocus);

        }
    }
}
