package com.mastercard.androidapp.presentation.checkout.signInV2;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.ProfileAspire;
import com.mastercard.androidapp.presentation.base.BasePresenter;

public class SignIn {
    interface View {
        void proceedToHome();
        void onCheckPassCodeFailure();
        void toBINCheckout(ProfileAspire profileAspire,boolean messageInclude);
        void toPrivacyPolicy(ProfileAspire profileAspire,boolean isMajorChange);
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void doSignIn(String email, String password);
        void abort();
    }
}
