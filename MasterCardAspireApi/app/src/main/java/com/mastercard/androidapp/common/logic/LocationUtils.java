package com.mastercard.androidapp.common.logic;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by ThuNguyen on 6/30/2015.
 */
public class LocationUtils {
    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000; // 1 second

    private static final int ADDRESS_RESULT_MAXIMUM = 10;

    public static double distanceBetweenTwoLocations(double latitude1, double longitude1, double latitude2, double longitude2){
        Location location1 = new Location("Location 1");
        location1.setLatitude(latitude1);
        location1.setLongitude(longitude1);

        Location location2 = new Location("Location 2");
        location2.setLatitude(latitude2);
        location2.setLongitude(longitude2);

        return Math.abs(location1.distanceTo(location2));
    }

    public static String getGeoCountry(Context context, Location location){
        String countryName = "";
        if(context!=null) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(
                        location.getLatitude(),
                        location.getLongitude(),
                        // In this sample, get just a single address.
                        1);
            } catch (IOException ioException) {
            } catch (IllegalArgumentException illegalArgumentException) {
            }

            // Handle case where no address was found.
            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);
                countryName = address.getCountryName();
            }
        }
        return countryName;
    }
    public static List<Address> getAddressListFromAnyName(Context context, String name){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addressList = null;
        try{
            addressList = geocoder.getFromLocationName(name, ADDRESS_RESULT_MAXIMUM);
        }catch(IOException ioException){

        }catch (IllegalArgumentException illegalArgumentException){}

        // Filter the address via country code "vn"
        if(addressList != null && addressList.size() > 0){
            Iterator<Address> iterator = addressList.iterator();
            while(iterator.hasNext()){
                Address address = iterator.next();
                if(address.getCountryCode() == null || !address.getCountryCode().toLowerCase().equals("vn")){
                    iterator.remove();
                }
            }
        }
//        if(addressList != null && addressList.size() > 0){
//            List<Address> newAddressList = new ArrayList<>();
//            for(int index = 0; index < addressList.size(); index++){
//                Address address = addressList.get(index);
//                if(address != null && address.getCountryCode() != null && address.getCountryCode().toLowerCase().equals("vn")){
//                    newAddressList.add(address);
//                }
//            }
//            return newAddressList;
//        }
        return addressList;
    }
    public static Address getExactAddressFromAnyName(Context context, String name){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addressList = null;
        try{
            addressList = geocoder.getFromLocationName(name, 1);
        }catch(IOException ioException){

        }catch (IllegalArgumentException illegalArgumentException){}

        // Filter the address via country code "vn"
        if(addressList != null && addressList.size() > 0){
            return addressList.get(0);
        }
        return null;
    }
    public static List<Address> getAddressListFromLocation(Context context, double latitude, double longitude){
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addressList = null;
        try{
            addressList = geocoder.getFromLocation(latitude, longitude, 1);
        }catch(IOException ioException){

        }catch (IllegalArgumentException illegalArgumentException){}

        return addressList;
    }
    /**
     * Get display address
     * 0: street, 1: district, 2: province
     * @param address
     * @return
     */
    public static String getDisplayAddress(Address address){
        String displayLine = address.getAddressLine(0);

        String district = address.getAddressLine(1);
        if(district != null && district.length() > 0){
            displayLine += ", " + district;
        }
        String province = address.getAddressLine(2);
        if(province != null && province.length() > 0){
            displayLine += ", " + province;
        }
        return displayLine;
    }
}
