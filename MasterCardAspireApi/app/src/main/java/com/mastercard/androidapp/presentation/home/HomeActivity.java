package com.mastercard.androidapp.presentation.home;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.common.constant.CityData;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.common.constant.RequestCode;
import com.mastercard.androidapp.common.constant.ResultCode;
import com.mastercard.androidapp.presentation.base.NavDrawerActivity;
import com.mastercard.androidapp.presentation.explore.ExploreFragment;
import com.mastercard.androidapp.presentation.info.MasterCardUtilityActivity;
import com.mastercard.androidapp.presentation.inspiregallery.InspireGalleryFragment;
import com.mastercard.androidapp.presentation.profile.MyProfileActivity;
import com.mastercard.androidapp.presentation.request.AskConciergeActivity;
import com.mastercard.androidapp.presentation.selectcategory.CategoryPresenter;
import com.mastercard.androidapp.presentation.selectcity.SelectCityActivity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by vinh.trinh on 5/3/2017.
 */

public class HomeActivity extends NavDrawerActivity implements
        NavigationView.OnNavigationItemSelectedListener
        , HomeFragment.HomeEventsListener
        , InspireGalleryFragment.InspireGalleryEventListener {

    public static final int REQUEST_CODE_ASK_CONCIERGE = 300;
    private static final String STATE_LAST_SELECTED = "last_selected";
    private int selectedMenuItemId = R.id.nav_home;
    @Nullable
    @BindView(R.id.title)
    AppCompatTextView tvTitle;
    @Nullable
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private View askConciergeBell;
    private DialogHelper dialogHelper;
    private HomeViewPresenter viewPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);
        ButterKnife.bind(this);
        navigationView.setNavigationItemSelectedListener(this);
        setupToolbar(toolbar);

        setTitle(R.string.title_home);
        viewPresenter=new HomeViewPresenter(this);
        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            addFragment(HomeFragment.newInstance());
        } else {
            selectedMenuItemId = savedInstanceState.getInt(STATE_LAST_SELECTED);
            navigationView.setCheckedItem(selectedMenuItemId);
        }

        if (!CityData.citySelected()) {
            PreferencesStorageAspire prefStorage = new PreferencesStorageAspire(getApplicationContext());
            String selectedCity = prefStorage.selectedCity();
            CityData.setSelectedCity(selectedCity);
        }

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.HOME.getValue());
        }
        // Observe the back stack changed to decide the color of menu item
        getSupportFragmentManager().addOnBackStackChangedListener(() -> {
            FragmentManager fm = getSupportFragmentManager();
            Fragment currentFrag = fm.findFragmentById(R.id.fragment_place_holder);
            if(currentFrag != null){
                if(currentFrag instanceof HomeFragment){
                    selectedMenuItemId = R.id.nav_home;
                }else if(currentFrag instanceof ExploreFragment){
                    selectedMenuItemId = R.id.nav_explore;
                }
            }
            if(selectedMenuItemId != 0){
                navigationView.setCheckedItem(selectedMenuItemId);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigationView.setCheckedItem(selectedMenuItemId);
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        Drawable icConcierge = menu.findItem(R.id.action_ask_concierge).getIcon();
        ViewUtils.menuTintColors(this, icConcierge);
        return true;
    }

    boolean guarded = false;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!guarded) {
            ClickGuard.guard(ButterKnife.findById(toolbar, R.id.action_ask_concierge));
            guarded = true;
        }
        if (item.getItemId() == R.id.action_ask_concierge) {
            navigateToAskConcierge();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        //set back toolbar color to default before switch between fragments.
        onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        switch (id) {
            case R.id.nav_home:
                setTitle(getResources().getString(R.string.title_home));
                addFragment(HomeFragment.newInstance());
                break;
            case R.id.nav_ask:
                navigateToAskConcierge();
                break;
            case R.id.nav_explore:
                navigateToExplore();
                break;
            case R.id.nav_profile:
                navigateToMyProfile();
                break;
            case R.id.nav_change_pass:
                viewPresenter.navigateToChangePassword();
                break;
            case R.id.nav_about:
                //App.getInstance().track("Test", "Test", "Linh test");
                navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.About);
                break;
            case R.id.nav_privacy:
                navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
                break;
            case R.id.nav_term_of_use:
                navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY.TermsOfUse);
                break;
            case R.id.nav_sign_out:
                // Track "Sign out"
                App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.SIGN_OUT.getValue(),
                        AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                        AppConstant.GA_TRACKING_LABEL.SIGN_OUT.getValue());
                viewPresenter.signOut();
                break;

        }
        navigationView.setCheckedItem(selectedMenuItemId);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        int backStackEntryCount = fm.getBackStackEntryCount();
        if (backStackEntryCount > 1) {
            FragmentManager.BackStackEntry backEntry = fm.getBackStackEntryAt(backStackEntryCount - 1);
            if (backEntry.getName().equals(InspireGalleryFragment.class.getSimpleName())) {
                onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
//            fm.popBackStack();
            selectedMenuItemId = R.id.nav_home;
            setTitle(R.string.title_home);
            navigationView.setCheckedItem(selectedMenuItemId);
            if (!backEntry.getName().equalsIgnoreCase(HomeFragment.class.getSimpleName())) {
                addFragmentAfterClearAll(HomeFragment.newInstance());
            } else {
                finish();
            }
        } else {
            finish();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_LAST_SELECTED, selectedMenuItemId);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setTitle(int title) {
        tvTitle.setText(title);
    }

    @Override
    public void setTitle(CharSequence title) {
        tvTitle.setText(title);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        FragmentManager fm = getSupportFragmentManager();
        if (requestCode == REQUEST_CODE_ASK_CONCIERGE) {
            if (resultCode == ResultCode.RESULT_OK) {
                Fragment exploreFragment = fm.findFragmentByTag(ExploreFragment.class.getSimpleName());
                if (exploreFragment == null) {
                    navigateToExplore();
                } else {
                    fm.beginTransaction().show(exploreFragment).commit();
                    selectedMenuItemId = R.id.nav_explore;
                    navigationView.setCheckedItem(selectedMenuItemId);
                }
            }
        } else if (requestCode == RequestCode.SELECT_CITY) {
            if (resultCode == ResultCode.RESULT_OK) {
                navigateToExplore();
            } else {
                setTitle(getResources().getString(R.string.title_home));
                // can't access to Explore page until a city is selected
                InspireGalleryFragment inspireGalleryFragment = (InspireGalleryFragment) fm
                        .findFragmentByTag(InspireGalleryFragment.class.getSimpleName());
                if (inspireGalleryFragment != null) {
                    fm.popBackStack();
                }
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.getBooleanExtra(IntentConstant.RESET_EXPLORE_PAGE, false)) {
            navigateToExplore();
        }

//        if(intent != null && intent.getBooleanExtra(IntentConstant.RESET_EXPLORE_PAGE, false)){
//            if(!CityData.citySelected()) {
//                Intent intent1 = new Intent(this, SelectCityActivity.class);
//                startActivityForResult(intent1, RequestCode.SELECT_CITY);
//                return;
//            }
//            CategoryPresenter.CATEGORY_ALL = true;
//            setTitle(R.string.explore);
//            addSingleInstanceFragment(ExploreFragment.newInstance());
//            lastSelected = R.id.nav_explore;
//            navigationView.setCheckedItem(lastSelected);
//        }
    }

    void addSingleInstanceFragment(Fragment fragment){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        addFragmentToBackstack(fragment, ft);
    }

    @Override
    public void navigateToInspirationGallery() {
        setTitle(getResources().getString(R.string.title_home));
        addFragment(InspireGalleryFragment.newInstance());
    }

    @Override
    public void navigateToExplore() {
        if (!CityData.citySelected()) {
            Intent intent = new Intent(this, SelectCityActivity.class);
            startActivityForResult(intent, RequestCode.SELECT_CITY);
            return;
        }
        CategoryPresenter.CATEGORY_ALL = true;
        FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentByTag(ExploreFragment.class.getSimpleName()) != null)
            fm.popBackStack();
        setTitle(R.string.explore);
        replaceFragment(ExploreFragment.newInstance(), null);
        selectedMenuItemId = R.id.nav_explore;
        navigationView.setCheckedItem(selectedMenuItemId);
    }

    @Override
    public void navigateToAskConcierge() {
        Intent intent = new Intent(this, AskConciergeActivity.class);
        startActivityForResult(intent, REQUEST_CODE_ASK_CONCIERGE);
    }

    private void navigateToMyProfile() {
        Intent intent = new Intent(this, MyProfileActivity.class);
        startActivity(intent);
    }

    private void navigateToMasterCardUtility(AppConstant.MASTERCARD_COPY_UTILITY utilityType) {
        Intent intent = new Intent(this, MasterCardUtilityActivity.class);
        intent.putExtra(IntentConstant.MASTERCARD_COPY_UTILITY, utilityType);
        startActivity(intent);
    }

//    void addFragment(Fragment fragment) {
//        getSupportFragmentManager()
//                .beginTransaction()
//                .add(R.id.fragment_place_holder, fragment,
//                        fragment.getClass().getSimpleName())
//                .commit();
//    }

    void replaceFragment(Fragment fragment, String backEntryKey) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_place_holder, fragment,
                        fragment.getClass().getSimpleName())
                .addToBackStack(backEntryKey == null ? "home" : backEntryKey)
                .commit();
    }


    void addFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        //if(fm.getBackStackEntryCount()>1) fm.popBackStack();
        Fragment curFragment = fm.findFragmentById(R.id.fragment_place_holder);
        Fragment fragmentPopped = fm.findFragmentByTag(fragment.getClass().getSimpleName());
        FragmentTransaction ft = fm.beginTransaction();
        if (curFragment != null &&
                curFragment.getClass().getSimpleName().equalsIgnoreCase(fragment.getClass().getSimpleName())) {
            //ft.show(fragment); // Do nothing
            return;
        } else if (fragment instanceof HomeFragment) {
            clearAllFragments();
            // Add fragment to backstack
            ft.add(R.id.fragment_place_holder, fragment,
                    fragment.getClass().getSimpleName());
            ft.addToBackStack(fragment.getClass().getSimpleName())
                    .commit();
        } else {
            if (fragmentPopped == null) {
                addFragmentToBackstack(fragment, ft);
            } else {
                if (fragment instanceof ExploreFragment) {
                    clearAllFragmentExceptFragment(fragment);
                } else {
                    addFragmentToBackstack(fragment, ft);
                }
            }
        }
    }

    void addFragmentToBackstack(Fragment fragment, FragmentTransaction ft) {
        // Add fragment to backstack
        ft.add(R.id.fragment_place_holder, fragment,
                fragment.getClass().getSimpleName());
        ft.addToBackStack(fragment.getClass().getSimpleName())
                .commit();
    }

    public void clearAllFragments() {
        FragmentManager fm = getSupportFragmentManager();
        if (fm.getBackStackEntryCount() > 0) {
            fm.popBackStackImmediate(null,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    void clearAllFragmentExceptFragment(Fragment notRemovedFragment) {
        FragmentManager fm = getSupportFragmentManager();
        List<Fragment> fragmentList = fm.getFragments();
        if (fragmentList != null && fragmentList.size() > 0) {
            FragmentTransaction ft = fm.beginTransaction();
            for (int index = 0; index < fragmentList.size(); index++) {
                if (!fragmentList.get(index).getClass().getSimpleName().equalsIgnoreCase(notRemovedFragment.getClass().getSimpleName())) {
                    ft.remove(fragmentList.get(index));
                }
            }
            ft.commit();
        }
    }

    void addFragmentAfterClearAll(Fragment fragment) {
        clearAllFragments();
        addFragment(fragment);
    }


    void replaceFragmentNoBackStack(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_place_holder, fragment,
                        fragment.getClass().getSimpleName())
                .commit();
    }

    void adjustBarColor() {
        FragmentManager fm = getSupportFragmentManager();
        int backStackEntryCount = fm.getBackStackEntryCount();
        if (backStackEntryCount > 0) {
            FragmentManager.BackStackEntry backEntry = fm.getBackStackEntryAt(backStackEntryCount - 1);
            if (!backEntry.getName().equals(InspireGalleryFragment.class.getSimpleName())) {
                onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            } else {
                onPageColorChanged(((InspireGalleryFragment) fm.findFragmentById(R.id.fragment_place_holder)).getCurrentColor());
            }
        }
    }

    @Override
    public void onPageColorChanged(int color) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place_holder);
        if (currentFragment != null && currentFragment instanceof InspireGalleryFragment) {
            setStatusBarColor(color);
            setToolbarColor(color);
        } else {
            setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }

        //setNavViewBackgroundColor(color);
    }

    @Override
    protected void onDrawerOpened() {
       /* tvTitle.setVisibility(View.GONE);
        onPageColorChanged(ContextCompat.getColor(this, R.color.colorPrimaryDark));*/
//        menuBell.setVisible(false);
    }

    @Override
    protected void onDrawerClosed() {
       /* tvTitle.setVisibility(View.VISIBLE);
        adjustBarColor();*/
//        menuBell.setVisible(true);
    }

    private float startX;
    private float startY;

    /*@Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            startX = event.getX();
            startY = event.getY();
        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            float endX = event.getX();
            float endY = event.getY();
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START, false);
                askConciergeBell = ButterKnife.findById(toolbar, R.id.action_ask_concierge);
                int[] bellLocation = new int[2];
                askConciergeBell.getLocationOnScreen(bellLocation);
                Rect toolbarViewRect = new Rect(bellLocation[0],
                        bellLocation[1],
                        bellLocation[0] + askConciergeBell.getWidth(),
                        bellLocation[1] + askConciergeBell.getHeight());
                if (ViewUtils.isAClick(startX, endX, startY, endY)) {
                    if (toolbarViewRect.contains((int) event.getX(), (int) event.getY())) {
                        navigateToAskConcierge();
                    }
                }
            } else {
                return super.dispatchTouchEvent(event);
            }
        }
        return super.dispatchTouchEvent(event);
    }*/

    public void alertDialog(String message) {
        dialogHelper.alert(null, message);
    }
}
