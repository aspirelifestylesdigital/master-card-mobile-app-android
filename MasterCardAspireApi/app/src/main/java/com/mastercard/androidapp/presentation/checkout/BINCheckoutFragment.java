package com.mastercard.androidapp.presentation.checkout;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;
import android.widget.TextView;

import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.common.logic.EntranceLock;
import com.mastercard.androidapp.presentation.base.BaseFragment;
import com.mastercard.androidapp.presentation.widget.TextPattern;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.JustifiedTextView;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class BINCheckoutFragment extends BaseFragment {
    public static final String TERM_OF_USE_TEMP_HYPERLINK = "https://www.termofuse.com";
    public static final String PRIVACY_POLICY_TEMP_HYPERLINK = "https://www.privacypolicy.com";
    public static final String ANONYMOUS_HYPERLINK = "https://www.abc.com";

    private BINCheckoutListener listener;
    @BindView(R.id.scroll_view)
    ScrollView mainScrollView;
    @BindView(R.id.fakeViewToMoveButtonDown)
    View fakeViewToMoveButtonDown;
    @BindView(R.id.layout_container)
    View layoutContainer;
    @BindView(R.id.tv_greeting)
    TextView tvGreeting;
    @BindView(R.id.contentBIN)
    View contentBinView;
    @BindView(R.id.edt_bin_code)
    AppCompatEditText edtBINCode;
    @BindView(R.id.checkbox_acknowledge)
    AppCompatCheckBox chbAcknowledge;
    @BindView(R.id.text_acknowledge)
    JustifiedTextView textAcknowledge;
    @BindView(R.id.btn_bin_checkout)
    AppCompatButton btnSubmit;
    private boolean checked = false;
    private boolean _6digit = false;
    private EntranceLock entranceLock = new EntranceLock();

    public static BINCheckoutFragment newInstance() {
        Bundle args = new Bundle();
        BINCheckoutFragment fragment = new BINCheckoutFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BINCheckoutListener) {
            listener = (BINCheckoutListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement BINCheckoutListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bin_checkout, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            edtBINCode.setText(savedInstanceState.getString("input"));

        }
        edtBINCode.addTextChangedListener(new TextPattern(edtBINCode, "####-##"));
        edtBINCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                _6digit = s.length() == 7;
                btnSubmit.setEnabled(checked && _6digit);
                btnSubmit.setClickable(checked && _6digit);
            }
        });
        chbAcknowledge.setOnCheckedChangeListener((buttonView, isChecked) -> {
            checked = isChecked;
            textAcknowledge.setPressed(checked);
            textAcknowledge.setTextColor(getResources().getColor(R.color.checkbox_text_selected_color));
            btnSubmit.setEnabled(_6digit && isChecked);
            btnSubmit.setClickable(_6digit && isChecked);
        });
        createClickSpans();
        ClickGuard.guard(btnSubmit);
        btnSubmit.setClickable(false);
        contentBinView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                contentBinView.getWindowVisibleDisplayFrame(r);
                int screenHeight = contentBinView.getRootView().getHeight();

                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    edtBINCode.setCursorVisible(true);
                    //mainScrollView.smoothScrollTo(0, (int) tvGreeting.getTop() + layoutContainer.getTop());
//                    mainScrollView.post(new Runnable() {
//                        @Override
//                        public void run() {
//                            mainScrollView.fullScroll(View.FOCUS_DOWN);
//                        }
//                    });
                    scaleView();
                } else {
                    fakeViewToMoveButtonDown.setVisibility(View.VISIBLE);
                    // keyboard is closed
                    edtBINCode.setCursorVisible(false);
                }
            }
        });
        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.BIN_CODE.getValue());
        }
    }

    public void scaleView() {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                fakeViewToMoveButtonDown.getY(),
                fakeViewToMoveButtonDown.getY() + fakeViewToMoveButtonDown.getHeight(), // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(false); // Needed to keep the result of the animation
        anim.setDuration(0);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //fakeViewToMoveButtonDown.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fakeViewToMoveButtonDown.startAnimation(anim);
    }

    @OnClick({R.id.submit_layout, R.id.contentBIN, R.id.layout_container, R.id.tv_greeting, R.id.tv_blabla, R.id.tv_blabla2})
    public void onClick(View view) {
        hideSoftKey();
    }

    @OnClick(R.id.btn_bin_checkout)
    public void submit(View view) {
        hideSoftKey();
        String input = edtBINCode.getText().toString();
        listener.onBINCodeSubmit(input.replace("-", ""));
    }

    @OnClick(R.id.text_acknowledge)
    public void onCheckBoxTick(View view) {
        chbAcknowledge.toggle();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("input", edtBINCode.getText().toString());
        super.onSaveInstanceState(outState);
    }


    void hideSoftKey() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        boolean hide = imm.hideSoftInputFromWindow(edtBINCode.getWindowToken(), 0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void createClickSpans() {
        String acknowledgeTextFormat = "<a href='%s' style='text-decoration:none;color:white'>I acknowledge that I have read and agree to the </a><a href='%s'>Terms of Use</a><a href='%s' style='text-decoration:none;color:white'> and </a><a href='%s'>Privacy Policy</a>";
        String acknowledgeText = String.format(acknowledgeTextFormat, ANONYMOUS_HYPERLINK, TERM_OF_USE_TEMP_HYPERLINK, ANONYMOUS_HYPERLINK, PRIVACY_POLICY_TEMP_HYPERLINK);
        textAcknowledge.getSettings().setJavaScriptEnabled(true);
        textAcknowledge.getSettings().setDomStorageEnabled(true);
        textAcknowledge.getSettings().setAllowFileAccess(true);

        textAcknowledge.setText(acknowledgeText);
        textAcknowledge.setJustifyTextViewListener(new JustifiedTextView.IJustifyTextViewListener() {
            @Override
            public void onContentScroll(boolean hitToBottom) {
            }

            @Override
            public void onHyperLinkClicked(String url) {
                if (!entranceLock.isClickContinuous()) {
                    if (url.contains(TERM_OF_USE_TEMP_HYPERLINK)) {
                        listener.onTermOfUseClick();
                        hideSoftKey();
                    } else if (url.contains(PRIVACY_POLICY_TEMP_HYPERLINK)) {
                        listener.onPrivacyPolicyClick();
                        hideSoftKey();
                    } else {
                        chbAcknowledge.toggle();
                    }
                }
            }

        });
        /*ClickableSpan privacyPolicyClick = new ClickableSpan() {
            @Override
            public void onClick(View widget) {listener.onPrivacyPolicyClick();
            }

        };
        ClickableSpan termOfUseClick = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                listener.onTermOfUseClick();
            }
        };
        String acknowledgeText = getString(R.string.acknowledge_text);
        String termsOfUseText = "Terms of Use";
        String privacyPolicyText = "Privacy Policy";
        int termsOfUseStart = acknowledgeText.indexOf(termsOfUseText);
        int termsOfUseEnd = termsOfUseStart + termsOfUseText.length();
        int privacyPolicyStart = acknowledgeText.indexOf(privacyPolicyText);
        int privacyPolicyEnd = privacyPolicyStart + privacyPolicyText.length();

        SpannableString ss = new SpannableString(acknowledgeText);
        ss.setSpan(termOfUseClick, termsOfUseStart, termsOfUseEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(privacyPolicyClick, privacyPolicyStart, privacyPolicyEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorStrongAccent)), termsOfUseStart, termsOfUseEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), R.color.colorStrongAccent)), privacyPolicyStart, privacyPolicyEnd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        textAcknowledge.setText(ss);
        textAcknowledge.setMovementMethod(LinkMovementMethod.getInstance());*/
    }

    public interface BINCheckoutListener {
        void onBINCodeSubmit(String input);

        void onTermOfUseClick();

        void onPrivacyPolicyClick();
    }
}
