package com.mastercard.androidapp.common.constant;

/**
 * Created by tung.phan on 5/5/2017.
 */

public interface SharedPrefConstant {
    String PROFILE = "ppfile";
    String BIN_CODE = "bin_code";
    String METADATA = "pMetadata";
    String SELECTED_CITY = "city";

}
