package com.mastercard.androidapp.common.constant;

/**
 * Created by tung.phan on 5/23/2017.
 */

public interface IntentConstant {

    String SELECTED_CATEGORY = "selected_category";
    String SELECTED_CITY = "selected_city";
    String CATEGORY_ID = "category_id";
    String INDEX_CITY_GUIDE_CATEGORY_SELECT = "id_from_sub_category_select";
    String SUB_CATEGORY_NAME = "sub_category_name";
    String SUB_CATEGORY_IMAGE = "sub_category_image";
    String CATEGORY_NAME = "category_name";
    String SUPPER_CATEGORY = "supper_category";
    String EXPLORE_DETAIL = "explore_detail";
    String SUGGESTED_CONCIERGE = "suggested_concierge";
    String MASTERCARD_COPY_UTILITY = "mastercard_copy_utility";
    String INVOKE_KEYBOARD = "invoke_keyboard";
    String RESET_EXPLORE_PAGE = "reset_explore_page";
    String ASK_SCREEN_TO_SELECT_CITY = "ask_screen_to_select_city";

    String SIGN_IN_PROFILE = "sign_in_profile";
    String SPLASH_SCREEN_HIDE_NAV_BACK = "splash_screen_hide_nav_back";
    String EMAIL_VALUES_FORGOT = "email_values_forgot";
}
