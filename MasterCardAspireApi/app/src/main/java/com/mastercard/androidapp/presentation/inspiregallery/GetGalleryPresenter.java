package com.mastercard.androidapp.presentation.inspiregallery;

import com.mastercard.androidapp.domain.model.GalleryViewPagerItem;
import com.mastercard.androidapp.domain.usecases.GetGalleries;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public class GetGalleryPresenter implements GetGallery.Presenter {

    private CompositeDisposable disposables;
    private GetGallery.View view;
    private GetGalleries getGalleries;

    GetGalleryPresenter(GetGalleries getGalleries) {
        disposables = new CompositeDisposable();
        this.getGalleries = getGalleries;
    }

    @Override
    public void attach(GetGallery.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getGalleryList() {
        disposables
                .add(getGalleries.param(null)
                .on(Schedulers.io(), AndroidSchedulers.mainThread())
                .execute(new GetGalleryObserver()));
    }

    private final class GetGalleryObserver extends DisposableSingleObserver<List<GalleryViewPagerItem>> {
        @Override
        public void onSuccess(List<GalleryViewPagerItem> exploreRViewItemList) {
            view.onGetGalleryListFinished(exploreRViewItemList);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.onUpdateFailed();
            dispose();
        }
    }
}
