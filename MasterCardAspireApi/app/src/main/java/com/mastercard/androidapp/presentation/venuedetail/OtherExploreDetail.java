package com.mastercard.androidapp.presentation.venuedetail;

import com.mastercard.androidapp.domain.model.explore.OtherExploreDetailItem;
import com.mastercard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface OtherExploreDetail {

    interface View {
        void onGetContentFullFinished(OtherExploreDetailItem exploreRViewItem);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getContent(Integer contentId);
    }

}
