package com.mastercard.androidapp.datalayer.entity.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class Member {
    @SerializedName("FIRSTNAME")
    private String firstName;
    @SerializedName("LASTNAME")
    private String lastName;
    @SerializedName("EMAIL")
    private String email;
    @SerializedName("MOBILENUMBER")
    private String mobileNumber;
    @SerializedName("ZIPCODE")
    private String zipCode;

    public String firstName() {
        return firstName;
    }

    public String lastName() {
        return lastName;
    }

    public String email() {
        return email;
    }

    public String mobileNumber() {
        return mobileNumber;
    }

    public String zipCode() {
        return zipCode;
    }
}
