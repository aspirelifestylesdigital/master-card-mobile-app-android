package com.mastercard.androidapp.domain.usecases;

import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.SignInCase;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.presentation.splashscreen.SubsequentAccessPresenter;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class GetUserApp {

    private SignInCase signInCase;
    private CheckBIN checkBIN;
    private GetMasterCardCopy privacyPolicy;
    private PreferencesStorageAspire preferencesStorageAspire;

    //- variable
    private ProfileAspire profileAspireCache;

    public GetUserApp(SignInCase signInCase,
                      CheckBIN checkBIN,
                      GetMasterCardCopy privacyPolicy,
                      PreferencesStorageAspire preferencesStorageAspire) {

        this.signInCase = signInCase;
        this.checkBIN = checkBIN;
        this.privacyPolicy = privacyPolicy;
        this.preferencesStorageAspire = preferencesStorageAspire;
    }

    public Single<SubsequentAccessPresenter.ResultBuilder> signInCleanPreference(SignInCase.Params params) {
        return Completable.create(e -> {
            //clean reset preference
            preferencesStorageAspire.clear();
            e.onComplete();
        }).andThen(signInCase.loadProfileOKTACheckStatus(params.email)
                .andThen(signInCase(params))
        );
    }

    private Single<SubsequentAccessPresenter.ResultBuilder> signInCase(SignInCase.Params params) {
        return signInCase.loadProfileNoSaveStorage(params).flatMap(profileAspire -> {
                    profileAspireCache = profileAspire;
                    return binCheckWithProfileAspire(profileAspire)
                            .flatMap(resultBuilder ->
                                    ifPrivacyChanged(resultBuilder.policyVersion,
                                            resultBuilder.binCodeStatus));
                }
        );
    }

    public ProfileAspire getProfileAspireCache() {
        return profileAspireCache;
    }

    private Single<SubsequentAccessPresenter.ResultBuilder> binCheckWithProfileAspire(
            ProfileAspire profileAspire) {
        if (profileAspire == null) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }

        String binCodeInProfile = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);

        String policyVersion = ProfileLogicCore.getAppUserPreference(profileAspire.getAppUserPreferences(),
                ConstantAspireApi.APP_USER_PREFERENCES.POLICY_VERSION_KEY);

        if (TextUtils.isEmpty(binCodeInProfile)) {
            return binCheck(policyVersion);
        }

        return checkBIN.buildUseCaseSingle(binCodeInProfile)
                .map(validBinCode ->
                        new SubsequentAccessPresenter.ResultBuilder(
                                policyVersion,
                                validBinCode ? SubsequentAccessPresenter.BIN_STATUS.VALID : SubsequentAccessPresenter.BIN_STATUS.INVALID,
                                SubsequentAccessPresenter.PRIVACY_VERSION_STATUS.NONE
                        )
                );

    }


    private Single<SubsequentAccessPresenter.ResultBuilder> binCheck(String privacyVersion) {
        return Single.just(
                new SubsequentAccessPresenter.ResultBuilder(
                        privacyVersion, SubsequentAccessPresenter.BIN_STATUS.INVALID,
                        SubsequentAccessPresenter.PRIVACY_VERSION_STATUS.NONE));
    }

    /**
     * @return
     */
    private Single<SubsequentAccessPresenter.ResultBuilder> ifPrivacyChanged(
            String privacyVersion, SubsequentAccessPresenter.BIN_STATUS binCodeStatus) {

        return privacyPolicy.buildUseCaseSingle(AppConstant.MASTERCARD_COPY_UTILITY.Privacy.getValue())
                .flatMap(result -> Single.just(
                        new SubsequentAccessPresenter.ResultBuilder(privacyVersion,
                                binCodeStatus, checkPrivacyVersionChanged(
                                privacyVersion, result.getCurrentVersion())))
                );
    }

    private SubsequentAccessPresenter.PRIVACY_VERSION_STATUS checkPrivacyVersionChanged(
            String local, String server) {
        String[] localMajorVersion = local.split("\\.");
        String[] serverMajorVersion = server.split("\\.");
        if (!localMajorVersion[0].equals(serverMajorVersion[0])) {
            return SubsequentAccessPresenter.PRIVACY_VERSION_STATUS.MAJOR_CHANGE;
        }
        return SubsequentAccessPresenter.PRIVACY_VERSION_STATUS.NONE;
    }

    public void saveProfileLocalSignInCase() {
        if (profileAspireCache == null) {
            return;
        }
        Completable.create(e -> {
            String binCodeValue = ProfileLogicCore.getAppUserPreference(profileAspireCache.getAppUserPreferences(),
                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY);
            preferencesStorageAspire.editor().binCode(binCodeValue).build().save();
            e.onComplete();
        }).andThen(signInCase.saveProfileStorage(profileAspireCache))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe();
    }

}
