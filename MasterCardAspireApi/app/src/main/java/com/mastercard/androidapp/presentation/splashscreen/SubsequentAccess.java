package com.mastercard.androidapp.presentation.splashscreen;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 5/4/2017.
 */

public interface SubsequentAccess {

    interface View {
        void toPrivacyPolicy(boolean majorChange);
        /** message include == true : BIN checkout screen will show dialog to let user know */
        void toBINcheckout(boolean messageInclude);
        void toLoginScreen();
        void proceedToHome();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();

        void onErrorGetToken();

        void hideProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void process();
    }

}
