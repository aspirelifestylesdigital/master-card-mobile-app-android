package com.mastercard.androidapp.datalayer.entity.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class GetDetailsRequest {
    @SerializedName("AccessToken")
    private String accessToken;
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @SerializedName("Functionality")
    private String functionality;
    @SerializedName("OnlineMemberId")
    private String onlineMemberID;
    @SerializedName("uniqueID")
    private String uniqueID;

    public GetDetailsRequest(String accessToken, String onlineMemberID, String uniqueID) {
        this.accessToken = accessToken;
        this.consumerKey = "MCD";
        this.functionality = "GetUserDetails";
        this.onlineMemberID = onlineMemberID;
        this.uniqueID = uniqueID;
    }
}
