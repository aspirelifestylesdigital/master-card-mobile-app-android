package com.mastercard.androidapp.presentation.explore;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.logic.GlideHelper;
import com.mastercard.androidapp.domain.model.explore.ExploreRViewItem;
import com.mastercard.androidapp.presentation.selectcategory.CategoryPresenter;
import com.mastercard.androidapp.presentation.widget.FooterLoaderAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by tung.phan on 5/9/2017.
 */

public class ExploreRViewAdapter
        extends FooterLoaderAdapter<ExploreRViewAdapter.ExploreItemViewHolder> {
    private static final int INFO_ROW_MAXIMUM = 5;
    private final int expectedImageWidth;
    private final List<ExploreRViewItem> data;
    private ExploreRViewAdapterListener listener;
    private int selectedPosition = -1;
    private boolean isSearchAction;

    public void setSearchAction(boolean searchAction) {
        isSearchAction = searchAction;
    }

    public ExploreRViewItem getItem(int pos) {
        return data.get(pos);
    }

    public List<ExploreRViewItem> getData() {
        return new ArrayList<>(data);
    }

    public boolean isEmpty() {
        return this.data.size() == 0;
    }

    public void clear() {
        if(this.data.size() == 0) return;
        this.data.clear();
        setItemCount(0);
        notifyDataSetChanged();
    }

    public void add(ExploreRViewItem datum) {
        this.data.add(datum);
        setItemCount(this.data.size());
        notifyDataSetChanged();
    }

    public void append(List<ExploreRViewItem> data) {
        this.data.addAll(data);
        setItemCount(this.data.size());
        notifyDataSetChanged();
    }

    public void swapData(List<ExploreRViewItem> data) {
        this.data.clear();
        this.data.addAll(data);
        setItemCount(this.data.size());
        notifyDataSetChanged();
    }

    ExploreRViewAdapter(Context context, List<ExploreRViewItem> data, ExploreRViewAdapterListener listener) {
        super(context);
//        picasso = imageLoader(context);
        this.data = new ArrayList<>(data);
        this.listener = listener;
        setItemCount(this.data.size());
        expectedImageWidth = context.getResources().getDimensionPixelSize(R.dimen.explore_text_align_image);
    }

    private void bindViewTypeNormal(ExploreItemViewHolder holder, ExploreRViewItem exploreRViewItem) {
        holder.title.setText(Html.fromHtml(exploreRViewItem.title));
        if(TextUtils.isEmpty(exploreRViewItem.description)) {
            holder.shortDescription.setVisibility(View.INVISIBLE);
        } else {
            holder.shortDescription.setVisibility(View.VISIBLE);
            holder.shortDescription.setText(exploreRViewItem.description);
        }
        holder.specialDescription.setText(Html.fromHtml(exploreRViewItem.summary));

        if (holder.imageContainer.getVisibility() != View.VISIBLE) {
            holder.imageContainer.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.contentContainer.getLayoutParams();
            params.setMarginStart(expectedImageWidth);
            holder.contentContainer.setLayoutParams(params);
        }
        ImageView iv;
        switch (exploreRViewItem.getItemType()) {
            case DINING:
                iv = holder.diningImage;
                holder.diningImage.setVisibility(View.VISIBLE);
                holder.image.setVisibility(View.GONE);
                break;
            default:
                iv = holder.image;
                holder.image.setVisibility(View.VISIBLE);
                holder.diningImage.setVisibility(View.GONE);
                break;
        }
        if(TextUtils.isEmpty(exploreRViewItem.imageURL)) {
            GlideHelper.getInstance().loadImage(R.drawable.img_placeholder, R.drawable.img_placeholder, iv, expectedImageWidth);
        } else {
            GlideHelper.getInstance().loadImage(exploreRViewItem.imageURL, R.drawable.img_placeholder, iv, expectedImageWidth);
        }
        if(CategoryPresenter.CATEGORY_ALL) {
            holder.showMask(exploreRViewItem.getDisplayCategory());
        } else {
            holder.hideMask();
        }
        if (exploreRViewItem.hasStar) {
            holder.star.setVisibility(View.VISIBLE);
        } else {
            holder.star.setVisibility(View.GONE);
        }
        // Hide short description(address) for Experiences
//        if(exploreRViewItem.categoryName() != null && exploreRViewItem.categoryName().equalsIgnoreCase("Experiences")){
//            holder.shortDescription.setVisibility(View.INVISIBLE);
//        }else{
//            holder.shortDescription.setVisibility(View.VISIBLE);
//        }
    }

    private void bindViewTypeNoImage(ExploreItemViewHolder holder, ExploreRViewItem exploreRViewItem) {

        if (holder.imageContainer.getVisibility() != View.GONE) {
            holder.imageContainer.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.contentContainer.getLayoutParams();
            params.setMarginStart(0);
            holder.contentContainer.setLayoutParams(params);
        }
        holder.title.setText(Html.fromHtml(exploreRViewItem.title));
        if(TextUtils.isEmpty(exploreRViewItem.description)) {
            holder.shortDescription.setVisibility(View.INVISIBLE);
        } else {
            holder.shortDescription.setVisibility(View.VISIBLE);
            holder.shortDescription.setText(exploreRViewItem.description);
        }
        holder.specialDescription.setText((Html.fromHtml(exploreRViewItem.summary)));
        if(exploreRViewItem.hasStar) {
            holder.star.setVisibility(View.VISIBLE);
        } else{
            holder.star.setVisibility(View.GONE);
        }
        // Hide short description(address) for Experiences
        if(exploreRViewItem.categoryName() != null && exploreRViewItem.categoryName().equalsIgnoreCase("Experiences")){
            holder.shortDescription.setVisibility(View.INVISIBLE);
        }else{
            holder.shortDescription.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.explore_rview_item, parent, false);
        return new ExploreItemViewHolder(itemView);
    }

    @Override
    public void bindYourViewHolder(RecyclerView.ViewHolder holder, int position) {
        ExploreItemViewHolder viewHolder = (ExploreItemViewHolder) holder;
        viewHolder.itemView.setSelected(position == selectedPosition);
        final ExploreRViewItem commonRViewItem = data.get(position);
        if (commonRViewItem.getItemType() == ExploreRViewItem.ItemType.CITY_GUIDE ||
                commonRViewItem.getItemType() == ExploreRViewItem.ItemType.SEARCH || isSearchAction) {
            bindViewTypeNoImage(viewHolder, commonRViewItem);
        } else {
            bindViewTypeNormal(viewHolder, commonRViewItem);
        }
    }

    class ExploreItemViewHolder extends RecyclerView.ViewHolder {
        private View imageContainer;
        private LinearLayout contentContainer;
        private View mask;
        private TextView tvMask;
        private ImageView image, star, diningImage;
        private TextView title, shortDescription, specialDescription;
        ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener;
        ExploreItemViewHolder(View view) {
            super(view);
            imageContainer = ButterKnife.findById(view, R.id.image_container);
            contentContainer = ButterKnife.findById(view, R.id.content_container);
            mask = ButterKnife.findById(view, R.id.img_mask);
            tvMask = ButterKnife.findById(view, R.id.img_mask_text);
            image = ButterKnife.findById(view, R.id.image);
            diningImage = ButterKnife.findById(view, R.id.diningImage);
            star = ButterKnife.findById(view, R.id.star);
            title = ButterKnife.findById(view, R.id.title);
            shortDescription = ButterKnife.findById(view, R.id.short_description);
            specialDescription = ButterKnife.findById(view, R.id.special_description);
            view.setOnClickListener(v -> {
                selectedPosition = getAdapterPosition();
                listener.onItemClick(selectedPosition);
            });
            onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    if (title.getWidth() > 0 && title.getLayout() != null) {
                        int lineOfTitle = title.getLayout().getLineCount();
                        int descriptionMaxLine = INFO_ROW_MAXIMUM - (lineOfTitle + 1);
                        /*if (specialDescription.getLineCount() >= 3) {
                            if (descriptionMaxLine == 2) {
                                if (specialDescription.getLayout() != null) {
                                    int lineEndIndex = specialDescription.getLayout().getLineEnd(1);
                                    CharSequence text = specialDescription.getText().subSequence(0, lineEndIndex - 3) + "...";
                                    specialDescription.setText(text);
                                }
                            } else if (descriptionMaxLine == 3 && specialDescription.getLineCount() > 3) {
                                if (specialDescription.getLayout() != null) {
                                    int lineEndIndex = specialDescription.getLayout().getLineEnd(2);
                                    CharSequence text = specialDescription.getText().subSequence(0, lineEndIndex - 3) + "...";
                                    specialDescription.setText(text);
                                }
                            }
                        }*/
                        specialDescription.setMaxLines(descriptionMaxLine);
                    }
                }
            };
            /*title.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
                if(title.getWidth() > 0 && title.getLayout() != null){
                    int firstLineWidth = (int)title.getLayout().getLineWidth(0);
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    layoutParams.leftMargin = firstLineWidth;
                    layoutParams.topMargin = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, App.getInstance().getResources().getDisplayMetrics());
                    star.setLayoutParams(layoutParams);
                }
            });*/
            title.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
        }
        void showMask(String text) {
            if(text.equalsIgnoreCase("Experiences")){
                tvMask.setTextSize(TypedValue.COMPLEX_UNIT_SP, 11);
            }else{
                tvMask.setTextSize(TypedValue.COMPLEX_UNIT_SP, 13);
            }
            tvMask.setVisibility(View.VISIBLE);
            tvMask.setText(text);
            mask.setVisibility(View.VISIBLE);
        }

        void hideMask() {
            tvMask.setVisibility(View.GONE);
            mask.setVisibility(View.GONE);
        }
    }

    public interface ExploreRViewAdapterListener {
        void onItemClick(int pos);
    }
}
