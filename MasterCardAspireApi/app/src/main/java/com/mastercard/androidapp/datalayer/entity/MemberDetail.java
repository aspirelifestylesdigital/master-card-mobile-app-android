package com.mastercard.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class MemberDetail {
    @Expose
    @SerializedName("UniqueID")
    private String uniqueID;
    @Expose
    @SerializedName("CustomerID")
    private String customerID;
    @Expose
    @SerializedName("CardType")
    private String cardType;
    @Expose
    @SerializedName("CardNumber")
    private String cardNumber;
    @Expose
    @SerializedName("CardDate")
    private String cardDate;
    @Expose
    @SerializedName("PolicyDate")
    private String policyDate;
    @Expose
    @SerializedName("PolicyVersion")
    private String policyVersion;
    @Expose
    @SerializedName("OptStatus")
    private String optStatus;
    @Expose
    @SerializedName("OptDate")
    private String optStatusDate;
    @Expose
    @SerializedName("AppLastUsedDate")
    private String appLastUsedDate;
    @Expose
    @SerializedName("ValidBinNumber")
    private String validBinNumber;
    @Expose
    @SerializedName("BinEndDate")
    private String binEndDate;
    @Expose
    @SerializedName("OnlineMemberDetailId")
    private String onlineMemberDetailID;

    public MemberDetail(String uniqueID, String cardType, String cardDate, String policyDate,
                        String policyVersion, String optStatus, String optStatusDate, String appLastUsedDate,
                        String validBinNumber, String binEndDate) {
        this.uniqueID = uniqueID;
        this.customerID = validBinNumber;
        this.cardType = cardType;
        this.cardNumber = validBinNumber;
        this.cardDate = cardDate;
        this.policyDate = policyDate;
        this.policyVersion = policyVersion;
        this.optStatus = optStatus;
        this.optStatusDate = optStatusDate;
        this.appLastUsedDate = appLastUsedDate;
        this.validBinNumber = validBinNumber;
        this.binEndDate = binEndDate;
    }

    public MemberDetail(String uniqueID, String cardType, String cardDate, String policyDate,
                        String policyVersion, String optStatus, String optStatusDate, String appLastUsedDate,
                        String validBinNumber, String binEndDate, String onlineMemberDetailID) {
        this.onlineMemberDetailID = onlineMemberDetailID;
        this.uniqueID = uniqueID;
        this.customerID = validBinNumber;
        this.cardType = cardType;
        this.cardNumber = validBinNumber;
        this.cardDate = cardDate;
        this.policyDate = policyDate;
        this.policyVersion = policyVersion;
        this.optStatus = optStatus;
        this.optStatusDate = optStatusDate;
        this.appLastUsedDate = appLastUsedDate;
        this.validBinNumber = validBinNumber;
        this.binEndDate = binEndDate;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public String getCustomerID() {
        return customerID;
    }

    public String getCardType() {
        return cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getCardDate() {
        return cardDate;
    }

    public String getPolicyDate() {
        return policyDate;
    }

    public String getPolicyVersion() {
        return policyVersion;
    }

    public String getOptStatus() {
        return optStatus;
    }

    public String getOptStatusDate() {
        return optStatusDate;
    }

    public String getAppLastUsedDate() {
        return appLastUsedDate;
    }

    public String getValidBinNumber() {
        return validBinNumber;
    }

    public String getBinEndDate() {
        return binEndDate;
    }

    public String getOnlineMemberDetailID() {
        return onlineMemberDetailID;
    }
}