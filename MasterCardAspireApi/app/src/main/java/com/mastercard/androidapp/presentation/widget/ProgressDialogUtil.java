package com.mastercard.androidapp.presentation.widget;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;

import com.mastercard.androidapp.R;


/**
 * Created by ThuNguyen on 3/1/2016.
 */
public class ProgressDialogUtil {
    private static ProgressDialog mProgressDialog;
    private static boolean isLoading = false;
    public static void showLoading(Context context){
        showLoading(context, "");
    }
    public static void showLoading(Context context, String title) {
        try {
            if (!isLoading) {
                if (mProgressDialog != null && mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                }

                mProgressDialog = new ProgressDialog(context);

                mProgressDialog.getWindow().setDimAmount(0.0f);
                mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        isLoading = false;
                    }
                });
                mProgressDialog.setMessage(title);
                mProgressDialog.setCancelable(true);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.setIndeterminate(true);
//                mProgressDialog.setIndeterminateDrawable(ContextCompat.getDrawable(context, R.drawable.progress_dialog_icon_animation));
                mProgressDialog.show();
                mProgressDialog.setContentView(R.layout.progressbar_animation);
                isLoading = true;
            }
        } catch (Exception e) {
        }
    }
    public static void hideLoading() {
        if (mProgressDialog != null && isLoading) {
            mProgressDialog.hide();
            mProgressDialog.dismiss();
            isLoading = false;
        }
    }
}
