package com.mastercard.androidapp.presentation.profile;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public interface CreateProfile {

    interface View {
        void showProfileCreatedDialog();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void createProfile(Profile profile);
        void abort();
    }

}
