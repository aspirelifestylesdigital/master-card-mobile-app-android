package com.mastercard.androidapp.presentation.info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;

import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.datalayer.entity.GetClientCopyResult;
import com.mastercard.androidapp.datalayer.repository.B2CDataRepository;
import com.mastercard.androidapp.domain.usecases.GetMasterCardCopy;
import com.mastercard.androidapp.presentation.base.CommonActivity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.JustifiedTextView;

/**
 * Created by ThuNguyen on 6/19/2017.
 */

public class MasterCardUtilityActivity extends CommonActivity implements MasterCardUtility.View {

    MasterCardUtilityPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isSwipeBack = false;
        setContentView(R.layout.activity_mastercard_copy_utility);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        // Get mastercard utility type
        AppConstant.MASTERCARD_COPY_UTILITY type = (AppConstant.MASTERCARD_COPY_UTILITY) getIntent().getSerializableExtra(IntentConstant.MASTERCARD_COPY_UTILITY);
        switch (type){
            case TermsOfUse:
                title.setText(R.string.menu_term_of_use);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.TERMS_OF_USE.getValue());
                }
                break;
            case Privacy:
                title.setText(R.string.menu_privacy_policy);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.PRIVACY_POLICY.getValue());
                }
                break;
            case About:
                title.setText(R.string.menu_about);
                if(savedInstanceState == null){
                    // Track GA
                    App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.ABOUT_THIS_APP.getValue());
                }
                break;
        }
        if(savedInstanceState == null) {
            MasterCardUtilityFragment masterCardUtilityFragment = MasterCardUtilityFragment
                    .newInstance(type);
            masterCardUtilityFragment.setJustifyTextViewListener(justifyTextViewListener);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.mastercard_copy_utility_place_holder
                            , masterCardUtilityFragment
                            , MasterCardUtilityFragment.class.getSimpleName())
                    .addToBackStack(MasterCardUtilityFragment.class.getSimpleName())
                    .commit();
        }
        presenter = new MasterCardUtilityPresenter(new GetMasterCardCopy(new B2CDataRepository()));
        presenter.attach(this);
        presenter.getMasterCardCopy(type.getValue());

    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onGetMasterCardCopy(GetClientCopyResult result) {
//        ((MasterCardUtilityFragment)getSupportFragmentManager()
//                .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()))
//                .renderContent(result.getText());
        ((MasterCardUtilityFragment)getSupportFragmentManager()
                .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()))
                .renderContent(result.getText());
    }

    @Override
    public void onUpdateFailed() {
        if(!isDestroyed()) {
            ((MasterCardUtilityFragment) getSupportFragmentManager()
                    .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()))
                    .renderContent("");
            if (App.getInstance().hasNetworkConnection()) {
                new DialogHelper(this).showTimeoutError();
            }else{
                new DialogHelper(this).showNoInternetAlert();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detach();
    }
    JustifiedTextView.IJustifyTextViewListener justifyTextViewListener = new JustifiedTextView.IJustifyTextViewListener() {
        @Override
        public void onContentScroll(boolean hitToBottom) {
        }

        @Override
        public void onHyperLinkClicked(String url) {
            new DialogHelper(MasterCardUtilityActivity.this).showLeavingAlert(url);
        }
    };
}
