package com.mastercard.androidapp.domain.usecases;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.utils.CommonUtils;
import com.api.aspire.domain.repository.AuthAspireRepository;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.MapProfileBase;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public class CheckBIN extends UseCase<Boolean, String> {

    private AuthAspireRepository verifyRepository;
    private GetToken getToken;
    private MapProfileBase mapLogic;

    public CheckBIN(AuthAspireRepository verifyRepository,
                    MapProfileBase mapLogic,
                    GetToken getToken) {
        this.verifyRepository = verifyRepository;
        this.mapLogic = mapLogic;
        this.getToken = getToken;
    }

    @Override
    Observable<Boolean> buildUseCaseObservable(String s) {
        return null;
    }

    @Override
    public Single<Boolean> buildUseCaseSingle(String binCode) {
        final int passCodeNumber = CommonUtils.convertStringToInt(binCode);
        if (passCodeNumber == -1) {
            return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
        }
        return getToken.getTokenService()
                .flatMap(serviceToken ->
                        verifyRepository.checkPassCode(serviceToken,
                                mapLogic.getXAppId(),
                                mapLogic.getXOrganization(),
                                passCodeNumber)
                );
    }

    @Override
    Completable buildUseCaseCompletable(String s) {
        return null;
    }
}
