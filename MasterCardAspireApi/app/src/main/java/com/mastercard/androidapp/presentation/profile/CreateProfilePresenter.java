package com.mastercard.androidapp.presentation.profile;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.datasource.RemoteChangeRecoveryQuestionDataStore;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.ChangeSecurityQuestionDataRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.api.aspire.domain.usecases.CreateProfileCase;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.SignInCase;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.domain.mapper.profile.MapProfile;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Old flow don't update here
 */
public class CreateProfilePresenter implements CreateProfile.Presenter {

    private CompositeDisposable disposables;
    private CreateProfile.View view;
    private CreateProfileCase createProfileCase;

    private ProfilePMAMapView profilePMAMapView;
    private ChangeSecurityQuestion.Param securityQuestionParam;
    private PreferencesStorageAspire prefStorage;

    CreateProfilePresenter(Context c) {
        disposables = new CompositeDisposable();
        this.prefStorage = new PreferencesStorageAspire(c);
        MapProfileApp mapLogic = new MapProfileApp();

        RemoteUserProfileOKTADataStore remoteProfileOKTA = new RemoteUserProfileOKTADataStore();
        UserProfileOKTADataRepository userProfileOKTARepository = new UserProfileOKTADataRepository(remoteProfileOKTA);
        ChangeSecurityQuestion useCaseSecurityQuestion = new ChangeSecurityQuestion(userProfileOKTARepository,
                new ChangeSecurityQuestionDataRepository(new RemoteChangeRecoveryQuestionDataStore()),
                mapLogic
        );
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(prefStorage, mapLogic);
        GetToken getToken = new GetToken(prefStorage, mapLogic);

        SignInCase signInCase = new SignInCase(mapLogic,
                profileRepository,
                userProfileOKTARepository,getToken);

        this.createProfileCase = new CreateProfileCase(
                profileRepository,
                useCaseSecurityQuestion,
                signInCase,
                getToken
        );

    }

    @Override
    public void attach(CreateProfile.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        view = null;
    }

    public void setProfilePMAMapView(ProfilePMAMapView profilePMAMapView) {
        this.profilePMAMapView = profilePMAMapView;
    }

    public void setSecurityQuestionParam(ChangeSecurityQuestion.Param securityQuestionParam) {
        this.securityQuestionParam = securityQuestionParam;
    }

    @Override
    public void createProfile(Profile profile) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        disposables.add(
                caseCheckParamRequest(profile)
                        .flatMap(params -> createProfileCase.buildUseCaseSingle(params))
                        .flatMapCompletable(profileAspire -> Completable.create(e->{
                            //-- add binCode to preference
                            String binCodeValue = ProfileLogicCore.getAppUserPreference(
                                    profileAspire.getAppUserPreferences(),
                                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY
                            );
                            prefStorage.editor().binCode(binCodeValue).build().save();
                            e.onComplete();
                        }))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new SaveProfileObserver()));
    }

    private Single<CreateProfileCase.Params> caseCheckParamRequest(Profile profile) {
        return Single.create(e -> {
            if (securityQuestionParam == null) {
                e.onError(new Exception(ErrCode.UNKNOWN_ERROR.name()));
            }else{
                String partyId = (profilePMAMapView != null
                        && !TextUtils.isEmpty(profilePMAMapView.getPartyId()))
                        ? profilePMAMapView.getPartyId() : "";

                //-- old flow don't use
//                CreateProfileCase.Params params = new MapProfile().createProfile(profile,
//                        partyId,
//                        securityQuestionParam);
//                e.onSuccess(params);
            }
        });
    }

    @Override
    public void abort() {
        disposables.clear();
    }

    private final class SaveProfileObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            view.dismissProgressDialog();
            view.showProfileCreatedDialog();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
            dispose();
        }
    }

}
