package com.mastercard.androidapp.presentation.base;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.content.res.AppCompatResources;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.TypefaceSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.logic.Utils;
import com.mastercard.androidapp.presentation.widget.ViewUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Vinh.Trinh
 * This activity contain navigation drawer menu.
 */
public abstract class NavDrawerActivity extends BaseActivity {

    static {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
    }

    @BindView(R.id.view_stubs)
    protected CoordinatorLayout viewContainer;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    protected NavigationView navigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_nav_drawer);
        ButterKnife.bind(this);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, R.string.navigation_drawer_open
                , R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                supportInvalidateOptionsMenu();
                NavDrawerActivity.this.onDrawerClosed();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                supportInvalidateOptionsMenu();
                NavDrawerActivity.this.onDrawerOpened();
            }
        };
        View actionDrawerToggle = navigationView.getHeaderView(0).findViewById(R.id.action_drawer_toggle);
        ColorStateList csl = AppCompatResources.getColorStateList(this, R.color.ic_menu_tint_color);
        Drawable drawable = DrawableCompat.wrap(((ImageView)actionDrawerToggle).getDrawable());
        DrawableCompat.setTintList(drawable, csl);
        actionDrawerToggle.setBackground(drawable);
        actionDrawerToggle.setOnClickListener(view -> drawerLayout.closeDrawer(GravityCompat.START));

        View askConcierge = navigationView.getHeaderView(0).findViewById(R.id.action_ask_concierge);
        drawable = DrawableCompat.wrap(((ImageView)askConcierge).getDrawable());
        DrawableCompat.setTintList(drawable, csl);
        askConcierge.setBackground(drawable);
        askConcierge.setOnClickListener(view ->{
            navigateToAskConcierge();
            drawerLayout.closeDrawer(GravityCompat.START);
        });
        navigationView.setOnClickListener(view -> {});

//        navigationView.getHeaderView(0).setOnClickListener(v -> drawerLayout.closeDrawer(GravityCompat.START));
//        navigationView.getHeaderView(2).setOnClickListener(v -> navigateToAskConcierge());
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerToggle.syncState();
        appConfig();
    }

    public abstract void navigateToAskConcierge();

    private void appConfig() {
        // remove shadow
        drawerLayout.setScrimColor(Color.TRANSPARENT);
        // remove NavigationView elevation
        drawerLayout.setDrawerElevation(0);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu) {
        for(int index = 0; index < this.navigationView.getMenu().size(); index ++){
            setFontStyleForMenuItem(this.navigationView.getMenu().getItem(index));
        }
        return super.onCreatePanelMenu(featureId, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle your other action bar items...
        if (item.getItemId() == android.R.id.home) {
            if (drawerLayout.isDrawerOpen(navigationView)) drawerLayout.closeDrawer(GravityCompat.START);
            else drawerLayout.openDrawer(GravityCompat.START);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        if (viewContainer != null) {
            LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            inflater.inflate(layoutResID, viewContainer, true);
        }
    }

    @Override
    public void setContentView(View view) {
        if (viewContainer != null) {
            ViewGroup.LayoutParams lp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            setContentView(view, lp);
        }
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        if (viewContainer != null) {
            viewContainer.addView(view, params);
        }
    }

    protected void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        Drawable icMenu = ContextCompat.getDrawable(this, R.drawable.ic_drawer);
        ViewUtils.menuTintColors(this, icMenu);
//        icMenu.setColorFilter(new
//                PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN));
        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(icMenu);
        ab.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void setToolbarColor(int color) {
        getSupportActionBar().setBackgroundDrawable(
                new ColorDrawable(color)
        );
    }

    public void setStatusBarColor(int color) {
        if (Utils.isHigherThanLolipop()) {
            /*getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);*/
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }
    }

    public void setNavViewBackgroundColor(int color) {
        navigationView.setBackgroundColor(color);
    }

    protected void onDrawerOpened() {}
    protected void onDrawerClosed() {}

    private void setFontStyleForMenuItem(MenuItem menuItem){
        String itemTitle = menuItem.getTitle().toString();
        if(!TextUtils.isEmpty(itemTitle)){
            SpannableString span = new SpannableString(itemTitle);
            CustomTypefaceSpan typefaceSpan = new CustomTypefaceSpan("Arial", Typeface.createFromAsset(getAssets(), "mc_med.ttf"));
            AbsoluteSizeSpan absoluteSizeSpan = new AbsoluteSizeSpan(getResources().getDimensionPixelSize(R.dimen.font_size_xlarge));
            span.setSpan(typefaceSpan, 0, itemTitle.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            span.setSpan(absoluteSizeSpan, 0, itemTitle.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            menuItem.setTitle(span);
        }
    }
    @SuppressLint("ParcelCreator")
    public class CustomTypefaceSpan extends TypefaceSpan {
        private final Typeface newType;
        public CustomTypefaceSpan(String family, Typeface type) {
            super(family);
            newType = type;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            applyCustomTypeFace(ds, newType);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            applyCustomTypeFace(paint, newType);
        }

        private void applyCustomTypeFace(Paint paint, Typeface tf) {
            paint.setTypeface(tf);
        }
    }
}
