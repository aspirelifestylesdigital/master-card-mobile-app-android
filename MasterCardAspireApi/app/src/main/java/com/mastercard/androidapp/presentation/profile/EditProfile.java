package com.mastercard.androidapp.presentation.profile;

import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 5/11/2017.
 */

public interface EditProfile {

    interface View {
        void showProfile(Profile profile);

        void showErrorDialog(ErrCode errCode, String extraMsg);

        void showProgressDialog();

        void dismissProgressDialog();

        void profileUpdated();
    }

    interface Presenter extends BasePresenter<View> {
        void getProfile();

        void updateProfile(Profile profile);

        void abort();

        void getProfileLocal();

        void updateSecurity(String question, String answer);

        void updateProfile(Profile profile, String question, String answer);
    }
}
