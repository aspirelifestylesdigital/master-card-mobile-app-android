package com.mastercard.androidapp.presentation.profile;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.api.aspire.common.constant.ErrorApi;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.presentation.base.CommonActivity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;
import com.mastercard.androidapp.presentation.widget.ViewUtils;

public class MyProfileActivity extends CommonActivity implements ProfileFragment.ProfileEventsListener,
        EditProfile.View {

    private EditProfilePresenter presenter;
    private DialogHelper dialogHelper;
    private ProfileFragment myFragment;

    private EditProfilePresenter editProfilePresenter() {
        return new EditProfilePresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.acticity_dummy_content);
        setTitle("My Profile");
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder
                            , ProfileFragment.newInstance(ProfileFragment.PROFILE.EDIT)
                            , ProfileFragment.class.getSimpleName())
                    .commit();
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.MY_PROFILE.getValue());
        }
        dialogHelper = new DialogHelper(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        toolbar.setClickable(true);
        toolbar.setOnClickListener(view -> {
            if (getCurrentFocus() != null) {
                ViewUtils.hideSoftKey(getCurrentFocus());
            }
        });
        presenter = editProfilePresenter();
        presenter.attach(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void onFragmentCreated() {
        presenter.getProfileLocal();
        presenter.getProfile();
    }

    @Override
    public void onUpdateSecurityQuestion(String question, String answer) {
        presenter.updateSecurity(question, answer);
    }

    @Override
    public void onProfileSubmitAndUpdateSecurity(Profile profile, String question, String answer) {
        presenter.updateProfile(profile, question, answer);
    }

    @Override
    public void onProfileSubmit(Profile profile) {
        presenter.updateProfile(profile);
    }

    @Override
    public void onProfileCancel() {
        presenter.getProfileLocal();
    }

    @Override
    public void showProfile(Profile profile) {
        myFragment = (ProfileFragment) getSupportFragmentManager()
                .findFragmentByTag(ProfileFragment.class.getSimpleName());
        if (myFragment != null) {
            myFragment.loadProfile(profile);
        }
    }

    @Override
    public void profileUpdated() {
        dialogHelper.alert(null, getString(R.string.profile_updated_message));
        if (myFragment != null) {
            myFragment.reloadProfileAfterUpdated();
        }
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (ErrorApi.isGetTokenError(extraMsg)) {
            dialogHelper.showGetTokenError();
        } else if (extraMsg.equalsIgnoreCase(ErrCode.CHANGE_SECURITY_QUESTION_ERROR.name())) {
            dialogHelper.showGeneralError();
        } else if (App.getInstance().hasNetworkConnection()) {
            dialogHelper.alert("ERROR!", getString(R.string.upsert_profile_error));
        } else {
            dialogHelper.showNoInternetAlert();
        }
    }

    @Override
    public void showProgressDialog() {
        //dialogHelper.showProgress(getString(R.string.dialog_save_processing),false, dialog -> presenter.abort());
        ProgressDialogUtil.showLoading(this);
    }

    @Override
    public void dismissProgressDialog() {
        //dialogHelper.dismissProgress();
        ProgressDialogUtil.hideLoading();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            if (getCurrentFocus() != null) {
                ViewUtils.hideSoftKey(getCurrentFocus());
            }
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            if (myFragment != null) {
                myFragment.getView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        myFragment.invokeSoftKey();
                    }
                }, 100);

            }
        });
    }

}
