package com.mastercard.androidapp.presentation.cityguidecategory;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.common.constant.ResultCode;
import com.mastercard.androidapp.domain.model.SubCategoryItem;
import com.mastercard.androidapp.presentation.base.CommonActivity;
import com.mastercard.androidapp.presentation.widget.ListItemDecoration;
import com.support.mylibrary.common.Utils;

import java.util.List;

import butterknife.BindView;

import static com.mastercard.androidapp.common.constant.IntentConstant.CATEGORY_ID;

/**
 * Created by vinh.trinh on 5/10/2017.
 */

public class CityGuideCategoriesActivity extends CommonActivity implements
        SubCategoryAdapter.OnCategoryItemClickListener, SubCategory.View {

    @BindView(R.id.selection_recycle_view)
    RecyclerView subCategoryRView;
    @BindView(R.id.loading)
    ProgressBar loading;
    private SubCategoryAdapter subCategoryAdapter;
    private SubCategoryPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.common_recyclerview_activity_layout);
        setTitle(R.string.city_guide_category_title);
        presenter = new SubCategoryPresenter();
        presenter.attach(this);
        int padding = Utils.dip2px(this, 20);
        subCategoryRView.setPadding(padding, padding, padding, 0);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        subCategoryRView.setLayoutManager(layoutManager);

        subCategoryRView.setPadding(0,
                0,
                0,
                getResources().getDimensionPixelSize(R.dimen.margin_smallest));

        ListItemDecoration dividerItemDecoration = new ListItemDecoration(subCategoryRView.getContext());
        subCategoryRView.addItemDecoration(dividerItemDecoration);
        dividerItemDecoration.setAdapterItemCount(0);

        /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(subCategoryRView.getContext(),
                DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_drawable));
        subCategoryRView.addItemDecoration(dividerItemDecoration);*/
        initIntentExtra();
    }

    private void initIntentExtra() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null && bundle.getInt(CATEGORY_ID) != 0) {
            presenter.getSubCategories(bundle.getInt(CATEGORY_ID));
        }
    }

    @Override
    public void onItemClick(int index) {
        SubCategoryItem item = subCategoryAdapter.getItem(index);
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.INDEX_CITY_GUIDE_CATEGORY_SELECT, index);
        intent.putExtra(IntentConstant.SUB_CATEGORY_NAME, item.getSubCategoryName());
        intent.putExtra(IntentConstant.SUB_CATEGORY_IMAGE, item.getImageResources());
        setResult(ResultCode.RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void updateSubCategoryRViewAdapter(List<SubCategoryItem> subCategoryItems) {
        if (subCategoryAdapter == null) {
            subCategoryAdapter = new SubCategoryAdapter(this, subCategoryItems);
            subCategoryRView.setAdapter(subCategoryAdapter);
            subCategoryAdapter.setListener(this);
        } else {
            subCategoryAdapter.swapData(subCategoryItems);
        }
    }

    @Override
    public void showLoading() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loading.setVisibility(View.GONE);
    }
}
