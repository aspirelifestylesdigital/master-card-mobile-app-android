package com.mastercard.androidapp.datalayer.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class Member {
    @Expose
    @SerializedName("ConsumerKey")
    private String consumerKey;
    @Expose
    @SerializedName("Functionality")
    private String functionality;
    @Expose
    @SerializedName("Email")
    private String email;
    @Expose
    @SerializedName("FirstName")
    private String firstName;
    @Expose
    @SerializedName("LastName")
    private String lastName;
    @Expose
    @SerializedName("MobileNumber")
    private String phoneNumber;
    @Expose
    @SerializedName("ZipCode")
    private String zipCode;
    @Expose
    @SerializedName("Program")
    private String program;
    @Expose
    @SerializedName("DeviceOS")
    private String deviceOS;
    @Expose
    @SerializedName("Salutation")
    private String salutation;
    @Expose
    @SerializedName("MemberDeviceID")
    private String memberDeviceID;
    @Expose
    @SerializedName("onlineMemberID")
    private String onlineMemberID;

    public Member(String functionality, String email, String firstName, String lastName,
                  String phoneNumber, String zipCode, String memberDeviceID) {
        this.consumerKey = "MCD";
        this.functionality = functionality;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.zipCode = zipCode;
        this.program = "MCD VASP";
        this.salutation = "";
        this.deviceOS = "Android";
        this.memberDeviceID = memberDeviceID;
    }

    public Member(String functionality, String email, String firstName, String lastName, String phoneNumber,
                  String zipCode, String memberDeviceID, String onlineMemberID) {
        this.consumerKey = "MCD";
        this.functionality = functionality;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.zipCode = zipCode;
        this.program = "MCD VASP";
        this.salutation = "";
        this.deviceOS = "Android";
        this.memberDeviceID = memberDeviceID;
        this.onlineMemberID = onlineMemberID;
    }
}