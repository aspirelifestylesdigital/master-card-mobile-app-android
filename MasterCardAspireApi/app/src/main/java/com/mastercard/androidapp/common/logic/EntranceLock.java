package com.mastercard.androidapp.common.logic;

/**
 * Created by ThuNguyen on 11/2/2015.
 * This class is to prevent many events click continuously
 */
public class EntranceLock {
    private long mTimeForLastClick = 0;

    public boolean isClickContinuous(){
        if(mTimeForLastClick == 0){
            mTimeForLastClick = System.currentTimeMillis();
            return false;
        }
        long currentTime = System.currentTimeMillis();
        long delta = Math.abs(currentTime - mTimeForLastClick);
        mTimeForLastClick = currentTime;
        return (delta <= 500);
    }
}
