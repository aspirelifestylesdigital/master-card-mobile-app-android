package com.mastercard.androidapp.presentation.checkout;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.presentation.base.BaseFragment;
import com.mastercard.androidapp.presentation.info.MasterCardUtilityFragment;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.JustifiedTextView;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

//import com.support.mylibrary.widget.ClickGuard;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class PrivacyPolicyFragment extends BaseFragment {
    public static final String VERSION_MAJOR_CHANGE = "version_major_change";

    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.checkbox_privacy)
    AppCompatCheckBox checkBox;
    private PrivacyPolicyListener listener;
    private boolean contentLoaded = false;

    public static PrivacyPolicyFragment newInstance(boolean isMajorChange) {

        Bundle args = new Bundle();
        args.putBoolean(VERSION_MAJOR_CHANGE, isMajorChange);
        PrivacyPolicyFragment fragment = new PrivacyPolicyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PrivacyPolicyListener) {
            listener = (PrivacyPolicyListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement PrivacyPolicyListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_privacy_policy, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MasterCardUtilityFragment masterCardUtilityFragment = MasterCardUtilityFragment
                .newInstance(AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
        masterCardUtilityFragment.setJustifyTextViewListener(justifyTextViewListener);
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_privacy_place_holder
                        , masterCardUtilityFragment
                        , MasterCardUtilityFragment.class.getSimpleName())
                .commit();
        //ClickGuard.guard(btnSubmit);
        listener.onPrivacyEnter();

        if(getArguments() != null && getArguments().getBoolean(VERSION_MAJOR_CHANGE, false)){
            // Show dialog major change
            new DialogHelper(getActivity()).alert("", getString(R.string.privacy_major_change));
        }
    }

    @OnCheckedChanged(R.id.checkbox_privacy)
    public void checkboxToggle(boolean checked) {
        btnSubmit.setEnabled(contentLoaded && checked);
    }

    @OnClick(R.id.btn_submit)
    public void submit(View view) {
        listener.onPrivacySubmit();
    }

    public void contentLoaded() {
        contentLoaded = true;
        btnSubmit.setEnabled(checkBox.isChecked());
    }

    JustifiedTextView.IJustifyTextViewListener justifyTextViewListener = new JustifiedTextView.IJustifyTextViewListener() {
        @Override
        public void onContentScroll(boolean hitToBottom) {
            if(hitToBottom){
                checkBox.setTextColor(Color.WHITE);
                checkBox.setEnabled(true);
            }
        }

        @Override
        public void onHyperLinkClicked(String url) {
            new DialogHelper(getActivity()).showLeavingAlert(url);
        }
    };
    public interface PrivacyPolicyListener {
        void onPrivacyEnter();
        void onPrivacySubmit();
    }
}
