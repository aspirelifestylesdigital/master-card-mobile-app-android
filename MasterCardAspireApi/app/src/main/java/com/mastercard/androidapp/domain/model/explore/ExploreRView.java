package com.mastercard.androidapp.domain.model.explore;

import android.os.Parcelable;

/**
 * Created by vinh.trinh on 6/8/2017.
 */

public abstract class ExploreRView implements Parcelable {

    protected ItemType itemType;

    public enum ItemType {
        NORMAL,
        DINING,
        CITY_GUIDE,
        SEARCH
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public ItemType getItemType() {
        return this.itemType;
    }

    public abstract String getTitle();
    public abstract String getDescription();
    public abstract String getImageUrl();
    public abstract Integer getId();
}
