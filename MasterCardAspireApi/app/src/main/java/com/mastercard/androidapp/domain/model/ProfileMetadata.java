package com.mastercard.androidapp.domain.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by vinh.trinh on 6/19/2017.
 */

public class ProfileMetadata implements Parcelable {

    public final String cardType;
    public final String cardDate;
    public final String policyDate;
    public final String policyVersion;
    String optStatus;
    private String optStatusDate;
    public final String validBinNumber;
    public final String binEndDate;


    public ProfileMetadata(String cardType, String cardDate, String policyDate, String policyVersion,
                           String optStatus, String optStatusDate, String validBinNumber, String binEndDate) {
        this.cardType = cardType;
        this.cardDate = cardDate;
        this.policyDate = policyDate;
        this.policyVersion = policyVersion;
        this.optStatus = optStatus;
        this.optStatusDate = optStatusDate;
        this.validBinNumber = validBinNumber;
        this.binEndDate = binEndDate;
    }

    public ProfileMetadata(String jsonString) throws JSONException {
        JSONObject json = new JSONObject(jsonString);
        // TODO no null field should be accepted here, but until API has full support
        cardType = json.has("cardType") ? json.getString("cardType") : null;
        cardDate = json.has("cardDate") ? json.getString("cardDate") : null;
        optStatus = json.has("optStatus") ? json.getString("optStatus") : null;
        optStatusDate = json.has("optStatusDate") ? json.getString("optStatusDate") : null;
        policyDate = json.has("policyDate") ? json.getString("policyDate") : null;
        policyVersion = json.getString("policyVersion");
        validBinNumber = json.getString("validBinNumber");
        binEndDate = json.has("binEndDate") ? json.getString("binEndDate") : null;
    }

    public JSONObject toJSON() throws JSONException {
        JSONObject json = new JSONObject();
        if (cardType != null) json.put("cardType", cardType);
        if (cardDate != null) json.put("cardDate", cardDate);
        if (optStatus != null) json.put("optStatus", optStatus);
        if (optStatusDate != null) json.put("optStatusDate", optStatusDate);
        if (policyDate != null) json.put("policyDate", policyDate);
        json.put("policyVersion", policyVersion);
        json.put("validBinNumber", validBinNumber);
        if (binEndDate != null) json.put("binEndDate", binEndDate);
        return json;
    }

    private ProfileMetadata(Parcel in) {
        cardType = in.readString();
        cardDate = in.readString();
        policyDate = in.readString();
        policyVersion = in.readString();
        optStatus = in.readString();
        optStatusDate = in.readString();
        validBinNumber = in.readString();
        binEndDate = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(cardType);
        dest.writeString(cardDate);
        dest.writeString(policyDate);
        dest.writeString(policyVersion);
        dest.writeString(optStatus);
        dest.writeString(optStatusDate);
        dest.writeString(validBinNumber);
        dest.writeString(binEndDate);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileMetadata> CREATOR = new Creator<ProfileMetadata>() {
        @Override
        public ProfileMetadata createFromParcel(Parcel in) {
            return new ProfileMetadata(in);
        }

        @Override
        public ProfileMetadata[] newArray(int size) {
            return new ProfileMetadata[size];
        }
    };

    void setOptStatus(String optStatus) {
        this.optStatus = optStatus;
    }

    void setOptStatusDate(String optStatusDate) {
        this.optStatusDate = optStatusDate;
    }

    public String getOptStatus() {
        return optStatus;
    }

    public String getOptStatusDate() {
        return optStatusDate;
    }
}
