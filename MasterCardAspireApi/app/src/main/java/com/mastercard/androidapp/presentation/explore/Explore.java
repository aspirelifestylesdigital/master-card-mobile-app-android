package com.mastercard.androidapp.presentation.explore;

import com.mastercard.androidapp.domain.model.explore.ExploreRView;
import com.mastercard.androidapp.domain.model.explore.ExploreRViewItem;
import com.mastercard.androidapp.presentation.base.BasePresenter;

import java.util.List;


/**
 * Created by tung.phan on 5/8/2017.
 */

public interface Explore {
    interface View {
        void showLoading(boolean more);
        void hideLoading(boolean more);
        void updateRecommendAdapter(List<ExploreRViewItem> datum);
        List<ExploreRViewItem> historyData();
        void onUpdateFailed(String message);
        void emptyData(boolean cityNotInCityGuide);
        void onSearchNoData();
        void clearSearch(boolean repeat);
    }

    interface Presenter extends BasePresenter<Explore.View> {
        void getAccommodationData();
        void handleCitySelection();
        void handleCategorySelection(String category);
        void cityGuideCategorySelection(int index);
        void searchByTerm(String term, boolean withOffers);
        void clearSearch();
        void offHasOffers();
        void loadMore();
        ExploreRView detailItem(ExploreRView.ItemType type, int index);
    }
}
