package com.mastercard.androidapp.presentation.profile.signUpV2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.api.aspire.domain.usecases.ChangeSecurityQuestion;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.domain.model.ProfileMetadata;
import com.mastercard.androidapp.presentation.base.BaseSwipeBackActivity;
import com.mastercard.androidapp.presentation.checkout.signInV2.SignInActivity;
import com.mastercard.androidapp.presentation.home.HomeActivity;
import com.mastercard.androidapp.presentation.profile.MyProfileActivity;
import com.mastercard.androidapp.presentation.profile.forgotPwdV2.ForgotPasswordV2Activity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;
import com.mastercard.androidapp.presentation.widget.ViewUtils;

import org.json.JSONException;

import butterknife.BindView;

public class SignUpV2Activity extends BaseSwipeBackActivity
        implements CreateProfileFragment.ProfileEventsListener,
        MatchUpFragment.MatchUpEventsListener, SignUpV2.View, PasswordFragment.PasswordEventsListener {
    private SignUpV2Presenter presenter;
    private DialogHelper dialogHelper;
    private ProfilePMAMapView profilePMAMapView;

    @BindView(R.id.fragment_place_holder)
    FrameLayout fragmentHolder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_sign_up_v2);
        presenter = new SignUpV2Presenter(this);
        presenter.attach(this);
        dialogHelper = new DialogHelper(this);
        if (savedInstanceState == null) {
            loadFirstFragment(MatchUpFragment.newInstance());
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_UP.getValue());
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public void onProfileSubmit(Profile profile, ChangeSecurityQuestion.Param param) {
        ProfileMetadata profileMetadata = null;
        try {
            profileMetadata = new ProfileMetadata(
                    getIntent().getStringExtra(Intent.EXTRA_TEXT));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (profileMetadata != null) {
            profile.setMetadata(profileMetadata);
            profile.updateMetadata();
            profile.setPinCode(profileMetadata.validBinNumber);
        }

        presenter.setProfilePMAMapView(profilePMAMapView);
        presenter.setSecurityQuestionParam(param);

        presenter.createProfile(profile);
    }

    @Override
    public void showProfileCreatedDialog() {
        dialogHelper.createBuilderWithLayout(0, R.string.profile_created_message
                , R.string.back_to_profile, R.string.home, R.layout.custom_progress_home_dialog)
                .onPositive((dialog, which) -> {
                    backToProfile();
                    // Track GA with "Sign up" event
                    App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                            AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                            AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
                })
                .onNegative((dialog, which) -> {
                    proceedToHome();
                    // Track GA with "Sign up" event
                    App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                            AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                            AppConstant.GA_TRACKING_LABEL.SIGN_UP.getValue());
                })
                .build().show();
    }


    private void proceedToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void backToProfile() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);

        Intent mainIntent = new Intent(this, MyProfileActivity.class);
        startActivity(mainIntent);
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.isNetworkUnEnable(errCode, extraMsg)) {
            dialogHelper.showNoInternetAlert();
        } else if (ErrorApi.isGetTokenError(extraMsg)) {
            dialogHelper.showGetTokenError();
        } else if (ErrCode.USER_EXISTS_ERROR.name().equalsIgnoreCase(extraMsg)) {
            dialogHelper.alert(App.getInstance().getString(R.string.errorTitle), App.getInstance().getString(R.string.errorExistEmail));
        } else if (extraMsg.equalsIgnoreCase(ErrCode.CREATE_ACCOUNT_ASPIRE_PARAM_ERROR.name())) {
            dialogHelper.alert(getString(R.string.invalid_create_pwd_title),
                    getString(R.string.invalid_create_pwd));
        } else {
            dialogHelper.showGeneralError();
        }
    }

    @Override
    public void showProgressDialog() {
        ProgressDialogUtil.showLoading(this);
    }

    @Override
    public void dismissProgressDialog() {
        ProgressDialogUtil.hideLoading();
    }

    @Override
    public void showMatchUpDialog() {
        dialogHelper.action("", getString(R.string.match_up_found_email),
                getString(R.string.lb_sign_in), getString(R.string.forgot_password_screen),
                (dialogInterface, i) -> {
                    Intent intent = new Intent(SignUpV2Activity.this, SignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                },
                (dialogInterface, i) -> {
                    Intent intent = new Intent(this, SignInActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);

                    Intent forgotIntent = new Intent(SignUpV2Activity.this, ForgotPasswordV2Activity.class);
                    startActivity(forgotIntent);
                });
    }

    @Override
    public void showExistPMAAccountDialog(ProfilePMAMapView profilePMAMapView) {
        dialogHelper.alert("", getString(R.string.match_up_found_email), dialogInterface -> {
            dialogInterface.dismiss();
            onAddFragment(CreateProfileFragment.newInstance(profilePMAMapView));
        });
    }

    @Override
    public void showCreateNewAccountDialog(String email) {
        dialogHelper.alert("", getString(R.string.match_up_create_new_account), dialogInterface -> {
            dialogInterface.dismiss();
            ProfilePMAMapView profilePMAMapView = ProfilePMAMapView.empty();
            profilePMAMapView.setEmail(email);
            onAddFragment(CreateProfileFragment.newInstance(profilePMAMapView));
        });
    }


    @Override
    public void onProfileInputError(String message) {
        dialogHelper.profileDialog(message, dialog -> {
            //get current Create fragment show error and show keyboard
            if(getSupportFragmentManager() == null){
                return;
            }
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_place_holder);
            if (fragment instanceof CreateProfileFragment) {
                final CreateProfileFragment createFragment = (CreateProfileFragment) fragment;
                if(createFragment.getView() != null){
                    createFragment.getView().postDelayed(createFragment::showSoftKey, 100);
                }
            }
        });
    }

    @Override
    public void onProfilePMACreate(ProfilePMAMapView profilePMAMapView, Profile profile) {
        this.profilePMAMapView = profilePMAMapView;
        onAddFragment(PasswordFragment.newInstance(profile));
    }


    @Override
    public void onMatchUpSubmit(String email) {
        presenter.matchUpProfile(email);
    }

    @Override
    public int getFrameLayoutId() {
        return R.id.fragment_place_holder;
    }

    @Override
    public void onBackPressed() {
        ViewUtils.hideSoftKey(this.getCurrentFocus());
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            super.onBackPressed();
            //additional code
        } else {
            getSupportFragmentManager().popBackStack();
        }

    }
}