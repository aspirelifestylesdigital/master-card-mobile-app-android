package com.mastercard.androidapp.presentation.info;

import com.mastercard.androidapp.datalayer.entity.GetClientCopyResult;
import com.mastercard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 6/13/2017.
 */

public interface MasterCardUtility {

    interface View {
        void onGetMasterCardCopy(GetClientCopyResult result);
        void onUpdateFailed();
    }

    interface Presenter extends BasePresenter<View> {
        void getMasterCardCopy(String type);
    }

}
