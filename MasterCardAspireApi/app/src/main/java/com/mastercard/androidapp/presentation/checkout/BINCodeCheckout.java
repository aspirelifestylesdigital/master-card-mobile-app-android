package com.mastercard.androidapp.presentation.checkout;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.domain.model.ProfileAspire;
import com.mastercard.androidapp.domain.model.ProfileMetadata;
import com.mastercard.androidapp.presentation.base.BasePresenter;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public interface BINCodeCheckout {

    interface View {
        void showInvalidBINCodeDialog(boolean profileCreated, boolean binExpired);
        void privacyContentLoaded(String content);
        void proceedToSignUp(ProfileMetadata profileMetadata);
        void proceedToHome();
        void showErrorDialog(ErrCode errCode, String extraMsg);
        void showProgressDialog();
        void dismissProgressDialog();
    }

    interface Presenter extends BasePresenter<View> {
        void checkoutBINCode(String input);
        void checkoutBINCode(String input, ProfileAspire profileAspire);
        void loadPrivacyContent();
        void onPolicyAccepted();
        void onPolicyAcceptedFromSignIn(ProfileAspire profileAspire);
        void abort();
    }

}
