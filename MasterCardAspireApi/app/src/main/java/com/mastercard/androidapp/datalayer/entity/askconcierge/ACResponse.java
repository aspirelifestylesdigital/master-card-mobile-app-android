package com.mastercard.androidapp.datalayer.entity.askconcierge;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class ACResponse {
    @SerializedName("transactionId")
    String transactionId;
    @SerializedName("status")
    Status status;

    public String getTransactionId() {
        return transactionId;
    }

    public Status getStatus() {
        return status;
    }

    public class Status {
        @SerializedName("success")
        boolean success;
        @SerializedName("message")
        String message;

        public boolean isSuccess() {
            return success;
        }

        public String getMessage() {
            return message;
        }
    }

    public static ACResponse empty() {
        return new ACResponse();
    }
}