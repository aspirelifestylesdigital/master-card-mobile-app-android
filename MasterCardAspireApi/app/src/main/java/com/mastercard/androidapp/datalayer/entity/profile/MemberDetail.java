package com.mastercard.androidapp.datalayer.entity.profile;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vinh.trinh on 6/22/2017.
 */

public class MemberDetail {

    @SerializedName("CARDTYPE")
    private String cardType;
    @SerializedName("CARDNUMBER")
    private String cardNumber;
    @SerializedName("CARDDATE")
    private String cardDate;
    @SerializedName("POLICYDATE")
    private String policyDate;
    @SerializedName("POLICYVERSION")
    private String policyVersion;
    @SerializedName("OPTSTATUS")
    private String optStatus;
    @SerializedName("OPTDATE")
    private String optDate;
    @SerializedName("VALIDBINNUMBER")
    private String validBinNumber;
    @SerializedName("BINENDDATE")
    private String binEndDate;

    public String cardType() {
        return cardType;
    }

    public String cardDate() {
        return cardDate;
    }

    public String policyDate() {
        return policyDate;
    }

    public String policyVersion() {
        return policyVersion;
    }

    public String optStatus() {
        return optStatus;
    }

    public String optStatusDate() {
        return optDate;
    }

    public String validBinNumber() {
        return validBinNumber;
    }

    public String binEndDate() {
        return binEndDate;
    }
}
