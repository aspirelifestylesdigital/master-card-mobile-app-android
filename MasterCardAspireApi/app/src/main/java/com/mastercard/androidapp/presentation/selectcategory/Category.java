package com.mastercard.androidapp.presentation.selectcategory;

import com.mastercard.androidapp.presentation.base.BasePresenter;


/**
 * Created by tung.phan on 5/31/2017.
 */

public interface Category {

    interface View {
    }

    interface Presenter extends BasePresenter<Category.View> {

        void getCategories();
    }
}
