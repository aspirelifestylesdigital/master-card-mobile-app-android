package com.mastercard.androidapp.domain.repository;

import com.mastercard.androidapp.datalayer.entity.ContentFulls;
import com.mastercard.androidapp.datalayer.entity.GetClientCopyResult;
import com.mastercard.androidapp.datalayer.entity.QuestionAndAnswers;
import com.mastercard.androidapp.datalayer.entity.QuestionsAndAnswer;
import com.mastercard.androidapp.datalayer.entity.SearchContent;
import com.mastercard.androidapp.datalayer.entity.Tiles;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by tung.phan on 5/31/2017.
 */

public interface B2CRepository {

    Single<List<QuestionsAndAnswer>> getCityGuides(Integer categoryId, Integer paging);

    Single<List<QuestionsAndAnswer>> getDiningList(Integer condition, Integer paging);

    Single<QuestionAndAnswers> getQandADetail(Integer categoryId, Integer questionId);

    Single<List<Tiles>> getTilesList(String... categories);

    Single<ContentFulls> getContentFull(Integer contentId);

    Single<GetClientCopyResult> getMasterCardCopy(String type);

    Single<List<Tiles>> getGalleries();

    Single<List<SearchContent>> searchByTerm(String term, Integer paging, String...cities);

}
