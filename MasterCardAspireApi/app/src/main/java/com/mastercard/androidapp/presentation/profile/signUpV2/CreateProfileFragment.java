package com.mastercard.androidapp.presentation.profile.signUpV2;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.api.aspire.data.entity.userprofile.pma.ProfilePMAMapView;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.logic.MaskerHelper;
import com.mastercard.androidapp.common.logic.Validator;
import com.mastercard.androidapp.domain.mapper.profile.MapProfile;
import com.mastercard.androidapp.domain.model.Profile;
import com.mastercard.androidapp.presentation.base.BaseSwipeBackFragment;
import com.mastercard.androidapp.presentation.profile.CopyPasteHandler;
import com.mastercard.androidapp.presentation.profile.ProfileComparator;
import com.mastercard.androidapp.presentation.widget.TextPattern;
import com.mastercard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 4/27/2017.
 */

public class CreateProfileFragment extends BaseSwipeBackFragment {

    private static final String ARG_PROFILE_PMA = "profile_pma";
    private final String STATE_INPUTS = "input";
    @BindView(R.id.profile_parent_view)
    View profileParentView;
    @BindView(R.id.profile_rootview)
    RelativeLayout rlProfileView;
    @BindView(R.id.scroll_view)
    ScrollView mainScrollView;
    @BindView(R.id.fakeViewToMoveButtonDown)
    View fakeViewToMoveButtonDown;
    @BindView(R.id.contain_view)
    View containView;
    @BindView(R.id.checkBox_profile)
    AppCompatCheckBox checkBox;
    @BindView(R.id.tv_checkbox)
    TextView tvCheckbox;
    @BindView(R.id.edt_first_name)
    ErrorIndicatorEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    ErrorIndicatorEditText edtLastName;
    @BindView(R.id.flEmailMask)
    View flEmailMask;
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_phone)
    ErrorIndicatorEditText edtPhone;
    @BindView(R.id.edt_zip_code)
    ErrorIndicatorEditText edtZipCode;
    @BindView(R.id.btn_submit)
    AppCompatButton btnSubmit;
    @BindView(R.id.content_wrapper)
    LinearLayout contentWrapper;
    @BindView(R.id.content_submit)
    LinearLayout contentSubmit;
    //    boolean initial = false;
//    boolean firstNameMask;
    private ProfileEventsListener listener;
    private Profile profile = new Profile();
    private ProfileComparator profileComparator;
    private Validator validator = new Validator();
    private FocusChangeListener firstNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener lastNameMasker = new FocusChangeListener(MaskerHelper.INPUT.NAME);
    private FocusChangeListener emailMasker = new FocusChangeListener(MaskerHelper.INPUT.EMAIL);
    private FocusChangeListener phoneMasker = new FocusChangeListener(MaskerHelper.INPUT.PHONE);
    private FocusChangeListener zipCodeMasker = new FocusChangeListener(MaskerHelper.INPUT.FREE);
    private TextPattern phonePattern;
    private AppCompatEditText editText;
    private String errMessage;
    private int fieldFocus;
    private Rect contentWrapperViewRect;
    private boolean isKeyboardShow;
    private boolean isEdtMode;
    private boolean profileFromUpdate = true;
    private ProfilePMAMapView profilePMAMapView;

    public static CreateProfileFragment newInstance(ProfilePMAMapView profilePMAMapView) {
        Bundle args = new Bundle();
        args.putParcelable(ARG_PROFILE_PMA, profilePMAMapView);
        CreateProfileFragment fragment = new CreateProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ProfileEventsListener) {
            listener = (ProfileEventsListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement ProfileEventsListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_profile, container, false);
        return attachToSwipeBack(view);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(R.string.register).toUpperCase());
        profilePMAMapView = getArguments().getParcelable(ARG_PROFILE_PMA);

        edtEmail.setOnFocusChangeListener(emailMasker);
        tvCheckbox.setText(R.string.profile_checkbox_text_create);

        edtFirstName.setOnFocusChangeListener(firstNameMasker);
        edtLastName.setOnFocusChangeListener(lastNameMasker);
        edtPhone.setOnFocusChangeListener(phoneMasker);
        edtZipCode.setOnFocusChangeListener(zipCodeMasker);
        initTextInteractListener();
        phonePattern = new TextPattern(edtPhone, "#-###-###-####", 2);
        edtZipCode.addTextChangedListener(new TextPattern(edtZipCode, "#####-####"));
        edtPhone.addTextChangedListener(phonePattern);

        // Make secure for Email and Phone
        ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

            public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode actionMode, MenuItem item) {
                return false;
            }

            public void onDestroyActionMode(ActionMode actionMode) {
            }
        };

        edtPhone.setCustomSelectionActionModeCallback(actionModeCallback);
        edtEmail.setCustomSelectionActionModeCallback(actionModeCallback);

        // Click guard
        ClickGuard.guard(btnSubmit);

        if (profilePMAMapView != null) {
            profile.setEmail(profilePMAMapView.getEmail());
            edtEmail.setText(MaskerHelper.mask(
                    MaskerHelper.INPUT.EMAIL,
                    profilePMAMapView.getEmail()));
        }

        edtEmail.setEnabled(false);
        edtEmail.setClickable(false);
        edtEmail.setBackgroundResource(R.drawable.disable_underline_indicator);

        loadProfile();

    }

    @OnClick({R.id.profile_rootview, R.id.content_wrapper, R.id.content_submit, R.id.flEmailMask})
    public void onClick(View view) {
        hideSoftKey();
        if (view.getId() == R.id.flEmailMask) {
            edtEmail.setText(emailMasker.original);
        }
    }

    private void initTextInteractListener() {
        CopyPasteHandler copyPasteHandler = new CopyPasteHandler(validator);
        //everytime user paste text to edittext, onTextPasted get called.
        edtFirstName.setTextInteractListener(copyPasteHandler);
        edtLastName.setTextInteractListener(copyPasteHandler);
        edtPhone.setTextInteractListener(copyPasteHandler);
        edtZipCode.setTextInteractListener(copyPasteHandler);

        rlProfileView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                if (rlProfileView == null) return;
                rlProfileView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rlProfileView.getRootView().getHeight();

                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    if (!isKeyboardShow) {
                        isKeyboardShow = true;
                        // keyboard is opened
                        mainScrollView.smoothScrollTo(0, (int) edtFirstName.getTop() + contentWrapper.getTop());
                        scaleView();
                        processAfterKeyboardToggle(true);
                    }
                } else {
                    // keyboard is closed
                    if (isKeyboardShow) {
                        isKeyboardShow = false;
                        fakeViewToMoveButtonDown.setVisibility(View.VISIBLE);
                        processAfterKeyboardToggle(false);
                    }
                }
            }
        });
    }

    @OnClick(R.id.btn_submit)
    void onSubmit(View view) {
        profileFromUpdate = true;
        hideSoftKey();
        storeUp();
        fieldFocus = checkHasFocus();
        if (formValidation()) {
            listener.onProfilePMACreate(profilePMAMapView, profile);
        }
    }


    @OnClick(R.id.tv_checkbox)
    void onCheckBoxTick(View view) {
        checkBox.toggle();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        storeUp();
        outState.putParcelable(STATE_INPUTS, profile);
        super.onSaveInstanceState(outState);
    }

    private boolean formValidation() {
        checkout();
        if (editText != null) {
            editText.requestFocus();
            editText.setError("");
            editText.setSelection(editText.getText().length());
            listener.onProfileInputError(errMessage);
            return false;
        }
        return true;
    }

    private void checkout() {
        editText = null;
        StringBuilder messageBuilder = new StringBuilder();
        if (!validator.name(profile.getFirstName())) {
            editText = edtFirstName;
            edtFirstName.setError("");
            if (profile.getFirstName().length() > 0)
                messageBuilder.append(getString(R.string.input_err_invalid_name, "first"));
        } else {
            edtFirstName.setError(null);
        }
        if (!validator.name(profile.getLastName())) {
            if (editText == null) editText = edtLastName;
            edtLastName.setError("");
            if (profile.getLastName().length() > 0) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(getString(R.string.input_err_invalid_name, "last"));
            }
        } else {
            edtLastName.setError(null);
        }

        if (!validator.phone(profile.getPhone())) {
            if (editText == null) editText = edtPhone;
            edtPhone.setError("");
            if (profile.getPhone().length() > 0) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(getString(R.string.input_err_invalid_phone));
            }
        } else {
            edtPhone.setError(null);
        }
        if (!validator.zipCode(profile.getZipCode())) {
            if (editText == null) editText = edtZipCode;
            edtZipCode.setError("");
            if (profile.getZipCode().length() > 0) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(getString(R.string.input_err_invalid_zip));
            }
        } else {
            edtZipCode.setError(null);
        }
        if (editText != null) {
            String temp = messageBuilder.toString();
            messageBuilder = new StringBuilder("");
            if (profile.anyEmpty()) {
                messageBuilder.append(getString(R.string.input_err_required));
            }
            if (!profile.allIsEmpty()) {
                if (messageBuilder.length() > 0) messageBuilder.append("\n");
                messageBuilder.append(temp);
            }
        }
        errMessage = messageBuilder.toString();
    }

    private void storeUp() {
        firstNameMasker.original = firstNameMasker.original.trim();
        lastNameMasker.original = lastNameMasker.original.trim();
        //emailMasker.original = emailMasker.original.trim();
        phoneMasker.original = phoneMasker.original.trim();
        String firstName = firstNameMasker.original;
        String lastName = lastNameMasker.original;
        //String email = emailMasker.original;
        String phone = phoneMasker.original;
        profile.setFirstName(firstName);
        profile.setLastName(lastName);
        profile.setPhone(phone);
        //profile.setEmail(email);
        profile.setZipCode(edtZipCode.getText().toString());
        profile.setAgreed(checkBox.isChecked());
    }

    private int checkHasFocus() {
        if (edtFirstName.hasFocus()) {
            profile.setFirstName(edtFirstName.getText().toString().trim());
            return 1;
        }
        if (edtLastName.hasFocus()) {
            profile.setLastName(edtLastName.getText().toString().trim());
            return 2;
        }

        if (edtPhone.hasFocus()) {
            profile.setPhone(edtPhone.getText().toString().trim());
            return 4;
        }
        // zip code
        return 5;
    }


    void loadProfile() {
        //this.profile.setMetadata(profile.getMetadata());
        firstNameMasker.original = profilePMAMapView.getFirstName();
        lastNameMasker.original = profilePMAMapView.getLastName();
        phoneMasker.original = MapProfile.getPhoneFormat(profilePMAMapView.getPhone());
        zipCodeMasker.original = MapProfile.getZipCodeFormat(profilePMAMapView.getZipCode());
        emailMasker.original = profilePMAMapView.getEmail();

        edtFirstName.setText(profilePMAMapView.getFirstName());
        edtLastName.setText(profilePMAMapView.getLastName());
        edtPhone.setText(MapProfile.getPhoneFormat(profilePMAMapView.getPhone()));
        edtZipCode.setText(MapProfile.getZipCodeFormat(profilePMAMapView.getZipCode()));
        checkBox.setChecked(profilePMAMapView.isAgreedNewsLetter());
        if (profileFromUpdate) {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, false);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, false);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, false);
        } else {
            edtFirstName.getOnFocusChangeListener().onFocusChange(edtFirstName, fieldFocus == 1);
            edtLastName.getOnFocusChangeListener().onFocusChange(edtLastName, fieldFocus == 2);
            edtPhone.getOnFocusChangeListener().onFocusChange(edtPhone, fieldFocus == 4);
        }

        profileComparator = new ProfileComparator(edtFirstName, edtLastName,
                edtPhone, edtZipCode, checkBox, btnSubmit);
        profileComparator.original = profile;

        resetErrorText();
    }


    private void clearFocus(EditText... editTexts) {
//        firstNameMask = true;
        for (EditText editText : editTexts) {
            editText.clearFocus();
        }
    }

    private void resetErrorText() {
        edtFirstName.setError(null);
        edtLastName.setError(null);
        edtZipCode.setError(null);
        edtPhone.setError(null);
    }

    private void processAfterKeyboardToggle(boolean isKeyboardShow) {
        View currentView = getActivity().getCurrentFocus();
        if (currentView != null && currentView instanceof EditText) {
            ((EditText) currentView).setCursorVisible(isKeyboardShow);

            if (!isKeyboardShow) {
                currentView.clearFocus();
                profileParentView.requestFocus();
                //currentView.getOnFocusChangeListener().onFocusChange(currentView, false);
            } else {
                //containView.clearFocus();
                //currentView.requestFocus();
            }
        }
    }


    public Rect contentWrapperViewRect() {
        if (contentWrapperViewRect == null) {
            int[] screenLocation = new int[2];
            contentWrapper.getLocationOnScreen(screenLocation);
            int left = screenLocation[0];
            int top = screenLocation[1];
            contentWrapperViewRect = new Rect(
                    left,
                    top,
                    left + contentWrapper.getWidth(),
                    top + contentWrapper.getHeight()
            );
        }
        return contentWrapperViewRect;
    }

    public void showSoftKey() {
        if (editText != null) {
            editText.requestFocus();
            editText.setError("");
            editText.setCursorVisible(true);
            editText.setSelection(editText.getText().toString().length());
            ViewUtils.showSoftKey(editText);
        }
    }

    void hideSoftKey() {
        InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(edtZipCode.getWindowToken(), 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideSoftKey();
    }

    public void scaleView() {
        Animation anim = new ScaleAnimation(
                1f, 1f, // Start and end values for the X axis scaling
                fakeViewToMoveButtonDown.getY(),
                fakeViewToMoveButtonDown.getY() + fakeViewToMoveButtonDown.getHeight(), // Start and end values for the Y axis scaling
                Animation.RELATIVE_TO_SELF, 0f, // Pivot point of X scaling
                Animation.RELATIVE_TO_SELF, 1f); // Pivot point of Y scaling
        anim.setFillAfter(false); // Needed to keep the result of the animation
        anim.setDuration(0);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //fakeViewToMoveButtonDown.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fakeViewToMoveButtonDown.startAnimation(anim);
    }

    public enum PROFILE {CREATE, EDIT, CREATE_WITH_PMA}

    public interface ProfileEventsListener {

        void onProfileInputError(String message);

        void onProfilePMACreate(ProfilePMAMapView profilePMAMapView, Profile profile);
    }

    private class FocusChangeListener implements View.OnFocusChangeListener {

        MaskerHelper.INPUT input;
        private String original = "";

        FocusChangeListener(MaskerHelper.INPUT type) {
            input = type;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            AppCompatEditText view = (AppCompatEditText) v;
            if (input == MaskerHelper.INPUT.PHONE) {
                if (hasFocus) {
                    if (original.length() == 0) {
                        view.removeTextChangedListener(phonePattern);
                        original = "1-";
                        view.setText(original);
                        view.setSelection(2);
                        view.addTextChangedListener(phonePattern);
                        return;
                    }
                } else {
                    String text = view.getText().toString();
                    if (text.length() == 2) {
                        original = "";
                        view.removeTextChangedListener(phonePattern);
                        view.setText(original);
                        view.addTextChangedListener(phonePattern);
                        return;
                    }
                }
            }
            if (hasFocus) {
                view.setText(original);
                edtEmail.setText(MaskerHelper.mask(MaskerHelper.INPUT.EMAIL, emailMasker.original));
            } else {
                String text = view.getText().toString().trim();
                original = text;
                view.setText(MaskerHelper.mask(input, original));
            }
            view.setCursorVisible(hasFocus);

        }
    }
}
