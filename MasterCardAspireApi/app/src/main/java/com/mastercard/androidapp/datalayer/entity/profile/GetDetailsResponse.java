package com.mastercard.androidapp.datalayer.entity.profile;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vinh.trinh on 6/19/2017.
 */
public class GetDetailsResponse {
    @SerializedName("Member")
    private Member member;
    @SerializedName("MemberDetails")
    private List<MemberDetail> memberDetails;

    public Member getMember() {
        return member;
    }

    public List<MemberDetail> getMemberDetails() {
        return memberDetails;
    }
}