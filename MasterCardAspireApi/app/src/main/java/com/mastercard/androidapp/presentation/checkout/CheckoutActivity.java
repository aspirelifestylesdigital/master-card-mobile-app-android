package com.mastercard.androidapp.presentation.checkout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;

import com.api.aspire.domain.model.ProfileAspire;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.domain.model.ProfileMetadata;
import com.mastercard.androidapp.presentation.base.CommonActivity;
import com.mastercard.androidapp.presentation.home.HomeActivity;
import com.mastercard.androidapp.presentation.info.MasterCardUtilityActivity;
import com.mastercard.androidapp.presentation.info.MasterCardUtilityFragment;
import com.mastercard.androidapp.presentation.profile.signUpV2.SignUpV2Activity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;

import org.json.JSONException;

import butterknife.ButterKnife;


public class CheckoutActivity extends CommonActivity implements BINCodeCheckout.View,
        BINCheckoutFragment.BINCheckoutListener, PrivacyPolicyFragment.PrivacyPolicyListener {

    DialogHelper dialogHelper;
    BINCheckoutPresenter presenter;
    ProfileAspire profileAspire;

    //-- flow Sign In or Splash Screen (check binCode seconds time)
    private String msgInValidBinCode;

    private BINCheckoutPresenter binCheckoutPresenter() {
        return new BINCheckoutPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        dialogHelper = new DialogHelper(this);
        presenter = binCheckoutPresenter();
        presenter.attach(this);
        if (savedInstanceState == null) {
            boolean checkout = getIntent().getIntExtra(Intent.EXTRA_REFERRER, 0) == 0;
            boolean majorChange = getIntent().getBooleanExtra(PrivacyPolicyFragment.VERSION_MAJOR_CHANGE, false);

            Fragment fragment = checkout ? BINCheckoutFragment.newInstance() :
                    PrivacyPolicyFragment.newInstance(majorChange);

            String tag = checkout ? BINCheckoutFragment.class.getSimpleName() :
                    PrivacyPolicyFragment.class.getSimpleName();

            boolean flowFromSplashScreen = getIntent().getBooleanExtra(IntentConstant.SPLASH_SCREEN_HIDE_NAV_BACK, false);
            if (!checkout) {
                hideNavigatorBack();
                appBar.setVisibility(View.GONE);
            }else if(flowFromSplashScreen){
                hideNavigatorBack();
            }

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragment_place_holder, fragment, tag)
                    .commit();
        }
        msgInValidBinCode = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        if (!TextUtils.isEmpty(msgInValidBinCode)) {
            // this is when current BIN code is expired
            dialogHelper.action(null,
                    msgInValidBinCode,
                    getString(R.string.text_apply),
                    getString(R.string.text_cancel),
                    (dialog, which) -> creditCardSignup());
        }

        if (getIntent().hasExtra(IntentConstant.SIGN_IN_PROFILE)) {
            String profileStr = getIntent().getStringExtra(IntentConstant.SIGN_IN_PROFILE);
            if (!TextUtils.isEmpty(profileStr)) {
                this.profileAspire = new ProfileAspire(profileStr);
            }
        }
    }

    private void hideNavigatorBack(){
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            //- don't swipe
            disableSwipe();
        }
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    private void creditCardSignup() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.mastercard.com/sea/consumer/world_worldelite_mastercard.html"));
        startActivity(browserIntent);
    }

    // BINCheckoutListener
    @Override
    public void onBINCodeSubmit(String input) {
        if (profileAspire != null) {
            presenter.checkoutBINCode(input, profileAspire);
        } else {
            presenter.checkoutBINCode(input);
        }

    }

    // BINCheckoutListener
    @Override
    public void onTermOfUseClick() {
        Intent intent = new Intent(this, MasterCardUtilityActivity.class);
        intent.putExtra(IntentConstant.MASTERCARD_COPY_UTILITY, AppConstant.MASTERCARD_COPY_UTILITY.TermsOfUse);
        startActivity(intent);
    }

    // BINCheckoutListener
    @Override
    public void onPrivacyPolicyClick() {
        Intent intent = new Intent(this, MasterCardUtilityActivity.class);
        intent.putExtra(IntentConstant.MASTERCARD_COPY_UTILITY, AppConstant.MASTERCARD_COPY_UTILITY.Privacy);
        startActivity(intent);
    }

    // PrivacyPolicyListener
    @Override
    public void onPrivacyEnter() {
        presenter.loadPrivacyContent();
    }

    // PrivacyPolicyListener
    @Override
    public void onPrivacySubmit() {
        if (profileAspire != null) {
            //case sign in
            presenter.onPolicyAcceptedFromSignIn(profileAspire);
        } else {
            presenter.onPolicyAccepted();
        }
    }

    // presenter
    @Override
    public void privacyContentLoaded(String content) {
        FragmentManager fm = getSupportFragmentManager();
        ((MasterCardUtilityFragment) fm
                .findFragmentByTag(MasterCardUtilityFragment.class.getSimpleName()))
                .renderContent(content);
        ((PrivacyPolicyFragment) fm
                .findFragmentByTag(PrivacyPolicyFragment.class.getSimpleName()))
                .contentLoaded();
    }

    // presenter
    @Override
    public void proceedToSignUp(ProfileMetadata profileMetadata) {
        Intent intent = new Intent(this, SignUpV2Activity.class);
        try {
            intent.putExtra(Intent.EXTRA_TEXT, profileMetadata.toJSON().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    // presenter
    @Override
    public void proceedToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    // presenter
    @Override
    public void showInvalidBINCodeDialog(boolean profileCreated, boolean binExpired) {
        //splash expire and input old bin
        if (!TextUtils.isEmpty(msgInValidBinCode)
                && getString(R.string.checkout_bin_expired).equalsIgnoreCase(msgInValidBinCode)
                && binExpired) {

            //-- sign in / splash screen
            //-- input bin code = old bin local
            showDialogBinCodeInvalid(getString(R.string.checkout_bin_expired));

        } else if (!profileCreated || !binExpired) {
            //- sign up or input invalid bin
            //- expired but new bin is different old bin
            showDialogBinCodeInvalid(getString(R.string.checkout_err_msg));
        } else {

            showDialogBinCodeInvalid(getString(R.string.checkout_err_msg));
        }
    }

    private void showDialogBinCodeInvalid(String errorMessage) {
        dialogHelper.action(null,
                errorMessage,
                getString(R.string.text_apply),
                getString(R.string.text_cancel),
                (dialog, which) -> creditCardSignup());
    }

    // presenter
    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
//        String message;
        switch (errCode) {
            case CONNECTIVITY_PROBLEM:
                //extraMsg = getString(R.string.no_network_connection);
                dialogHelper.showNoInternetAlert();
                break;
            default:
                dialogHelper.showTimeoutError();
                //extraMsg = getString(R.string.bin_check_timeout);
                break;
        }
        //dialogHelper.alert(null, extraMsg);
    }

    // presenter
    @Override
    public void showProgressDialog() {
        //dialogHelper.showProgress(getString(R.string.dialog_checking));
        ProgressDialogUtil.showLoading(this);
    }

    // presenter
    @Override
    public void dismissProgressDialog() {
        //dialogHelper.dismissProgress();
        ProgressDialogUtil.hideLoading();
    }

    /*@Override
    public boolean onTouchEvent(MotionEvent event) {
        View view = findViewById(R.id.fragment_place_holder);
        InputMethodManager imm = (InputMethodManager) this
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        return super.onTouchEvent(event);
    }*/

}
