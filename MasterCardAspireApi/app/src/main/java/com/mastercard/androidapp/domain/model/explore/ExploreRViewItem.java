package com.mastercard.androidapp.domain.model.explore;

import android.os.Parcel;
import android.support.annotation.NonNull;

import com.mastercard.androidapp.presentation.explore.DiningSortingCriteria;

/**
 * Created by vinh.trinh on 5/12/2017.
 */

public class ExploreRViewItem extends ExploreRView implements Comparable<ExploreRViewItem> {
    public final int id;
    public final String title;
    public final String description;
    public final String summary;
    public final boolean hasStar;
    public final String imageURL;
    public final int dataListIndex;
    private String categoryName;
    private DiningSortingCriteria sortingCriteria;
    private String subCategoryName;

    public ExploreRViewItem(int id, String title, String description, String imageURL, boolean hasStar,
                            String summary, int dataIndex) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.summary = summary;
        this.imageURL = imageURL;
        this.hasStar = hasStar;
        itemType = ItemType.NORMAL;
        this.dataListIndex = dataIndex;
    }

    protected ExploreRViewItem(Parcel in) {
        id = in.readInt();
        title = in.readString();
        description = null;
        summary = null;
        hasStar = false;
        imageURL = null;
        dataListIndex = -1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
    }

    public static final Creator<ExploreRViewItem> CREATOR = new Creator<ExploreRViewItem>() {
        @Override
        public ExploreRViewItem createFromParcel(Parcel in) {
            return new ExploreRViewItem(in);
        }

        @Override
        public ExploreRViewItem[] newArray(int size) {
            return new ExploreRViewItem[size];
        }
    };

    @Override
    public int compareTo(@NonNull ExploreRViewItem o) {
        return this.title.compareToIgnoreCase(o.title);
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getImageUrl() {
        return imageURL;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public String categoryName() {
        return categoryName;
    }

    public void subCategoryName(String subcat) {
        this.subCategoryName = subcat;
    }

    public void categoryName(String cat) {
        this.categoryName = cat;
    }

    public DiningSortingCriteria getSortingCriteria() {
        return sortingCriteria;
    }

    public void setSortingCriteria(DiningSortingCriteria sortingCriteria) {
        this.sortingCriteria = sortingCriteria;
    }

    public String getDisplayCategory() {


        if ("Flowers".equalsIgnoreCase(categoryName)) {
            return "Flowers";
        }

        if ("Specialty Travel".equalsIgnoreCase(categoryName) &&
                (("Entertainment Experiences".equalsIgnoreCase(subCategoryName)) || ("Major Sports Events".equalsIgnoreCase(subCategoryName)))) {
            return "Experiences";
        }

        if ("Tickets".equalsIgnoreCase(categoryName)) {
            return "ARTS + \\nCULTURE";
        }

        if ("VIP Travel Services".equalsIgnoreCase(categoryName) &&
                (("Travel Services".equalsIgnoreCase(subCategoryName)))) {
            return "TRAVEL";
        }

        if ("Golf Merchandise".equalsIgnoreCase(categoryName) ||
                "Flowers".equalsIgnoreCase(categoryName) || "Wine".equalsIgnoreCase(categoryName)) {
            return "Shopping";
        }
        if ("Dining".equalsIgnoreCase(categoryName) ||
                (("Specialty Travel".equalsIgnoreCase(categoryName))) && ("Culinary Experiences".equalsIgnoreCase(subCategoryName))) {
            return "Dining";
        }

        return "";


    }
}
