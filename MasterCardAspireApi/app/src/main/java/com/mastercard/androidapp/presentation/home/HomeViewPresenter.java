package com.mastercard.androidapp.presentation.home;

import android.content.Intent;
import android.text.TextUtils;

import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.usecases.SignOut;
import com.mastercard.androidapp.common.constant.CityData;
import com.mastercard.androidapp.common.constant.RequestCode;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;
import com.mastercard.androidapp.presentation.changepass.ChangePasswordActivity;
import com.mastercard.androidapp.presentation.checkout.signInV2.SignInActivity;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomeViewPresenter {

    private HomeActivity homeActivity;
    private SignOut signOutCase;
    private GetToken getTokenCase;

    HomeViewPresenter(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
        PreferencesStorageAspire preferencesStorageAspire = new PreferencesStorageAspire(homeActivity.getApplicationContext());
        this.signOutCase = new SignOut(preferencesStorageAspire);
        this.getTokenCase = new GetToken(preferencesStorageAspire, new MapProfileApp());
    }

    void signOut() {

        // revoke token
        String token = getTokenCase.getAuthTokenCurrent();
        if (!TextUtils.isEmpty(token)) {
            getTokenCase.revokeToken(token)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe();
        }

        signOutCase.setSignOutProfile(() -> {
            //remove select city data
            CityData.reset();
        });
        Intent intent = new Intent(homeActivity, SignInActivity.class);
        homeActivity.startActivity(intent);
        homeActivity.finish();
    }

    void navigateToChangePassword() {
        Intent intent = new Intent(homeActivity, ChangePasswordActivity.class);
        homeActivity.startActivityForResult(intent, RequestCode.CHANGE_PASSWORD);
    }
}
