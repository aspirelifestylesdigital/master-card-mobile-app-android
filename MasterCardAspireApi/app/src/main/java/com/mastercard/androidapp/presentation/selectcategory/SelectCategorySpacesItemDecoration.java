package com.mastercard.androidapp.presentation.selectcategory;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SelectCategorySpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;

    public SelectCategorySpacesItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {

//        outRect.right = space;
//
        if (parent.getChildAdapterPosition(view) == 0) {
            outRect.top = 0;
        } else {
            outRect.top = space * 2;
        }

        if (parent.getChildLayoutPosition(view) != 0) {
            if (parent.getChildAdapterPosition(view) % 2 == 0) {
                outRect.left = space;
            } else {
                outRect.right = space;
            }
        }
    }
}