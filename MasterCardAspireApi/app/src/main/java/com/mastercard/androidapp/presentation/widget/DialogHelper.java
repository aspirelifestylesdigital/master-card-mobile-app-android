package com.mastercard.androidapp.presentation.widget;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.SignOut;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.CityData;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;
import com.mastercard.androidapp.presentation.checkout.signInV2.SignInActivity;

import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class DialogHelper {

    private Activity activity;
    private MaterialDialog progressDialog;

    public DialogHelper(Activity activity) {
        this.activity = activity;
    }

    public void alert(String title, String message) {
        alert(title, message, null);
    }

    public void alert(String title, String message, DialogInterface.OnDismissListener dismissListener) {
        Dialog dialog = new MyDialogBuilder(activity, title, message)
                .positiveText(R.string.text_ok)
                .onDismiss(dismissListener)
                .single(true)
                .build();
        dialog.show();
    }

    public void action(String title, String message, String positiveText, String negativeText,
                       DialogInterface.OnClickListener takeAction) {
        Dialog dialog = new MyDialogBuilder(activity, title, message)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .onPositive(takeAction)
                .build();
        dialog.show();
    }

    public void action(String title, String message, String positiveText, String negativeText,
                       DialogInterface.OnClickListener takeAction, DialogInterface.OnClickListener cancelAction) {
        Dialog dialog = new MyDialogBuilder(activity, title, message)
                .positiveText(positiveText)
                .negativeText(negativeText)
                .onPositive(takeAction)
                .onNegative(cancelAction)
                .build();
        dialog.show();
    }


    public void showProgress(String message) {
        showProgress(message, false, null);
    }

    /**
     * @param message
     * @param cancellable by default, in case cancellable = false -> ignore listener
     * @param listener
     */
    public void showProgress(String message, boolean cancellable
            , DialogInterface.OnCancelListener listener) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = new MaterialDialog.Builder(activity)
                .content(message)
                .progress(true, 0)
                .cancelable(cancellable)
                .cancelListener(listener)
                .show();
    }

    public void dismissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public MyDialogBuilder createBuilder(int title, int content, int positiveText, int negativeText) {
        return new MyDialogBuilder(activity, title, content)
                .positiveText(positiveText)
                .negativeText(negativeText);
    }

    public MyDialogBuilder createBuilderWithLayout(int title, int content, int positiveText, int negativeText, int layout) {
        return new MyDialogBuilder(activity, title, content, layout)
                .positiveText(positiveText)
                .negativeText(negativeText);
    }

    public void showLeavingAlert(String url) {
        Dialog dialog = new MyDialogBuilder(activity, R.string.text_alert, R.string.text_leaving_message)
                .onPositive((dialog1, which) -> {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse(url));
                    activity.startActivity(browserIntent);
                }).build();
        dialog.show();
    }

    public void showLeavingAlert(DialogInterface.OnClickListener onClickListener) {
        Dialog dialog = new MyDialogBuilder(activity, R.string.text_alert, R.string.text_leaving_message)
                .onPositive(onClickListener).build();
        dialog.show();
    }

    public void showRootedDeviceAlert(Activity activity) {
        Dialog dialog = new MyDialogBuilder(activity, 0, R.string.text_device_rooted_error)
                .onPositive((dialog1, which) -> {
                    activity.finishAffinity();
                })
                .positiveText("OK")
                .single(true)
                .cancelable(false)
                .build();
        dialog.show();
    }

    public void showNoInternetAlert() {
        showNoInternetAlert(null, null);
    }

    public void showNoInternetAlert(DialogInterface.OnClickListener settingsListener, DialogInterface.OnClickListener okClickListener) {
        Dialog dialog = new MyDialogBuilder(activity, R.string.unable_get_data_title, R.string.no_network_connection)
                .onPositive(settingsListener != null ? settingsListener : (dialog1, which) -> {
                    // Go to wifi settings
                    activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
                })
                .onNegative(okClickListener)
                .positiveText("SETTINGS")
                .negativeText("OK")
                .build();
        dialog.show();
    }

    public void showTimeoutError() {
        showTimeoutError(null, null);
    }

    public void showTimeoutError(DialogInterface.OnClickListener settingsListener, DialogInterface.OnClickListener okClickListener) {
        Dialog dialog = new MyDialogBuilder(activity, R.string.unable_get_data_title, R.string.timeout_error)
                .onPositive(settingsListener != null ? settingsListener : (dialog1, which) -> {
                    // Go to wifi settings
                    activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
                })
                .onNegative(okClickListener)
                .positiveText("SETTINGS")
                .negativeText("OK")
                .build();
        dialog.show();
    }

    public void profileDialog(String message, DialogInterface.OnDismissListener dismissListener) {
        final Dialog customDialog = new Dialog(activity, R.style.AppDialogTheme);
        customDialog.setContentView(R.layout.custom_alert_dialog);
        TextView titleView = ButterKnife.findById(customDialog, R.id.title);
        TextView messageView = ButterKnife.findById(customDialog, R.id.message);
        Button positiveBtn = ButterKnife.findById(customDialog, R.id.positive_btn);
        Button negativeBtn = ButterKnife.findById(customDialog, R.id.negative_btn);

        positiveBtn.setText(R.string.text_ok);
        titleView.setText(R.string.input_err_fields);
        messageView.setText(message);
        messageView.setGravity(Gravity.START);
        negativeBtn.setVisibility(View.GONE);
        positiveBtn.setBackground(null);
        positiveBtn.setOnClickListener(v -> customDialog.dismiss());
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.setOnDismissListener(dismissListener);
        customDialog.show();
    }

    public void signInDialog(String message, DialogInterface.OnDismissListener dismissListener) {
        final Dialog customDialog = new Dialog(activity, R.style.AppDialogTheme);
        customDialog.setContentView(R.layout.custom_alert_dialog);
        TextView titleView = ButterKnife.findById(customDialog, R.id.title);
        TextView messageView = ButterKnife.findById(customDialog, R.id.message);
        Button positiveBtn = ButterKnife.findById(customDialog, R.id.positive_btn);
        Button negativeBtn = ButterKnife.findById(customDialog, R.id.negative_btn);

        positiveBtn.setText(R.string.text_ok);
        titleView.setVisibility(View.GONE);
        if (TextUtils.isEmpty(message)) {
            messageView.setVisibility(View.GONE);
        }
        messageView.setText(message);
        messageView.setGravity(Gravity.CENTER);
        negativeBtn.setVisibility(View.GONE);
        positiveBtn.setBackground(null);
        positiveBtn.setOnClickListener(v -> customDialog.dismiss());
        customDialog.setCancelable(true);
        customDialog.setCanceledOnTouchOutside(true);
        customDialog.setOnDismissListener(dismissListener);
        customDialog.show();
    }


    public void showGeneralError() {
        Dialog dialog = new MyDialogBuilder(activity, "Cannot Get Data",
                "An error has occurred. Check your internet settings or try again.")
                .onPositive((dialog1, which) -> {
                    // Go to wifi settings
                    activity.startActivity(new Intent(Settings.ACTION_SETTINGS));
                })
                .onNegative((dialogInterface, i) -> dialogInterface.dismiss())
                .positiveText("SETTINGS")
                .negativeText("OK")
                .autoDismiss(false)
                .build();
        dialog.show();
    }

    public void showGetTokenError() {
        Dialog dialog = new MyDialogBuilder(activity, App.getInstance().getString(R.string.errorTitle),
                App.getInstance().getString(R.string.errorForceSignOut))
                .onPositive((dialog1, which) -> {

                    PreferencesStorageAspire preferencesStorageAspire = new PreferencesStorageAspire(activity.getApplicationContext());
                    SignOut signOut = new SignOut(preferencesStorageAspire);
                    GetToken getTokenCase = new GetToken(preferencesStorageAspire, new MapProfileApp());
                    // revoke token
                    String token = getTokenCase.getAuthTokenCurrent();
                    if (!TextUtils.isEmpty(token)) {
                        getTokenCase.revokeToken(token)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe();
                    }

                    signOut.setSignOutProfile(() -> {
                        //remove select city data
                        CityData.reset();
                    });

                    dialog1.dismiss();
                    Intent intent = new Intent(activity, SignInActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activity.startActivity(intent);
                    activity.finish();

                })
                .single(true)
                .positiveText(App.getInstance().getString(R.string.text_ok))
                .autoDismiss(false)
                .build();
        dialog.show();
    }

    public boolean networkUnavailability(ErrCode errCode, String extraMessage) {
        if (isNetworkUnEnable(errCode, extraMessage)) {
            Dialog dialog = new MyDialogBuilder(activity, "Cannot Get Data",
                    "Turn on cellular data or use Wi-Fi to access Mobile Concierge.")
                    .positiveText("SETTINGS")
                    .negativeText("OK")
                    .onPositive((dialogInterface, i) -> activity.startActivity(new Intent(Settings.ACTION_SETTINGS)))
                    .onNegative((dialogInterface, i) -> dialogInterface.dismiss())
                    .autoDismiss(false)
                    .build();
            dialog.show();
            return true;
        }
        return false;
    }

    public boolean isNetworkUnEnable(ErrCode errCode, String extraMessage) {
        return errCode == ErrCode.CONNECTIVITY_PROBLEM || (extraMessage != null && extraMessage.contains("Unable to resolve host"));
    }

}
