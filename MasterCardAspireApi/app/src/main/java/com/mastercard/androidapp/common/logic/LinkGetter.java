package com.mastercard.androidapp.common.logic;

/**
 * Created by ThuNguyen on 6/29/2017.
 */

import android.text.TextUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkGetter {
    private Pattern htmlTag;

    public LinkGetter() {
        htmlTag = Pattern.compile("<a\\b[^>]*href=\"[^>]*>(.*?)</a>");
    }

    public String removeInvalidHyperlink(String htmlText) {
        StringBuilder builder = new StringBuilder();
        builder.append(htmlText);
        String result = builder.toString();

        Matcher tagmatch = htmlTag.matcher(builder.toString());
        while (tagmatch.find()) {
            if(!tagmatch.group().contains("http")){
                int firstFoundIndex = result.indexOf(tagmatch.group());
                if(firstFoundIndex > -1){
                    result = result.substring(0, firstFoundIndex);
                }else {
                    result = result.replace(tagmatch.group(), "");
                }
            }
        }

        return result;
    }

    /**
     * "www.mastercard.com" ---> "https://www.mastercard.com"
     * @param htmlText
     * @return the htmlText with all valid hyperlink
     */
    public String adjustInvalidHyperlink(String htmlText) {
        String result = htmlText;
        Document doc = Jsoup.parse(htmlText);
        Elements elementsWithHrefAttributes = doc.select("[href]");
        for (Element element: elementsWithHrefAttributes) {
            String href = element.attr("href");
            if(!TextUtils.isEmpty(href) && !href.startsWith("mailto")&& !href.startsWith("#") && !href.contains("http")){ // href="#..." for index link
                int firstIndexOfHref = result.indexOf(href);
                if(firstIndexOfHref > -1){
                    result = result.substring(0, firstIndexOfHref) + "https://" + result.substring(firstIndexOfHref);
                }
            }
            //System.out.println("href content: " + href);
        }
        // Replace mailto redundant also
        result = result.replaceAll("mailto:\\s+", "mailto:");
        return result;
    }
}
