
package com.mastercard.androidapp.presentation.checkout.signInV2;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.domain.model.ProfileAspire;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.common.constant.RequestCode;
import com.mastercard.androidapp.common.logic.Validator;
import com.mastercard.androidapp.presentation.base.BaseActivity;
import com.mastercard.androidapp.presentation.changepass.ChangePasswordActivity;
import com.mastercard.androidapp.presentation.checkout.CheckoutActivity;
import com.mastercard.androidapp.presentation.checkout.PrivacyPolicyFragment;
import com.mastercard.androidapp.presentation.home.HomeActivity;
import com.mastercard.androidapp.presentation.profile.SignUpActivity;
import com.mastercard.androidapp.presentation.profile.forgotPwdV2.ForgotPasswordV2Activity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.PasswordInputFilter;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;
import com.mastercard.androidapp.presentation.widget.ViewKeyboardListener;
import com.mastercard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.widget.ButtonTextSpacing;
import com.support.mylibrary.widget.ErrorIndicatorEditText;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends BaseActivity implements SignIn.View {
    @BindView(R.id.edt_email)
    ErrorIndicatorEditText edtEmail;
    @BindView(R.id.edt_password)
    ErrorIndicatorEditText edtPassword;
    @BindView(R.id.btn_sign_in)
    ButtonTextSpacing btnSignIn;
    @BindView(R.id.holder)
    ConstraintLayout llRootView;
    @BindView(R.id.llParent)
    ConstraintLayout llParent;
    @BindView(R.id.scrollView)
    ScrollView scroll;
    DialogHelper dialogHelper;
    SignInPresenter presenter;
    Validator validator;

    private SignInPresenter buildPresenter() {
        return new SignInPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
        ButterKnife.bind(this);
        final Handler handler = new Handler();
        dialogHelper = new DialogHelper(this);
        presenter = buildPresenter();
        presenter.attach(this);
        validator = new Validator();
        keyboardInteractListener();

        btnSignIn.setEnabled(false);
        btnSignIn.post(new Runnable() {
            @Override
            public void run() {
                btnSignIn.setClickable(false);
            }
        });

        edtEmail.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                if (edtEmail.getText().toString().trim().length() == 0) {
                    edtEmail.setText("");
                    btnSignIn.setEnabled(false);
                }
                ViewUtils.hideSoftKey(edtEmail);
            } else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {
                    }
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }
            edtEmail.setCursorVisible(b);
        });

        edtPassword.setOnFocusChangeListener((view1, b) -> {
            if (!b) {
                ViewUtils.hideSoftKey(edtPassword);
            } else {
                new Thread(() -> {
                    try {
                        Thread.sleep(350);
                    } catch (InterruptedException e) {
                    }
                    handler.post(() -> scroll.scrollTo(0, scroll.getBottom()));
                }).start();
            }
            edtPassword.setCursorVisible(b);
        });

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (edtEmail.getText().toString().trim().length() == 0) {
                    btnSignIn.setEnabled(false);
                } else {
                    boolean isEnabled = editable.toString().length() > 0
                            && edtPassword.getText().toString().length() > 0;

                    btnSignIn.setEnabled(isEnabled);
                    btnSignIn.setClickable(isEnabled);
                }
            }
        });

        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                boolean isEnabled = editable.toString().length() > 0
                        && edtEmail.getText().toString().length() > 0;
                btnSignIn.setEnabled(isEnabled);
                btnSignIn.setClickable(isEnabled);
            }
        });

        edtPassword.setCustomSelectionActionModeCallback(new android.view.ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                return false;
            }

            @Override
            public void onDestroyActionMode(android.view.ActionMode actionMode) {
            }
        });

        edtPassword.setFilters(new InputFilter[]{new PasswordInputFilter(),
                new InputFilter.LengthFilter(
                        getResources().getInteger(R.integer.password_max_length_characters))});

        if (savedInstanceState == null) {
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.SIGN_IN.getValue());
        }


    }

    /**
     * handle click on back hide keyboard close cursor
     */
    private void keyboardInteractListener() {
        ViewKeyboardListener.KeyboardEvent event = new ViewKeyboardListener.KeyboardEvent() {
            @Override
            public void showKeyboard() {
                //dummy do nothing
            }

            @Override
            public void hideKeyboard() {
                //dummy do nothing
            }

            @Override
            public View getCurrentFocus() {
                return SignInActivity.this.getCurrentFocus();
            }
        };
        //-- add listen keyboard
        ViewKeyboardListener keyboardListener = new ViewKeyboardListener(llRootView, event);
        keyboardListener.setViewFocusChangeListener(llParent);
    }

    @Override
    protected void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @OnClick(R.id.btn_sign_in)
    public void signInClick(View view) {
        ViewUtils.hideSoftKey(view);
        String email = edtEmail.getText().toString();
        String password = edtPassword.getText().toString();

        //-- clean all error highlight
        edtEmail.setError(null);
        edtPassword.setError(null);

        final SignInValidate validate = validateFields(email, password);
        if (validate.isNoHaveErrorInput()) {
            presenter.doSignIn(email, password);
        } else {
            dialogHelper.signInDialog(validate.errorMgs, dialog -> {
                final ErrorIndicatorEditText edtError = validate.errorValidate;
                if (edtError != null) {
                    edtError.postDelayed(() -> {
                        edtError.requestFocus();
                        edtError.setError("");
                        edtError.setCursorVisible(true);
                        edtError.setSelection(edtError.getText().toString().length());

                        ViewUtils.showSoftKey(edtError);
                    }, 100);
                }
            });
        }
    }

    @OnClick(R.id.tv_sign_up)
    public void signUp(View view) {
        Intent intent = new Intent(SignInActivity.this, CheckoutActivity.class);
        startActivityForResult(intent, RequestCode.REQUEST_CLEAR_FORM_SIGN_IN);
        new Handler().postDelayed(this::clearForm, 250);
    }

    @OnClick(R.id.tv_forgot_pw)
    public void forgotPasswordClick() {
        Intent intent = new Intent(SignInActivity.this, ForgotPasswordV2Activity.class);
        startActivityForResult(intent, RequestCode.SIGN_IN_FORGOT);
        new Handler().postDelayed(this::clearForm, 250);
    }

    @OnClick({R.id.holder, R.id.llParent})
    public void onClickOutsideView(View view) {
        hideAllEdtCursor();
    }

    private void hideAllEdtCursor() {
        edtEmail.setCursorVisible(false);
        edtPassword.setCursorVisible(false);
    }

    @Override
    public void proceedToHome() {
        boolean hasForgotPwd = new PreferencesStorageAspire(getApplicationContext()).hasForgotPwd();
        Intent intent;
        if (hasForgotPwd) {
            intent = new Intent(this, ChangePasswordActivity.class);
            intent.putExtra(Intent.EXTRA_REFERRER, "a");
        } else {
            intent = new Intent(this, HomeActivity.class);
        }

        startActivity(intent);
        finish();

        // Track GA with "Sign in" event
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.AUTHENTICATION.getValue(),
                AppConstant.GA_TRACKING_ACTION.CLICK.getValue(),
                AppConstant.GA_TRACKING_LABEL.SIGN_IN.getValue());
    }

    @Override
    public void onCheckPassCodeFailure() {
        dialogHelper.action(App.getInstance().getString(R.string.errorTitle),
                getString(R.string.checkout_bin_expired), getString(R.string.text_ok), getString(R.string.text_cancel),
                (dialogInterface, i) -> toNewPassCodeScreen());
    }

    @Override
    public void toBINCheckout(ProfileAspire profileAspire, boolean messageInclude) {
        Intent intent = new Intent(this, CheckoutActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, 0);
        if (messageInclude) {
            intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.checkout_bin_expired));
        }
        if (profileAspire != null) {
            String valueAspire = "";
            try {
                valueAspire = profileAspire.toJSON().toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra(IntentConstant.SIGN_IN_PROFILE, valueAspire);
        }

        startActivity(intent);
    }

    @Override
    public void toPrivacyPolicy(ProfileAspire profileAspire, boolean isMajorChange) {
        Intent intent = new Intent(this, CheckoutActivity.class);
        intent.putExtra(Intent.EXTRA_REFERRER, 1);
        intent.putExtra(PrivacyPolicyFragment.VERSION_MAJOR_CHANGE, isMajorChange);
        if (profileAspire != null) {
            String valueAspire = "";
            try {
                valueAspire = profileAspire.toJSON().toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra(IntentConstant.SIGN_IN_PROFILE, valueAspire);
        }
        startActivity(intent);
    }

    private void toNewPassCodeScreen() {
        Intent intent = new Intent(this, CheckoutActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showErrorDialog(ErrCode errCode, String extraMsg) {
        if (dialogHelper.networkUnavailability(errCode, extraMsg)) return;
        edtEmail.setError("");
        edtPassword.setError("");
        if (errCode == ErrCode.API_ERROR)
            dialogHelper.alert(getString(R.string.errorTitle), extraMsg);
        else dialogHelper.showGeneralError();
    }

    @Override
    public void showProgressDialog() {
        ProgressDialogUtil.showLoading(this);
    }

    @Override
    public void dismissProgressDialog() {
        ProgressDialogUtil.hideLoading();
    }

    //<editor-fold desc="Validate edt input">
    private SignInValidate validateFields(String email, String password) {
        ErrorIndicatorEditText valid = null;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getString(R.string.errorSignIn));
        if (TextUtils.isEmpty(email.trim()) || TextUtils.isEmpty(password.trim())) {
            if (TextUtils.isEmpty(email)) edtEmail.setError("");
            if (TextUtils.isEmpty(password)) edtPassword.setError("");
            else edtPassword.setError(null);
            //stringBuilder.append(getString(R.string.input_err_required));
            valid = edtEmail;
        }

        if (!TextUtils.isEmpty(email)) {
            if (!validator.email(email)) {
                edtEmail.setError("");
                //if (stringBuilder.length() > 0) stringBuilder.append("\n");
                //stringBuilder.append(getString(R.string.input_err_invalid_email));
                valid = edtEmail;
            } else {
                edtEmail.setError(null);
            }
        }

//        if (TextUtils.isEmpty(password) || password.length() < 5) {
//            edtPassword.setError("");
//            //if (stringBuilder.length() > 0) stringBuilder.append("\n");
//            //stringBuilder.append(getString(R.string.input_err_old_pwd_failed));
//            valid = edtPassword;
//        } else {
//            edtPassword.setError(null);
//        }

        String errMessage = stringBuilder.toString();
        return new SignInValidate(valid, errMessage);
    }

    private class SignInValidate {
        ErrorIndicatorEditText errorValidate = null;
        String errorMgs = "";

        SignInValidate(ErrorIndicatorEditText errorValidate, String errorMgs) {
            this.errorValidate = errorValidate;
            this.errorMgs = errorMgs;
        }

        boolean isNoHaveErrorInput() {
            return errorValidate == null;
        }
    }
    //</editor-fold>

    private void clearForm() {
        edtEmail.setText("");
        edtPassword.setText("");

        edtEmail.hideErrorColorLine();
        edtPassword.hideErrorColorLine();

        if (getCurrentFocus() != null) {
            ViewUtils.hideSoftKey(getCurrentFocus());
            getCurrentFocus().clearFocus();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK){
            switch (requestCode){
                case RequestCode.SIGN_IN_FORGOT:
                    String emailForgot = "";
                    if(data != null && data.hasExtra(IntentConstant.EMAIL_VALUES_FORGOT)){
                        emailForgot = data.getStringExtra(IntentConstant.EMAIL_VALUES_FORGOT);
                    }
                    edtEmail.setText(emailForgot);
                    break;
            }
        }
    }
}
