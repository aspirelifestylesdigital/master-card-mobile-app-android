package com.mastercard.androidapp.presentation.explore;


import android.text.TextUtils;

import com.mastercard.androidapp.App;
import com.mastercard.androidapp.common.constant.CityData;
import com.mastercard.androidapp.domain.model.explore.ExploreRView;
import com.mastercard.androidapp.domain.model.explore.ExploreRViewItem;
import com.mastercard.androidapp.domain.model.explore.LatLng;
import com.mastercard.androidapp.domain.usecases.AccommodationSearch;
import com.mastercard.androidapp.domain.usecases.GetAccommodationByCategories;
import com.mastercard.androidapp.domain.usecases.GetCityGuides;
import com.mastercard.androidapp.domain.usecases.GetDinningList;
import com.mastercard.androidapp.domain.usecases.UseCase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by tung.phan on 5/8/2017.
 */

public class ExplorePresenter implements Explore.Presenter {

    public static final int DEFAULT_PAGE = 1;
    private final Map<String, String[]> categoryMapper = new HashMap<>();

    private CompositeDisposable disposables;
    private GetAccommodationByCategories getAccommodationByCategories;
    private GetCityGuides getCityGuides;
    private GetDinningList getDinningList;
    private AccommodationSearch accommodationSearch;
    private Explore.View view;
    private UseCase executingUseCase;

    private Single<List<ExploreRViewItem>> historyData;
    private int paging = DEFAULT_PAGE;
    private boolean reachEnd = false;
    // for other categories
    private String selectedCategory = "all";
    private String lastSelectedCity = "";
    // for city guide
    private int cityGuideIndex = -1;
    // for search
    private String term;
    private boolean withOffers;

    private DiningSortingCriteria diningSortingCriteria = new DiningSortingCriteria((LatLng)null, null);

    ExplorePresenter(GetAccommodationByCategories other, GetDinningList dining,
                     GetCityGuides cityGuides, AccommodationSearch search) {
        disposables = new CompositeDisposable();
        getAccommodationByCategories = other;
        getDinningList = dining;
        getCityGuides = cityGuides;
        accommodationSearch = search;
        buildCategoryMapper();
    }

    boolean inSearchingMode() {
        return !TextUtils.isEmpty(term) || withOffers;
    }

    String searchTerm() {
        return term;
    }

    boolean isWithOffers() {
        return withOffers;
    }

    @Override
    public void attach(Explore.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        disposables.dispose();
        this.view = null;
    }

    @Override
    public void getAccommodationData() {
        handleCategorySelection(selectedCategory);
    }

    @Override
    public void handleCitySelection() {
        if(lastSelectedCity.equals(CityData.cityName())) {
            return;
        }
        reachEnd = false;
        int diningCode = CityData.diningCode();
        lastSelectedCity = CityData.cityName();
        if(!TextUtils.isEmpty(term) || withOffers) {
            view.clearSearch(false);
        }
        if(selectedCategory.equals("dining") && diningCode > 0) {
            getDiningList(diningCode, DEFAULT_PAGE);
        }
        if(cityGuideIndex > -1) {
            getCityGuide(cityGuideIndex, DEFAULT_PAGE);
        } else if(this.selectedCategory.equals("all")) {
            getByAllCategories(DEFAULT_PAGE, false);
        } else {
            getAccommodationData(this.selectedCategory, DEFAULT_PAGE);
        }
    }

    @Override
    public void handleCategorySelection(String category) {
        reachEnd = false;
        cityGuideIndex = -1;
        this.selectedCategory = category.toLowerCase();

        if(inSearchingMode()) {
            view.clearSearch(false);
        }
        if(this.selectedCategory.equals("dining")) {
            if(CityData.diningCode() > 0)
                getDiningList(CityData.diningCode(), DEFAULT_PAGE);
            else {
                view.emptyData(false);
            }
        } else if(!this.selectedCategory.equals("all")) {
            getAccommodationData(this.selectedCategory, DEFAULT_PAGE);
        } else {
            getByAllCategories(DEFAULT_PAGE, false);
        }
    }

    @Override
    public void cityGuideCategorySelection(int index) {
        view.clearSearch(false);
        reachEnd = false;
        selectedCategory = "";
        cityGuideIndex = index;
        getCityGuide(cityGuideIndex, DEFAULT_PAGE);
    }

    @Override
    public void searchByTerm(String term, boolean withOffers) {
        reachEnd = false;
        this.term = term;
        this.withOffers = withOffers;
        if(selectedCategory.equals("all") || selectedCategory.equals("dining"))
            searchByTerm(DEFAULT_PAGE, term, withOffers, lastSelectedCity, selectedCategory);
        else
            getAccommodationData(this.selectedCategory, DEFAULT_PAGE);
    }

    @Override
    public void clearSearch() {
        this.term = "";
        withOffers = false;
    }

    @Override
    public void offHasOffers() {
        withOffers = false;
        if(TextUtils.isEmpty(this.term)) {
            view.clearSearch(true);
            return;
        }
        searchByTerm(term, withOffers);
    }

    @Override
    public void loadMore() {
        if(reachEnd || paging == DEFAULT_PAGE) return;
        if(executingUseCase == null && selectedCategory.equals("all")) {
            getByAllCategories(paging, false);
            return;
        }
        if(executingUseCase instanceof GetCityGuides) {
            getCityGuide(cityGuideIndex, paging);
        } else if(executingUseCase instanceof GetDinningList) {
            getDiningList(CityData.diningCode(), paging);
        } else if(executingUseCase instanceof GetAccommodationByCategories) {
//            getAccommodationData(selectedCategory, paging);
        } else if(executingUseCase instanceof AccommodationSearch) {
            searchByTerm(paging, term, withOffers, lastSelectedCity,selectedCategory);
        }
    }

    @Override
    public ExploreRView detailItem(ExploreRView.ItemType type, int index) {
        /*if(executingUseCase == null && !this.selectedCategory.equals("all")) return null;*/
        if(type == ExploreRView.ItemType.CITY_GUIDE) {
            return getCityGuides.getItemView(index);
        } else if(type == ExploreRView.ItemType.DINING) {
            return getDinningList.getItemView(index);
        } else if(type == ExploreRView.ItemType.SEARCH) {
            return accommodationSearch.getItemView(index);
        } else {
            return getAccommodationByCategories.getItemView(index);
        }
    }

    /**
     * city guide API
     * @param cityGuideCategoryIndex cityGuideCategoryIndex
     */
    private void getCityGuide(int cityGuideCategoryIndex, int page) {
        preExecute(page);
        executingUseCase = getCityGuides;

        int code = CityData.specificCityGuideCode(cityGuideCategoryIndex);
        if(code == -1) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.emptyData(true);
            return;
        }
        Single<List<ExploreRViewItem>> executor = getCityGuides.buildUseCaseSingle(
                new GetCityGuides.Params(code).paging(page));
        executeAndMerge(executor);
    }

    /**
     * dining list API
     * @param condition selectedCategory ID
     */
    private void getDiningList(int condition, int page) {
        preExecute(page);
        executingUseCase = getDinningList;
        Single<List<ExploreRViewItem>> executor = getDinningList.buildUseCaseSingle(
                new GetDinningList.Params(condition, page)
        );
        executeAndMerge(executor);
    }

    private void getAccommodationData(String categoryName, int page) {
        String[] categories = categoryMapper.get(categoryName);
        if(categories == null) {
            view.emptyData(false);
            return;
        }
        getAccommodationData(categoryName, page, categories);
    }

    /**
     *
     * @param categories all, flowers... except dining
     */
    private void getAccommodationData(String catName, int page, String... categories) {
        preExecute(page);
        executingUseCase = getAccommodationByCategories;
        Single<List<ExploreRViewItem>> executor = getAccommodationByCategories.buildUseCaseSingle(
                new GetAccommodationByCategories.Params(
                        catName,
                        page,
                        inSearchingMode() ? new SearchConditions(term, withOffers) : null,
                        categories));
        executeAndMerge(executor);
    }

    private void getByAllCategories(int page, boolean skipPre) {
        if(!skipPre) {
            preExecute(page);
        }
        executingUseCase = null;
        List<Single<List<ExploreRViewItem>>> executors = new ArrayList<>();

        // accommodation
        if(page == 1) {
            Single<List<ExploreRViewItem>> getAccommodations =
                    getAccommodationByCategories.buildUseCaseSingle(
                            new GetAccommodationByCategories.Params(
                                    "all",
                                    page,
                                    inSearchingMode() ? new SearchConditions(term, withOffers) : null,
                                    categoryMapper.get("all"))
                    );
            executors.add(getAccommodations);
        }

        // dining
        int diningCode = CityData.citySelected() ? CityData.diningCode() : 0;
        Single<List<ExploreRViewItem>> getDining;
        if(diningCode > 0) {
            getDining = getDinningList.buildUseCaseSingle(
                    new GetDinningList.Params(diningCode, page)
            );
            executors.add(getDining);
        }

        if(executors.size() == 0) {
            view.hideLoading(paging > DEFAULT_PAGE);
            return;
        }

        // city guide
        /*if(page == 1) {
            Integer[] codes = CityData.allCityGuideCodes();
            if (codes.length > 0) {
                Single<List<ExploreRViewItem>> cityGuides = getCityGuides
                        .buildUseCaseSingle(new GetCityGuides.Params(codes));
                executors.add(cityGuides);
            }
        }*/

        Single<List<ExploreRViewItem>> mainExecutor = Single.zip(executors, objects -> {
            List<ExploreRViewItem> result = new ArrayList<>();
            for(Object obj : objects) {
                List<ExploreRViewItem> eachList = (List<ExploreRViewItem>) obj;
                result.addAll(eachList);
            }
            return result;
        });
        executeAndMerge(mainExecutor);
    }

    private void searchByTerm(int page, String term, boolean withOffers, String city, String category) {
        if(!preExecute(page)) {
            return;
        }
        executingUseCase = accommodationSearch;
        AccommodationSearch.Params params = new AccommodationSearch.Params(term, page, withOffers, city, selectedCategory.equals("dining"), category);
        disposables.add(accommodationSearch.buildUseCaseSingle(params)
                .zipWith(historyData, (searchResult, history) -> {
                    history.addAll(searchResult.exploreRViewItems);
                    if(selectedCategory.equals("dining")) history = DiningSorting.sort(history, diningSortingCriteria);
                    else Collections.sort(history);
                    return new AccommodationSearch.SearchResult(history, searchResult.reachEnd);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SearchObserver()));
    }

//    private void executeAndMerge(Single<List<ExploreRViewItem>> executor) {
//        disposables.add(executor.zipWith(historyData, (exploreRViewItems, history) -> {
//            if(exploreRViewItems.size() == 0) return exploreRViewItems;
//            history.addAll(exploreRViewItems);
//            Collections.sort(history);
//            return history;
//        })
//        .subscribeOn(Schedulers.io())
//        .observeOn(AndroidSchedulers.mainThread())
//        .subscribeWith(new AccommodationDataObserver()));
//    }


    private void executeAndMerge(Single<List<ExploreRViewItem>> executor) {
        disposables.add(executor.zipWith(historyData, (exploreRViewItems, history) -> {
            if(exploreRViewItems.size() == 0) return exploreRViewItems;
            history.addAll(exploreRViewItems);
            if(selectedCategory.equals("dining")) history = DiningSorting.sort(history, diningSortingCriteria);
            else Collections.sort(history);
            return history;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new AccommodationDataObserver()));
    }
//
//    private void preExecute(int page) {
//        if(page == DEFAULT_PAGE) {
//            view.emptyData(false);
//        }
//        historyData = Single.just(view.historyData());
//        paging = page;
//        view.showLoading(page > DEFAULT_PAGE);
//    }


    private boolean preExecute(int page) {
        if(!App.getInstance().hasNetworkConnection()) {
//            view.noInternetMessage();
            view.onUpdateFailed("");
            return false;
        }
        disposables.clear();
        if(page == DEFAULT_PAGE) {
//            view.emptyData();
            view.emptyData(false);
        }
        historyData = Single.just(view.historyData());
        paging = page;
        view.showLoading(page > DEFAULT_PAGE);
        return true;
    }

    private final class AccommodationDataObserver extends DisposableSingleObserver<List<ExploreRViewItem>> {

        @Override
        public void onSuccess(List<ExploreRViewItem> exploreRViewItems) {
            view.hideLoading(paging > DEFAULT_PAGE);
            if(exploreRViewItems.size()>0) {
                paging++;
            } else {
                if(inSearchingMode()) {
                    view.onSearchNoData();
                }
                reachEnd = true;
                return;
            }
            view.updateRecommendAdapter(exploreRViewItems);
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.onUpdateFailed(e.getMessage());
            dispose();
        }
    }

    private final class SearchObserver extends DisposableSingleObserver<AccommodationSearch.SearchResult> {

        private int pageCount = 1;

        @Override
        public void onSuccess(AccommodationSearch.SearchResult searchResult) {
            view.hideLoading(paging > DEFAULT_PAGE);
            int count = searchResult.exploreRViewItems.size();
            if(searchResult.reachEnd) {
                reachEnd = true;
            }
            if(searchResult.reachEnd && count == 0) {
                view.hideLoading(paging > DEFAULT_PAGE);
                view.onSearchNoData();
            } else {
                paging++;
                view.updateRecommendAdapter(searchResult.exploreRViewItems);
                if(count < pageCount*10) {
                    loadMore();
                } else {
                    pageCount++;
                }
            }
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.hideLoading(paging > DEFAULT_PAGE);
            view.onSearchNoData();
            dispose();
        }
    }

    private void buildCategoryMapper() {
        categoryMapper.put("all", new String[] {"Flowers", "Tickets", "VIP Travel Services",
                "Golf Merchandise", "Flowers", "Wine", "Specialty Travel"});
        categoryMapper.put("travel", new String[] {"VIP Travel Services"});
        categoryMapper.put("flowers", new String[] {"Flowers"});
        categoryMapper.put("shopping", new String[] {"Golf Merchandise", "Flowers", "Wine"});
        categoryMapper.put("sports", new String[] {"Tickets"});
        categoryMapper.put("arts + culture", new String[] {"Tickets"});
        categoryMapper.put("tours", new String[] {"VIP Travel Services"});
        categoryMapper.put("experiences", new String[] {"Specialty Travel"});
    }
}