package com.mastercard.androidapp.domain.mapper;

/**
 * Created by tung.phan on 5/17/2017.
 */
public class MappingExploreRViewItem {

//    public static final SparseArray<String> categoryNameMapper;
//
//    static {
//        categoryNameMapper = new SparseArray<>();
//        categoryNameMapper.put(934,"Flowers");
//        categoryNameMapper.put(15,"Flowers");
//
//        categoryNameMapper.put(27,"Shopping");
//        categoryNameMapper.put(1247,"Shopping");
//        categoryNameMapper.put(14,"Shopping");
//
//        categoryNameMapper.put(28,"Sports");
//
//        categoryNameMapper.put(1250,"Travel");
//        categoryNameMapper.put(1246,"Travel");
//        categoryNameMapper.put(13,"Travel");
//        categoryNameMapper.put(23,"Travel");
//
//        categoryNameMapper.put(1023,"Experiences");
//        categoryNameMapper.put(1138,"Experiences");
//        categoryNameMapper.put(1139,"Experiences");
//        categoryNameMapper.put(1244,"Experiences");
//        categoryNameMapper.put(1245,"Experiences");
//    }
//
//    /**
//     * transform Answer (from B2C API) to ExploreRViewItem
//     */
//    public static List<ExploreRViewItem> transformQandA(int startIndex, List<QuestionsAndAnswer> dataList) {
//        final List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
//        int length = dataList.size();
//        for (int i = 0; i < length; i++) {
//            QuestionsAndAnswer qa = dataList.get(i);
//            final List<ExploreRViewItem> exploreRViewItem = transformQandA(startIndex + i, qa);
//            exploreRViewList.addAll(exploreRViewItem);
//        }
//        return exploreRViewList;
//    }
//
//    private static List<ExploreRViewItem> transformQandA(int dataIndex, QuestionsAndAnswer questionsAndAnswers) {
//        List<ExploreRViewItem> exploreRViewItemList = new ArrayList<>();
//        for(int index = 0; index < questionsAndAnswers.getAnswers().size(); index ++) {
//            Answer answer = questionsAndAnswers.getAnswers().get(index);
//            ExploreRViewItem exploreRViewItem;
//
//            String insiderTip = "";
//            String insiderTipSeg = "<b>Insider tip</b>:";
//            if (answer.getAnswerText().contains(insiderTipSeg)) {
//                int insiderTipInd = answer.getAnswerText().indexOf(insiderTipSeg);
//                insiderTip = answer.getAnswerText().substring(insiderTipInd + insiderTipSeg.length()).trim().replace("\n", "").replace("\r", "");
//            }
//
//            exploreRViewItem = new ExploreRViewItem(
//                    questionsAndAnswers.getID(),
//                    answer.getName(),
//                    answer.getAddress3(),
//                    null,
//                    false,
//                    insiderTip,
//                    dataIndex,
//                    index
//            );
//            exploreRViewItem.setItemType(ExploreRViewItem.ItemType.CITY_GUIDE);
//            exploreRViewItemList.add(exploreRViewItem);
//        }
//        return exploreRViewItemList;
//    }
//
//    public static List<ExploreRViewItem> transformDining(int startIndex, List<QuestionsAndAnswer> dataList) {
//        final List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
//        int length = dataList.size();
//        for (int i = 0; i < length; i++) {
//            QuestionsAndAnswer qa = dataList.get(i);
//            final List<ExploreRViewItem> exploreRViewItem = transformDining(startIndex + i, qa);
//            exploreRViewList.addAll(exploreRViewItem);
//        }
//        return exploreRViewList;
//    }

//    private static List<ExploreRViewItem> transformDining(int dataIndex, QuestionsAndAnswer questionsAndAnswers) {
//        List<ExploreRViewItem> exploreRViewItemList = new ArrayList<>();
//        for(int index = 0; index < questionsAndAnswers.getAnswers().size(); index ++) {
//            Answer answer = questionsAndAnswers.getAnswers().get(index);
//            ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
//                    questionsAndAnswers.getID(),
//                    answer.getName(),
//                    answer.getAddress3(),
//                    answer.getImageURL(),
//                    !TextUtils.isEmpty(answer.getOffer2()),
//                    answer.getOffer2(),
//                    dataIndex,
//                    index
//            );
//            exploreRViewItem.setItemType(ExploreRViewItem.ItemType.DINING);
//            exploreRViewItem.categoryName("dining");
//
//            exploreRViewItemList.add(exploreRViewItem);
//        }
//        return exploreRViewItemList;
//    }

//    public static List<ExploreRViewItem> transformTiles(int startIndex, List<Tiles> dataList) {
//        List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
//        int length = dataList.size();
//        for (int i = 0; i < length; i++) {
//            Tiles tiles = dataList.get(i);
//            exploreRViewList.add(transformTiles(i + startIndex, tiles));
//        }
//        return exploreRViewList;
//    }

//    private static ExploreRViewItem transformTiles(int dataIndex, Tiles tiles) {
//        String categoryName = categoryNameMapper.get(tiles.getID());
//        ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
//                tiles.getID(),
//                tiles.getTitle(),
//                "experiences".equalsIgnoreCase(categoryName) ? null : "Multiple Cities",
//                tiles.getImage(),
//                true,
//                tiles.getText(),
//                dataIndex,
//                0
//        );
//        exploreRViewItem.categoryName(categoryName);
//        return exploreRViewItem;
//    }

//    public static List<ExploreRViewItem> transformSearch(int startIndex, List<SearchContent> dataList) {
//        final List<ExploreRViewItem> exploreRViewList = new ArrayList<>();
//        int length = dataList.size();
//        for (int i = 0; i < length; i++) {
//            SearchContent content = dataList.get(i);
//            final ExploreRViewItem exploreRViewItem = transformSearchContent(startIndex + i, content);
//            exploreRViewList.add(exploreRViewItem);
//        }
//        return exploreRViewList;
//    }

//    private static ExploreRViewItem transformSearchContent(int dataIndex, SearchContent content) {
//        String categoryName = categoryNameMapper.get(content.ID());
//        String address;
//        if(content.product().equals("CCA")) {
//            address = "experiences".equalsIgnoreCase(categoryName) ? null : "Multiple Cities";
//        } else {
//            address = content.address();
//        }
//        ExploreRViewItem exploreRViewItem = new ExploreRViewItem(
//                content.ID(),
//                content.title(),
//                address,
//                null,
//                content.hasOffer(),
//                content.description(),
//                dataIndex,
//                0
//        );
//        exploreRViewItem.setItemType(ExploreRView.ItemType.SEARCH);
//        return exploreRViewItem;
//    }
}
