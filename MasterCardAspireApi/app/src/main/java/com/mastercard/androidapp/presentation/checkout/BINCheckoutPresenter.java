package com.mastercard.androidapp.presentation.checkout;

import android.content.Context;
import android.text.TextUtils;

import com.api.aspire.common.constant.ConstantAspireApi;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.domain.mapper.profile.ProfileLogicCore;
import com.api.aspire.domain.model.ProfileAspire;
import com.api.aspire.domain.usecases.GetAccessToken;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.LoadProfile;
import com.api.aspire.domain.usecases.SaveProfile;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.api.aspire.common.constant.ErrCode;
import com.mastercard.androidapp.datalayer.entity.GetClientCopyResult;
import com.mastercard.androidapp.datalayer.repository.B2CDataRepository;
import com.mastercard.androidapp.domain.model.ProfileMetadata;
import com.mastercard.androidapp.domain.repository.B2CRepository;
import com.mastercard.androidapp.domain.usecases.CheckBIN;
import com.mastercard.androidapp.domain.usecases.GetMasterCardCopy;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by vinh.trinh on 4/26/2017.
 */

public class BINCheckoutPresenter implements BINCodeCheckout.Presenter {

    private CompositeDisposable compositeDisposable;
    private BINCodeCheckout.View view;
    private PreferencesStorageAspire prefStorage;
    private GetMasterCardCopy getPrivacyPolicy;
    private CheckBIN checkBIN;
    private LoadProfile loadProfile;
    private SaveProfile saveProfileCase;
    private GetAccessToken getAccessToken;

    //-- variable
    private ProfileAspire profileAspire;

    BINCheckoutPresenter(Context c) {
        compositeDisposable = new CompositeDisposable();
        MapProfileApp mapLogic = new MapProfileApp();
        B2CRepository repository = new B2CDataRepository();
        prefStorage = new PreferencesStorageAspire(c);
        this.getPrivacyPolicy = new GetMasterCardCopy(repository);
        GetToken getToken = new GetToken(prefStorage, mapLogic);
        this.checkBIN = new CheckBIN(new AuthDataAspireRepository(),
                mapLogic,
                getToken
        );
        ProfileDataAspireRepository profileRepository = new ProfileDataAspireRepository(prefStorage, mapLogic);
        this.loadProfile = new LoadProfile(profileRepository);
        this.saveProfileCase = new SaveProfile(profileRepository);
        this.getAccessToken = new GetAccessToken(loadProfile, getToken);
    }

    @Override
    public void attach(BINCodeCheckout.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void checkoutBINCode(final String input) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();
        compositeDisposable.add(
                binValidationExecutor(input)
                        .flatMap(valid -> {
                            if (valid)
                                return getPrivacyPolicy.buildUseCaseSingle(AppConstant.MASTERCARD_COPY_UTILITY.Privacy.getValue());
                            else
                                return Single.error(new Exception(ErrCode.PASS_CODE_ERROR.name()));
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<GetClientCopyResult>() {

                            @Override
                            public void onSuccess(@NonNull GetClientCopyResult result) {
                                boolean profileCreated = prefStorage.profileCreated();

                                //sign in case
                                if (profileAspire != null) {
                                    profileCreated = true;
                                }

                                String policyVersion = result.getCurrentVersion();
                                if (!TextUtils.isEmpty(policyVersion)) {
                                    //sign in case
                                    if (profileAspire != null) {

                                        //<editor-fold desc="Sign In Case">
                                        updateBinCodeAndPolicyVersion(input, policyVersion, profileAspire)
                                                .subscribeOn(Schedulers.io())
                                                .observeOn(AndroidSchedulers.mainThread())
                                                .subscribe(new DisposableCompletableObserver() {
                                                    @Override
                                                    public void onComplete() {
                                                        if(view != null){
                                                            view.dismissProgressDialog();
                                                            view.proceedToHome();
                                                        }
                                                    }

                                                    @Override
                                                    public void onError(Throwable e) {
                                                        if(view != null){
                                                            view.dismissProgressDialog();
                                                            view.showErrorDialog(ErrCode.UNKNOWN_ERROR, "");
                                                        }
                                                    }
                                                });
                                        //</editor-fold>

                                    } else {
                                        //splash screen -- auto login case
                                        if (profileCreated) {
                                            // Get profile local update binCode and policy version, Update profile server
                                            //<editor-fold desc="Splash Screen Case">
                                            loadProfile.loadStorage()
                                                    .flatMapCompletable(profileAspireLocal ->
                                                            updateBinCodeAndPolicyVersion(input,
                                                                    policyVersion,
                                                                    profileAspireLocal)
                                                    )
                                                    .subscribeOn(Schedulers.io())
                                                    .observeOn(AndroidSchedulers.mainThread())
                                                    .subscribe(new DisposableCompletableObserver() {
                                                        @Override
                                                        public void onComplete() {
                                                            if(view != null){
                                                                view.dismissProgressDialog();
                                                                view.proceedToHome();
                                                            }
                                                        }

                                                        @Override
                                                        public void onError(Throwable e) {
                                                            if(view != null){
                                                                view.dismissProgressDialog();
                                                                view.showErrorDialog(ErrCode.UNKNOWN_ERROR, "");
                                                            }
                                                        }
                                                    });
                                            //</editor-fold>
                                        } else {
                                            //flow from register
                                            view.dismissProgressDialog();
                                            view.proceedToSignUp(new ProfileMetadata(null, null, null, policyVersion, null,
                                                    null, input, null));
                                        }
                                    }
                                } else {
                                    view.dismissProgressDialog();
                                    view.showInvalidBINCodeDialog(profileCreated,
                                            input.equalsIgnoreCase(prefStorage.getBinCode()));
                                }
                            }

                            @Override
                            public void onError(@NonNull Throwable e) {
                                view.dismissProgressDialog();
                                if (ErrCode.PASS_CODE_ERROR.name().equalsIgnoreCase(e.getMessage())) {
                                    view.showInvalidBINCodeDialog(prefStorage.profileCreated(),
                                            input.equalsIgnoreCase(prefStorage.getBinCode()));
                                } else {
                                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR, e.getMessage());
                                }
                            }
                        })
        );
    }

    @Override
    public void checkoutBINCode(String input, ProfileAspire profileAspire) {
        this.profileAspire = profileAspire;
        checkoutBINCode(input);
    }

    @Override
    public void loadPrivacyContent() {
        compositeDisposable.add(
                getPrivacyPolicy.param(AppConstant.MASTERCARD_COPY_UTILITY.Privacy.getValue())
                        .on(Schedulers.io(), AndroidSchedulers.mainThread())
                        .execute(new DisposableSingleObserver<GetClientCopyResult>() {
                            @Override
                            public void onSuccess(GetClientCopyResult getMasterCardCopyResult) {
                                view.privacyContentLoaded(getMasterCardCopyResult.getText());
                                privacyPolicyVersion = getMasterCardCopyResult.getCurrentVersion();
                            }

                            @Override
                            public void onError(Throwable e) {
                                view.privacyContentLoaded("");
                            }
                        })
        );
    }

    @Override
    public void onPolicyAccepted() {
        //-- flow from splash screen
        view.showProgressDialog();
        compositeDisposable.add(
                loadProfile.loadStorage()
                        .flatMapCompletable(profileAspire -> {

                            ProfileLogicCore.setAppUserPreference(
                                    profileAspire.getAppUserPreferences(),
                                    ConstantAspireApi.APP_USER_PREFERENCES.POLICY_VERSION_KEY,
                                    privacyPolicyVersion);

                            //need update variable privacyPolicyVersion. [change flow]
                            return updateProfileRemote(profileAspire);
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new PolicyAcceptedObserver())
        );
    }

    private final class PolicyAcceptedObserver extends DisposableCompletableObserver {

        @Override
        public void onComplete() {
            view.dismissProgressDialog();
            view.proceedToHome();
            dispose();
        }

        @Override
        public void onError(Throwable e) {
            view.dismissProgressDialog();
            view.showErrorDialog(null, e.getMessage());
            dispose();
        }
    }


    @Override
    public void abort() {
        compositeDisposable.clear();
    }

    private Single<Boolean> binValidationExecutor(String input) {
        return checkBIN.buildUseCaseSingle(input);
    }

    private String privacyPolicyVersion;

    @Override
    public void onPolicyAcceptedFromSignIn(ProfileAspire profileAspire) {
        view.showProgressDialog();
        compositeDisposable.add(
                updateProfileAspirePolicy(profileAspire)
                        .flatMapCompletable(this::updateProfileRemote)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new PolicyAcceptedObserver())
        );
    }

    private Single<ProfileAspire> updateProfileAspirePolicy(ProfileAspire profileAspire) {
        return Single.create(e -> {
            ProfileLogicCore.setAppUserPreference(profileAspire.getAppUserPreferences(),
                    ConstantAspireApi.APP_USER_PREFERENCES.POLICY_VERSION_KEY,
                    privacyPolicyVersion);
            e.onSuccess(profileAspire);
        });
    }

    private Completable updateBinCodeAndPolicyVersion(final String binCode,
                                                      final String policyVersion,
                                                      final ProfileAspire profileAspire) {
        return Completable.create(e -> {

            //binCode
            ProfileLogicCore.setAppUserPreference(profileAspire.getAppUserPreferences(),
                    ConstantAspireApi.APP_USER_PREFERENCES.BIN_CODE_KEY, binCode);

            //policy version
            ProfileLogicCore.setAppUserPreference(profileAspire.getAppUserPreferences(),
                    ConstantAspireApi.APP_USER_PREFERENCES.POLICY_VERSION_KEY, policyVersion);

            prefStorage.editor().binCode(binCode).build().save();
            e.onComplete();
        }).andThen(updateProfileRemote(profileAspire));
    }

    private Completable updateProfileRemote(ProfileAspire profileAspire){
        return getAccessToken.execute(profileAspire)
                .flatMapCompletable(accessToken -> saveProfileCase.buildUseCaseCompletable(new SaveProfile.Params(profileAspire, accessToken)));
    }
}
