package com.mastercard.androidapp.presentation.request;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;

import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.presentation.base.BaseFragment;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.support.mylibrary.widget.ClickGuard;
import com.support.mylibrary.widget.FontCheckbox;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by vinh.trinh on 5/24/2017.
 */

public class PlaceRequestFragment extends BaseFragment implements View.OnTouchListener, ViewTreeObserver.OnGlobalLayoutListener {

    private static final String STATE_RECORD_TEXT = "text";

    @BindView(R.id.edt_ask_content)
    AppCompatEditText edtAskContent;
    @BindView(R.id.btn_record)
    ImageButton btnRecord;
    @BindView(R.id.layout_container)
    View layoutContainer;
    @BindDrawable(R.drawable.ic_microphone)
    Drawable icRecord;
    @BindDrawable(R.drawable.ic_send)
    Drawable icSend;
    @BindView(R.id.chb_reply_phone)
    FontCheckbox checkBoxPhone;
    @BindView(R.id.chb_reply_email)
    FontCheckbox checkBoxEmail;
    @BindView(R.id.reply_mode_container)
    ViewGroup replyModeContainer;
    AnimatorSet slideUp, slideDown;
    boolean checkboxShowing;
    private Spanned hintText;
    private boolean hasFocus;
    ViewEventsListener listener;
    DialogHelper dialogHelper;

    public static PlaceRequestFragment newInstance() {
        Bundle args = new Bundle();
        PlaceRequestFragment fragment = new PlaceRequestFragment();
        fragment.setArguments(args);
        return fragment;
    }
    public static PlaceRequestFragment newInstance(Bundle bundle) {
        PlaceRequestFragment fragment = new PlaceRequestFragment();
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ViewEventsListener) {
            listener = (ViewEventsListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement PlaceRequestFragment.ViewEventsListener.");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ask_concierge, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dialogHelper = new DialogHelper((Activity) this.getContext());
        icRecord = ContextCompat.getDrawable(getContext(), R.drawable.ic_microphone);
        icSend = ContextCompat.getDrawable(getContext(), R.drawable.ic_send);
        hintText = Html.fromHtml(getString(R.string.ask_hint));
        edtAskContent.setHint(hintText);
        if(edtAskContent.getText().length()>0)
            replyModeContainer.setVisibility(View.VISIBLE);
        edtAskContent.setOnTouchListener((view1, motionEvent) -> {
            view1.performClick();
            handleEditTextFocus(true);
            return false;
        });
        edtAskContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                String s = edtAskContent.getText().toString().trim();
                if(s.length() == 0){
                    btnRecord.setVisibility(View.INVISIBLE);
                    replyModeContainer.setVisibility(View.INVISIBLE);

                }else if( s.length() > 0) {
                        btnRecord.setVisibility(View.VISIBLE);
                        btnRecord.setImageDrawable(icSend);
                        replyModeContainer.setVisibility(View.VISIBLE);
                }

            }
        });
        layoutContainer.setOnTouchListener(this);

        if(getArguments() != null){
            String suggestedConcierge = getArguments().getString(IntentConstant.SUGGESTED_CONCIERGE);
            if(!TextUtils.isEmpty(suggestedConcierge)){
                edtAskContent.setText(Html.fromHtml(suggestedConcierge + "<br/>"));
            }
            boolean invokeKeyboard = getArguments().getBoolean(IntentConstant.INVOKE_KEYBOARD, false);
            if(invokeKeyboard){
                edtAskContent.post(() -> {
                    edtAskContent.requestFocus();
                    edtAskContent.setHint("");
                    edtAskContent.setCursorVisible(true);
                    InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imgr.showSoftInput(edtAskContent, InputMethodManager.SHOW_IMPLICIT);
                });

            }
        }
        setupCheckboxAnimation();
        ClickGuard.guard(btnRecord);
    }

    @Override
    public void onResume() {
        handleEditTextFocus(false);
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_RECORD_TEXT, edtAskContent.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @OnClick(R.id.btn_record)
    public void recordOrSend(View view) {
        String text = edtAskContent.getText().toString().trim();
        if(text.length() > 0)
            if(!checkBoxPhone.isChecked() && !checkBoxEmail.isChecked()){
               dialogHelper.alert(null, getString(R.string.choose_at_lease), new DialogInterface.OnDismissListener() {
                   @Override
                   public void onDismiss(DialogInterface dialogInterface) {
                       edtAskContent.postDelayed(new Runnable() {
                           @Override
                           public void run() {
                               edtAskContent.requestFocus();
                               edtAskContent.setCursorVisible(true);
                               InputMethodManager imgr = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                               imgr.showSoftInput(edtAskContent, InputMethodManager.SHOW_IMPLICIT);
                           }
                       }, 200);
                   }
               });
            }
            if(checkBoxPhone.isChecked() || checkBoxEmail.isChecked()){
                listener.sendRequest(edtAskContent.getText().toString(), checkBoxEmail.isChecked(),
                        checkBoxPhone.isChecked());
                checkBoxPhone.isChecked();
                checkBoxEmail.isChecked();

                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edtAskContent.getWindowToken(), 0);
            }

        if(text.length()  == 0 && !hasFocus) {
            listener.record();
        }
    }

    @OnClick(R.id.btn_call)
    public void callTheConcierge(View view) {
        listener.makePhoneCall(getString(R.string.phone_number_display_on_call));
    }

    @OnClick(R.id.tv_call_phone)
    public void internationalCall(View view) {
        listener.makePhoneCall(getString(R.string.call_international_number_display_on_call));
    }

    void speechToTextResult(String text) {
        edtAskContent.setText(text);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        view.performClick();
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            Rect outRect = new Rect();
            edtAskContent.getGlobalVisibleRect(outRect);
            if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                handleEditTextFocus(false);
            }
        }
        return false;
    }

    private void handleEditTextFocus(boolean hasFocus) {
        if(hasFocus) {
            if(edtAskContent.getText().toString().trim().length() > 0) {
                btnRecord.setVisibility(View.VISIBLE);
                btnRecord.setImageDrawable(icSend);
            } else {
                btnRecord.setVisibility(View.INVISIBLE);
            }
            edtAskContent.setHint("");
            edtAskContent.setCursorVisible(true);
            edtAskContent.requestFocus();
        } else {
            if(edtAskContent.getText().toString().trim().length() == 0) {
                if(edtAskContent.getText().length() == 0){
                    btnRecord.setVisibility(View.VISIBLE);
                    btnRecord.setImageDrawable(icRecord);
                }else{
                    btnRecord.setVisibility(View.INVISIBLE);
                }

            }else{
                btnRecord.setVisibility(View.VISIBLE);
                btnRecord.setImageDrawable(icSend);
            }
            edtAskContent.setHint(hintText);
            edtAskContent.setCursorVisible(false);
            InputMethodManager imm = (InputMethodManager) edtAskContent.getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(edtAskContent.getWindowToken(), 0);
        }
        this.hasFocus = hasFocus;
    }

    private void setupCheckboxAnimation() {
        replyModeContainer.getViewTreeObserver().addOnGlobalLayoutListener(this);
        edtAskContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                boolean hasText = s.length() > 0;
                btnRecord.setImageDrawable(hasText ? icSend : icRecord);
                if (hasText && !checkboxShowing) {
                    slideDown.start();
                    checkboxShowing = true;
                    return;
                }
                if (!hasText && checkboxShowing) {
                    slideUp.start();
                    checkboxShowing = false;
                }
            }
        });
    }

    @Override
    public void onGlobalLayout() {
        slideUp = checkBoxAnimation(true);
        slideDown = checkBoxAnimation(false);
        checkboxShowing = edtAskContent.getText().length() > 0;
        if (!checkboxShowing) {
            slideUp.start();
        }
        replyModeContainer.getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    private AnimatorSet checkBoxAnimation(boolean up) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator move = ObjectAnimator
                .ofFloat(replyModeContainer, "translationY", up ? -replyModeContainer.getHeight() : 0);
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            set.play(move);
        } else {
            ObjectAnimator fade = ObjectAnimator
                    .ofFloat(replyModeContainer, "alpha", up ? 0 : 1);
            set.playTogether(move, fade);
        }
        set.setDuration(300);
        return set;
    }

    interface ViewEventsListener {
        void record();
        void sendRequest(String content, boolean email, boolean phone);
        void makePhoneCall(String number);
    }
}
