package com.mastercard.androidapp.datalayer.entity.askconcierge;

import com.google.gson.annotations.SerializedName;
import com.mastercard.androidapp.BuildConfig;
import com.mastercard.androidapp.domain.model.Profile;

/**
 * Created by vinh.trinh on 5/16/2017.
 */

public class ACRequest {

    @SerializedName("newCaseRequest")
    NewCaseRequest body;

    public ACRequest(Profile profile, String memberID, String requestDetails, String city, String type) {

        String firstName = profile.getFirstName();
        String lastName = profile.getLastName();
        String emailAddress = profile.getEmail();
        String phoneNumber = profile.getPhone();
        body = new NewCaseRequest(memberID, firstName, lastName, emailAddress,
                phoneNumber, city, type, requestDetails);
    }

    class NewCaseRequest {
        @SerializedName("accessToken")
        AccessToken accessToken;
        @SerializedName("memberId")
        String memberID;
        @SerializedName("firstName")
        String firstName;
        @SerializedName("lastName")
        String lastName;
        @SerializedName("emailAddress")
        String emailAddress;
        @SerializedName("phoneNumber")
        String phoneNumber;
        @SerializedName("requestCity")
        String requestCity;
        @SerializedName("requestType")
        String requestType;
        @SerializedName("requestDetails")
        String requestDetails;

        NewCaseRequest(String memberID,  String firstName, String lastName,
                              String emailAddress, String phoneNumber, String requestCity,
                              String requestType, String requestDetails) {
            this.memberID = memberID;
            this.firstName = firstName;
            this.lastName = lastName;
            this.emailAddress = emailAddress;
            this.phoneNumber = phoneNumber;
            this.requestCity = requestCity;
            this.requestType = requestType;
            this.requestDetails = requestDetails;
            this.accessToken = new AccessToken();
        }
    }

    class AccessToken {
        @SerializedName("applicationName")
        String appName;
        @SerializedName("programPassword")
        String secretKey;
        @SerializedName("licenseKey")
        LicenseKey licenseKey;
        AccessToken() {
            appName = BuildConfig.WS_AC_APP_NAME;
            secretKey = BuildConfig.WS_AC_SECRET;
            licenseKey = new LicenseKey(BuildConfig.WS_AC_KEY);
        }
    }

    class LicenseKey {
        @SerializedName("key")
        String key;
        LicenseKey(String key) {
            this.key = key;
        }
    }
}
