package com.mastercard.androidapp.presentation.explore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.common.constant.AppConstant;
import com.mastercard.androidapp.common.constant.CityData;
import com.mastercard.androidapp.common.constant.CityGuide;
import com.mastercard.androidapp.common.constant.IntentConstant;
import com.mastercard.androidapp.common.constant.RequestCode;
import com.mastercard.androidapp.common.constant.ResultCode;
import com.mastercard.androidapp.datalayer.repository.B2CDataRepository;
import com.mastercard.androidapp.domain.model.explore.CityGuideDetailItem;
import com.mastercard.androidapp.domain.model.explore.ExploreRView;
import com.mastercard.androidapp.domain.model.explore.ExploreRViewItem;
import com.mastercard.androidapp.domain.usecases.AccommodationSearch;
import com.mastercard.androidapp.domain.usecases.GetAccommodationByCategories;
import com.mastercard.androidapp.domain.usecases.GetCityGuides;
import com.mastercard.androidapp.domain.usecases.GetDinningList;
import com.mastercard.androidapp.presentation.base.BaseFragment;
import com.mastercard.androidapp.presentation.cityguidecategory.CityGuideCategoriesActivity;
import com.mastercard.androidapp.presentation.home.HomeActivity;
import com.mastercard.androidapp.presentation.search.SearchActivity;
import com.mastercard.androidapp.presentation.selectcategory.CategoryPresenter;
import com.mastercard.androidapp.presentation.selectcategory.SelectCategoryActivity;
import com.mastercard.androidapp.presentation.selectcity.SelectCityActivity;
import com.mastercard.androidapp.presentation.venuedetail.CityGuideDetailActivity;
import com.mastercard.androidapp.presentation.venuedetail.DiningDetailActivity;
import com.mastercard.androidapp.presentation.venuedetail.OtherExploreDetailActivity;
import com.mastercard.androidapp.presentation.widget.DialogHelper;
import com.mastercard.androidapp.presentation.widget.ListItemDecoration;
import com.mastercard.androidapp.presentation.widget.ProgressDialogUtil;
import com.mastercard.androidapp.presentation.widget.RecyclerViewScrollListener;
import com.mastercard.androidapp.presentation.widget.ViewUtils;
import com.support.mylibrary.common.Utils;
import com.support.mylibrary.widget.ClickGuard;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class ExploreFragment extends BaseFragment implements Explore.View,
        ExploreRViewAdapter.ExploreRViewAdapterListener {

    private ExplorePresenter presenter;
    @BindView(R.id.recommend_view)
    RecyclerView recommendView;
    @BindView(R.id.location_btn)
    Button btnLocation;
    @BindView(R.id.category_btn)
    Button btnCategory;
    @BindView(R.id.search_btn)
    Button btnSearch;
    @BindView(R.id.search_box)
    FrameLayout searchBox;
    @BindView(R.id.edt_search)
    EditText edtSearch;
    @BindView(R.id.offer_wrapper)
    LinearLayout withOffersWrapper;
    @BindView(R.id.tv_no_data)
    TextView tvNoData;
    @BindView(R.id.btn_ask)
    Button btnAskConcierge;
    @BindView(R.id.loading)
    ProgressBar loadingProgressBar;
    private int lastSelected = -1;
    private String selectedSubCategoryName;
    private int selectedSubCategoryResId;

    private ExploreRViewAdapter exploreRViewAdapter;
    private ListItemDecoration dividerItemDecoration;

    private boolean shouldReload = false;

    public static ExploreFragment newInstance() {
        Bundle args = new Bundle();
        ExploreFragment fragment = new ExploreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ExplorePresenter buildPresenter() {
        B2CDataRepository b2CDataRepository = new B2CDataRepository();
        GetAccommodationByCategories other = new GetAccommodationByCategories(b2CDataRepository);
        GetCityGuides cityGuides = new GetCityGuides(b2CDataRepository);
        GetDinningList dinningList = new GetDinningList(b2CDataRepository);
        AccommodationSearch search = new AccommodationSearch(b2CDataRepository);
        return new ExplorePresenter(other, dinningList, cityGuides, search);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = buildPresenter();
        presenter.attach(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_explore, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recommendView.setLayoutManager(layoutManager);
        dividerItemDecoration = new ListItemDecoration(recommendView.getContext());
        recommendView.addItemDecoration(dividerItemDecoration);
        exploreRViewAdapter = new ExploreRViewAdapter(getContext(), new ArrayList<>(), this);
        recommendView.setAdapter(exploreRViewAdapter);
        dividerItemDecoration.setAdapterItemCount(0);
        ViewUtils.drawableTop(btnLocation, R.drawable.ic_location);
        ViewUtils.drawableTop(btnCategory, R.drawable.ic_category);
        ViewUtils.drawableTop(btnSearch, R.drawable.ic_search);
        setupLoadMore();
        edtSearch.setOnTouchListener((v, event) -> {
            if(event.getAction() == MotionEvent.ACTION_UP) {
                if(event.getRawX() >= (edtSearch.getRight() - edtSearch.getCompoundDrawables()[2].getBounds().width())) {
                    // your action here
                    clearSearch(true);
                    return true;
                }else{
                    // Search with the given keyword
                    searchWithGivenKeyword();
                    return true;
                }
            }
            return false;
        });
        if(savedInstanceState == null){
            // Track GA
            App.getInstance().track(AppConstant.GA_TRACKING_SCREEN_NAME.EXPLORE.getValue());
        }
        ClickGuard.guard(btnLocation, btnCategory, btnSearch);
        if(CityData.citySelected()) {
            handleSelectedCityResult();
        }
    }

    @OnClick(R.id.location_btn)
    public void clickBtnLocation(View view) {
        Intent intent = new Intent(getActivity(), SelectCityActivity.class);
        startActivityForResult(intent, RequestCode.SELECT_CITY);
    }

    @OnClick(R.id.category_btn)
    public void clickBtnCategory(View view) {
        Intent intent = new Intent(getContext(), SelectCategoryActivity.class);
        startActivityForResult(intent, RequestCode.SELECT_CATEGORY);
    }

    @OnClick(R.id.search_btn)
    public void clickBtnSearch(Button btnSearch) {
        //handle case btn Discover more pressed
        if (btnSearch.getText().toString()
                .equalsIgnoreCase(getResources().getString(R.string.text_discover_more))) {
            int cityId = CityData.guideCode();
            if(cityId > 0) {
                //start sub category select
                Intent intent = new Intent(getContext(), CityGuideCategoriesActivity.class);
                intent.putExtra(IntentConstant.CATEGORY_ID, cityId);
                startActivityForResult(intent, RequestCode.SELECT_SUB_CATEGORY);
            }
        } else {
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            startActivityForResult(intent, RequestCode.SEARCH_INPUT);
        }
    }

    @OnClick(R.id.btn_clear_offers)
    public void clearOffers() {
        presenter.offHasOffers();
        withOffersWrapper.setVisibility(View.GONE);
    }

    @OnClick(R.id.btn_ask)
    public void clickAskConcierge(View view) {
        ((HomeActivity) getActivity()).navigateToAskConcierge();
    }

    @Override
    public void onDestroy() {
        presenter.detach();
        super.onDestroy();
    }

    @Override
    public void hideLoading(boolean more) {
        if(more) {
            exploreRViewAdapter.showLoading(false);
        } else {
            //loadingProgressBar.setVisibility(View.GONE);
            ProgressDialogUtil.hideLoading();
        }
        scrollListener.loadDone();
    }

    @Override
    public void showLoading(boolean more) {
        if(more) {
            exploreRViewAdapter.showLoading(true);
        } else {
            //loadingProgressBar.setVisibility(View.VISIBLE);
            ProgressDialogUtil.showLoading(getActivity());
        }
    }

    @Override
    public void updateRecommendAdapter(List<ExploreRViewItem> data) {
        tvNoData.setVisibility(View.GONE);
        btnAskConcierge.setVisibility(View.GONE);
        exploreRViewAdapter.setSearchAction(presenter.inSearchingMode());
        exploreRViewAdapter.swapData(data);
        dividerItemDecoration.setAdapterItemCount(exploreRViewAdapter.getItemCount());
    }

    @Override
    public void onUpdateFailed(String message) {
        //((HomeActivity)getActivity()).alertDialog(message);
        if(getActivity() != null) {
            if (App.getInstance().hasNetworkConnection()) {
                new DialogHelper(getActivity()).showTimeoutError();
            } else {
                new DialogHelper(getActivity()).showNoInternetAlert();
            }
        }
    }

    @Override
    public void emptyData(boolean cityNotInCityGuide) {
        exploreRViewAdapter.clear();
        if(cityNotInCityGuide){
            //new DialogHelper(getActivity()).alert("", getString(R.string.city_not_in_city_guide_error));
            tvNoData.setText(R.string.empty_search_result_notify);
            tvNoData.setVisibility(View.VISIBLE);
            btnAskConcierge.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onSearchNoData() {
        if(getActivity() != null) {
            if (App.getInstance().hasNetworkConnection()) {
                if(exploreRViewAdapter.isEmpty()) {
                    tvNoData.setText(R.string.empty_search_result_notify);
                    tvNoData.setVisibility(View.VISIBLE);
                    btnAskConcierge.setVisibility(View.VISIBLE);
                }
            } else {
                new DialogHelper(getActivity()).showNoInternetAlert();
            }
        }
    }

    @Override
    public List<ExploreRViewItem> historyData() {
        return exploreRViewAdapter.getData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == RequestCode.SELECT_CITY && resultCode == ResultCode.RESULT_OK) {
            handleSelectedCityResult();
        } else if (requestCode == RequestCode.SELECT_CATEGORY) {
            handleSelectCategoryResult(resultCode, data);
        } else if (requestCode == RequestCode.SELECT_SUB_CATEGORY) {
            btnCategory.setText(getString(R.string.city_guide));
            handleSubCategorySelected(data);
        } else if (requestCode == RequestCode.SEARCH_INPUT) {
            handleSearchInputResult(resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Reset explore to Category "All"
     */
    public void makeExploreDefault(){
        Intent intent = new Intent();
        intent.putExtra(IntentConstant.SELECTED_CATEGORY, "");
        handleSelectCategoryResult(ResultCode.RESULT_OK, intent);
    }
    private void handleSelectCategoryResult(int resultCode, Intent data) {
        if (resultCode == ResultCode.RESULT_OK && data != null) {
            handleNormalCategorySelected(data);
            getActivity().setTitle(getString(R.string.explore));
        } else if (resultCode == ResultCode.RESULT_OK_WITH_ID && data != null) {
            String superCategory = data.getStringExtra(IntentConstant.SUPPER_CATEGORY);
            btnCategory.setText(superCategory);
            handleSubCategorySelected(data);
        }
    }
    private void handleNormalCategorySelected(Intent data) {
        String category = data.getStringExtra(IntentConstant.SELECTED_CATEGORY);
        if(TextUtils.isEmpty(category)) {
            category = "All";
            CategoryPresenter.CATEGORY_ALL = true;
        } else {
            CategoryPresenter.CATEGORY_ALL = false;
        }

        presenter.handleCategorySelection(category);
        String displayCategory = capitalWords(category);
        btnCategory.setText(displayCategory);
        btnSearch.setText(getResources().getString(R.string.text_search));
        //(int) getResources().getDimension(R.dimen.padding_medium)
        btnSearch.setPadding(0, (int) getResources().getDimension(R.dimen.padding_medium), 0, 0);
        ViewUtils.drawableTop(btnSearch, R.drawable.ic_search);

        // Track "select category"
        App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CATEGORY_SELECTION.getValue(),
                AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                displayCategory);
    }

    public void handleSelectedCityResult() {
        String selectedCity = CityData.cityName();
        btnLocation.setText(selectedCity);
        presenter.handleCitySelection();
    }

    private void handleSubCategorySelected(Intent data) {
        CategoryPresenter.CATEGORY_ALL = false;
        if (data == null) {
            return;
        }
        int categoryIndex = data.getIntExtra(IntentConstant.INDEX_CITY_GUIDE_CATEGORY_SELECT, 0);
        presenter.cityGuideCategorySelection(categoryIndex);
        selectedSubCategoryName = data.getStringExtra(IntentConstant.SUB_CATEGORY_NAME);
        getActivity().setTitle(selectedSubCategoryName);
        selectedSubCategoryResId = data.getIntExtra(IntentConstant.SUB_CATEGORY_IMAGE, 0);
        // Track city guide category
        String trackingLabel = "";
        switch (categoryIndex){
            case CityGuide.BAR_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_BAR_CLUB.getValue();
                break;
            case CityGuide.CULTURE_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_CULTURE.getValue();
                break;
            case CityGuide.DINNING_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_DINING.getValue();
                break;
            case CityGuide.SHOPPING_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_SHOPPING.getValue();
                break;
            case CityGuide.SPA_LIST_INDEX:
                trackingLabel = AppConstant.GA_TRACKING_LABEL.CITY_GUIDE_SPAS.getValue();
                break;
        }
        if(!TextUtils.isEmpty(trackingLabel)){
            App.getInstance().track(AppConstant.GA_TRACKING_CATEGORY.CATEGORY_SELECTION.getValue(),
                    AppConstant.GA_TRACKING_ACTION.SELECT.getValue(),
                    trackingLabel);
        }
        //reset text for btn Search and also remove the icon
        btnSearch.setText(getResources().getString(R.string.text_discover_more));
        ViewUtils.emptyDrawable(btnSearch);
        btnSearch.setPadding(0,Utils.dip2px(getContext(), 20),0,(int) getResources().getDimension(R.dimen.padding_smallest));
    }

    private void handleSearchInputResult(int resultCode, Intent data) {
        if(resultCode == ResultCode.RESULT_OK) {
            emptyData(false);
            String term = data.getStringExtra(Intent.EXTRA_TEXT);
            boolean withOffers = data.getBooleanExtra(Intent.EXTRA_REFERRER, false);
            showSearchBox(term, withOffers);
            presenter.searchByTerm(term, withOffers);
        } else if (!presenter.inSearchingMode() && shouldReload) {
            presenter.getAccommodationData();
            shouldReload = false;
        }


        /*else if(!presenter.inSearchingMode()) {
            presenter.getAccommodationData();
        }*/
    }

    @Override
    public void onItemClick(int pos) {
        // highlight selected item
        if(lastSelected > -1) exploreRViewAdapter.notifyItemChanged(lastSelected);
        exploreRViewAdapter.notifyItemChanged(pos);
        lastSelected = pos;

        ExploreRViewItem listItem = exploreRViewAdapter.getItem(pos);
        ExploreRView detailViewData = presenter.detailItem(listItem.getItemType(), listItem.dataListIndex);
        if(detailViewData == null) return;
        Intent intent;
        switch (detailViewData.getItemType()) {
            case DINING:
                intent = new Intent(getContext(), DiningDetailActivity.class);
                break;
            case CITY_GUIDE:
                if(detailViewData instanceof CityGuideDetailItem) {
                    ((CityGuideDetailItem) detailViewData)
                            .setSubCategoryName(selectedSubCategoryName)
                            .setImageResId(selectedSubCategoryResId);
                }

                intent = new Intent(getContext(), CityGuideDetailActivity.class);
//                intent.putExtra(IntentConstant.CATEGORY_NAME, selectedCity);
                break;
            case NORMAL:
                intent = new Intent(getContext(), OtherExploreDetailActivity.class);
                break;
            default:
                throw new IllegalStateException("type must be one of above");
        }
        intent.putExtra(IntentConstant.EXPLORE_DETAIL, detailViewData);
        startActivity(intent);
    }

    private RecyclerViewScrollListener scrollListener;
    private void setupLoadMore() {
        scrollListener = new RecyclerViewScrollListener() {

            @Override
            public void onScrollUp() {}

            @Override
            public void onScrollDown() {}

            @Override
            public void onLoadMore() {
                presenter.loadMore();
            }
        };
        recommendView.addOnScrollListener(scrollListener);
    }

    private String capitalWords(String text) {
        char[] chars = text.toCharArray();
        for (int i=chars.length-1; i >= 1; i--) {
            chars[i] = Character.toLowerCase(chars[i]);
            if(chars[i] == ' ' && i-1>=0) {
                chars[i+1] = Character.toUpperCase(chars[i+1]);
            }
        }
        return new String(chars);
    }

    private void showSearchBox(String term, boolean withOffers) {
        tvNoData.setVisibility(View.GONE);
        btnAskConcierge.setVisibility(View.GONE);
        searchBox.setVisibility(View.VISIBLE);
        SpannableStringBuilder str = null;
        int startInd, endInd;
        if(TextUtils.isEmpty(term)) {
            String text = getString(R.string.search_text_result, "offers");
            str = new SpannableStringBuilder(text);
            startInd = text.lastIndexOf("offers");
            endInd = startInd + "offers".length();
        } else {
            String text = getString(R.string.search_text_result, term);
            str = new SpannableStringBuilder(text);
            startInd = text.lastIndexOf(term);
            endInd = startInd + term.length();
        }
        str.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), startInd, endInd, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        edtSearch.setText(str);

        if(withOffers)
            withOffersWrapper.setVisibility(View.VISIBLE);
        else
            withOffersWrapper.setVisibility(View.GONE);
    }

    @Override
    public void clearSearch(boolean repeat) {
        searchBox.setVisibility(View.GONE);
        tvNoData.setVisibility(View.GONE);
        btnAskConcierge.setVisibility(View.GONE);
        withOffersWrapper.setVisibility(View.GONE);
        presenter.clearSearch();
        emptyData(false);
        if(repeat) {
            shouldReload = true;
            Intent intent = new Intent(getActivity(), SearchActivity.class);
            startActivityForResult(intent, RequestCode.SEARCH_INPUT);
        }
    }

    private void searchWithGivenKeyword() {
        // Go to Search screen
        Intent intent = new Intent(getActivity(), SearchActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, presenter.searchTerm());
        intent.putExtra(Intent.EXTRA_REFERRER, presenter.isWithOffers());
        startActivityForResult(intent, RequestCode.SEARCH_INPUT);
    }

}