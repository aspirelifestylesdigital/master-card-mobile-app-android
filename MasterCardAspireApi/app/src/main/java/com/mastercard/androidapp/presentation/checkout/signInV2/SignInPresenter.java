package com.mastercard.androidapp.presentation.checkout.signInV2;

import android.content.Context;

import com.api.aspire.common.constant.ErrCode;
import com.api.aspire.common.constant.ErrorApi;
import com.api.aspire.data.datasource.RemoteUserProfileOKTADataStore;
import com.api.aspire.data.preference.PreferencesStorageAspire;
import com.api.aspire.data.repository.AuthDataAspireRepository;
import com.api.aspire.data.repository.ProfileDataAspireRepository;
import com.api.aspire.data.repository.UserProfileOKTADataRepository;
import com.api.aspire.domain.repository.ProfileAspireRepository;
import com.api.aspire.domain.usecases.GetToken;
import com.api.aspire.domain.usecases.SignInCase;
import com.mastercard.androidapp.App;
import com.mastercard.androidapp.R;
import com.mastercard.androidapp.datalayer.repository.B2CDataRepository;
import com.mastercard.androidapp.domain.usecases.CheckBIN;
import com.mastercard.androidapp.domain.usecases.GetMasterCardCopy;
import com.mastercard.androidapp.domain.usecases.GetUserApp;
import com.mastercard.androidapp.domain.usecases.MapProfileApp;
import com.mastercard.androidapp.presentation.splashscreen.SubsequentAccessPresenter;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SignInPresenter implements SignIn.Presenter {
    private CompositeDisposable compositeDisposable;
    private SignIn.View view;
    private GetUserApp getUserApp;

    SignInPresenter(Context c) {
        compositeDisposable = new CompositeDisposable();

        MapProfileApp mapLogic = new MapProfileApp();
        PreferencesStorageAspire preferencesStorageAspire = new PreferencesStorageAspire(c);
        GetMasterCardCopy privacyPolicy = new GetMasterCardCopy(new B2CDataRepository());
        ProfileAspireRepository profileRepository = new ProfileDataAspireRepository(preferencesStorageAspire, mapLogic);
        UserProfileOKTADataRepository userProfileOKTARepository = new UserProfileOKTADataRepository(new RemoteUserProfileOKTADataStore());
        GetToken getToken = new GetToken(preferencesStorageAspire, mapLogic);
        SignInCase signInCase = new SignInCase(mapLogic,
                profileRepository,
                userProfileOKTARepository,
                getToken
        );
        CheckBIN checkBIN = new CheckBIN(new AuthDataAspireRepository(),
                mapLogic,
                getToken
        );
        this.getUserApp = new GetUserApp(signInCase,
                checkBIN,
                privacyPolicy,
                preferencesStorageAspire
        );
    }

    @Override
    public void attach(SignIn.View view) {
        this.view = view;
    }

    @Override
    public void detach() {
        compositeDisposable.dispose();
        view = null;
    }

    @Override
    public void doSignIn(String email, String password) {
        if (!App.getInstance().hasNetworkConnection()) {
            view.showErrorDialog(ErrCode.CONNECTIVITY_PROBLEM, null);
            return;
        }
        view.showProgressDialog();

        compositeDisposable.add(
                getUserApp.signInCleanPreference(new SignInCase.Params(email, password))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new GetSignInObserver()));

    }

    @Override
    public void abort() {
        compositeDisposable.clear();
    }

    private final class GetSignInObserver extends DisposableSingleObserver<SubsequentAccessPresenter.ResultBuilder> {
        @Override
        public void onSuccess(SubsequentAccessPresenter.ResultBuilder resultBuilder) {
            if (view == null) return;
            view.dismissProgressDialog();

            if (resultBuilder.binCodeStatus != SubsequentAccessPresenter.BIN_STATUS.VALID) {
                view.toBINCheckout(getUserApp.getProfileAspireCache(), true);
                return;
            } else if (resultBuilder.privacyPolicyChanged ==
                    SubsequentAccessPresenter.PRIVACY_VERSION_STATUS.MAJOR_CHANGE) {
                view.toPrivacyPolicy(getUserApp.getProfileAspireCache(), true);
                return;
            } else {
                getUserApp.saveProfileLocalSignInCase();
                view.proceedToHome();
            }

            dispose();
        }

        @Override
        public void onError(Throwable e) {
            if (view == null) return;
            view.dismissProgressDialog();
            if(ErrCode.PROFILE_OKTA_LOCKED_OUT.name().equals(e.getMessage())){
                view.showErrorDialog(ErrCode.API_ERROR, App.getInstance().getString(R.string.valid_login_okta_profile_error));
            } else if (ErrCode.USER_PROFILE_OKTA_NOT_EXIST.name().equals(e.getMessage())
                    || ErrorApi.isGetTokenError(e.getMessage())) {
                view.showErrorDialog(ErrCode.API_ERROR, App.getInstance().getString(R.string.valid_login_error));
            } else if (ErrCode.PASS_CODE_ERROR.name().equalsIgnoreCase(e.getMessage())) {
                //- check profile after sign in
                if (getUserApp.getProfileAspireCache() == null) {
                    view.showErrorDialog(ErrCode.UNKNOWN_ERROR,null);

                } else {
                    //-- go this flow Sign in invalid binCode
                    view.toBINCheckout(getUserApp.getProfileAspireCache(), true);
                }

            } else {
                view.showErrorDialog(ErrCode.UNKNOWN_ERROR,null);
            }
            dispose();
        }
    }

}
